$(function () {
    $('form select').select2();

    $('#users-change_password').change( function(){
        if($(this).is(":checked")){
            $('.change-password').removeClass('hidden');
            $('.change-password').addClass('show');
        }
        else if($(this).is(":not(:checked)")){
            $('.change-password').removeClass('show');
            $('.change-password').addClass('hidden');
        }
    });

});