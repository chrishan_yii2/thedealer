<?php

namespace backend\controllers;

use common\models\User;
use common\models\Users;
use Yii;
use common\models\ClassifiedListings;
use backend\models\ClassifiedSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\AdminController;

/**
 * ClassifiedController implements the CRUD actions for ClassifiedListings model.
 */
class ClassifiedController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'users-list', 'users-list-ajax', 'upload'],
                'rules' => [
                    // allow authenticated users
                        [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->user_type != \common\models\Users::ROLE_ADMIN) {
                                return $this->redirect('/login');
                            }else if(Yii::$app->user->identity->user_type == \common\models\Users::ROLE_ADMIN){
                                return true;
                            }
                        }
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all ClassifiedListings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClassifiedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClassifiedListings model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->redirect(Yii::$app->request->referrer);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ClassifiedListings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ClassifiedListings();
        
        $post = Yii::$app->request->post();
        
        if(isset(Yii::$app->request->isPost) && !empty(Yii::$app->request->post())){
            $post['ClassifiedListings']['start_date'] = time();
            $post['ClassifiedListings']['date_added'] = time();
            $dur = (isset($post['ClassifiedListings']['duration']) && !empty($post['ClassifiedListings']['duration'])) ? $post['ClassifiedListings']['duration'] : $post['ClassifiedListings']['start_date'];
            $post['ClassifiedListings']['end_date'] = time() + ($dur * 24 * 60 * 60);
        }
        
        if ($model->load($post) && $model->save()) {
            Yii::$app->session->setFlash('success', "Record Successfully Create");
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ClassifiedListings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Record Successfully Updated");
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ClassifiedListings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)   
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClassifiedListings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClassifiedListings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClassifiedListings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionUsersList($user_type)
    {
        $accounts = Users::find()->select(['id', 'contact_name', 'company_name'])->where(['user_type' => $user_type, 'status' => Users::STATUS_ACTIVE])->limit(300)->all();
        if (!empty($accounts)){
            echo '<option> Select Account </option>';
            foreach ($accounts as $account){
                if ($user_type == Users::ROLE_BUSINESS){
                    echo '<option value="'.$account->id.'">'.$account->company_name.'</option>';
                } else {
                    echo '<option value="'.$account->id.'">'.$account->contact_name.'</option>';
                }
            }
        } else {
            echo '<option value=" "> - </option>';
        }
        exit;
    }
    
    public function actionUsersListAjax($q = null, $id = null, $ad_type = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $userList = Users::find()->select(['id', 'contact_name as text'])
                                        ->where(['like', 'username' , $q])
                                        ->orWhere(['like', 'contact_name' , $q])
                                        ->orWhere(['like', 'company_name' , $q])
                                        ->andWhere(['user_type' => $ad_type])
                                        ->andWhere(['status' => Users::STATUS_ACTIVE])
                                        ->limit(100)
                                        ->asArray()
                                        ->all();
            $out['results'] = $userList;
        }
            
        return $out;
        
    }

    public function actionUpload()
    {
        $fileName = 'file';
        $uploadPath = 'uploads';

        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                //Now save file data to database

                echo \yii\helpers\Json::encode($file);
            }
        } else {
            return $this->render('upload');
        }

        return false;
    }
}
