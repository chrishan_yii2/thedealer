<?php

namespace backend\controllers;

use Yii;
use common\models\ClassifiedCategories;
use common\models\ClassifiedCategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClassifiedCategoriesController implements the CRUD actions for ClassifiedCategories model.
 */
class ClassifiedCategoriesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'child-cats', 'sub-child-cats'],
                'rules' => [
                    // allow authenticated users
                        [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action){
                            if (Yii::$app->user->identity->user_type != \common\models\Users::ROLE_ADMIN) {
                                return $this->redirect('/login');
                            }else if(Yii::$app->user->identity->user_type == \common\models\Users::ROLE_ADMIN){
                                return true;
                            }
                        }
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all ClassifiedCategories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClassifiedCategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClassifiedCategories model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->redirect(Yii::$app->request->referrer);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ClassifiedCategories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ClassifiedCategories();
        
        $post = Yii::$app->request->post();
        
        if(isset(Yii::$app->request->isPost) && !empty(Yii::$app->request->post())){
            $post['ClassifiedCategories']['slug'] = $this->createSlug($post['ClassifiedCategories']['category']);
            $post['ClassifiedCategories']['date_added'] = time();
            $post['ClassifiedCategories']['parent_id'] = (isset($post['ClassifiedCategories']['parent_id']) && !empty($post['ClassifiedCategories']['parent_id'])) ? $post['ClassifiedCategories']['parent_id'] : 0;
            $post['ClassifiedCategories']['temp_parent_id'] = (isset($post['ClassifiedCategories']['temp_parent_id']) && !empty($post['ClassifiedCategories']['temp_parent_id'])) ? $post['ClassifiedCategories']['temp_parent_id'] : 0;
            $post['ClassifiedCategories']['temp_parent_id_2'] = (isset($post['ClassifiedCategories']['temp_parent_id_2']) && !empty($post['ClassifiedCategories']['temp_parent_id_2'])) ? $post['ClassifiedCategories']['temp_parent_id_2'] : 0;
            $post['ClassifiedCategories']['image_count'] = (isset($post['ClassifiedCategories']['image_count']) && !empty($post['ClassifiedCategories']['image_count'])) ? $post['ClassifiedCategories']['image_count'] : 0;
            $post['ClassifiedCategories']['order'] = (isset($post['ClassifiedCategories']['order']) && !empty($post['ClassifiedCategories']['order'])) ? $post['ClassifiedCategories']['order'] : 0;
        }
        
        if ($model->load($post) && $model->save()) {
            
            Yii::$app->session->setFlash('success', "Record Successfully Create");
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ClassifiedCategories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             Yii::$app->session->setFlash('success', "Record Successfully updated");
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ClassifiedCategories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClassifiedCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClassifiedCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClassifiedCategories::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionChildCats($id) {
        $countChildCats = ClassifiedCategories::find()->childCategories($id)->count();
        $childCats = ClassifiedCategories::find()->childCategories($id)->all();
        if ($countChildCats > 0) {
            echo '<option> Select Child Category </option>';
            foreach ($childCats as $childCat) {
                echo '<option value="'.$childCat->id.'">'.$childCat->category.'</option>';
            }
        }
        else{
            echo '<option> - </option>';
        }
    }

    public function actionSubChildCats($id) {
        $countChildCats = ClassifiedCategories::find()->subChildCategories($id)->count();
        $childCats = ClassifiedCategories::find()->subChildCategories($id)->all();
        if ($countChildCats > 0) {
            echo '<option> Select Sub Child Category </option>';
            foreach ($childCats as $childCat) {
                echo '<option value="'.$childCat->id.'">'.$childCat->category.'</option>';
            }
        }
        else{
            echo '<option >  </option>';
        }
    }
    
    /*
     * Create slug able string
     */
    public static function createSlug($str, $delimiter = '-'){

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }

}
