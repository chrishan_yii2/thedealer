<?php
namespace backend\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;
/**
 * Description of AdminController
 *
 * @author Rohit Gupta
 */

class AdminController extends Controller
{
    
        
    public function init()
    {   
        parent::init();
    }
    
    
    /**
     * @inheritdoc
     */
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'controllers' => ['auth'],
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }
    
    /**
     * function to add items Breacrumbs array
     * 
     * @param string $name Label Name
     * @param string $link route
     */
    public function addBreadcrumb($name, $link = '')
    {
        $linkItem = array (
            'label' => $name
        );
        
        if (!empty($link)) {
            $linkItem['url'] = $link;
        }
        
        \Yii::$app->params['breadcrumbs'][] = $linkItem;
    }
    
    /**
     * Function to set various template variables
     * 
     * @param string $variable
     * @param any $value
     */
    public function setTemplateVariable($variable, $value)
    {
        if (!isset(\Yii::$app->params['template']))
        {
            \Yii::$app->params['template'] = array();
        }
        
        \Yii::$app->params['template'][$variable] = $value;
    }
    
    /**
     * Function to get template variable
     * 
     * @param string $variable
     * @param any $value
     */
    public function getTemplateVariable($variable)
    {
        if (!isset(\Yii::$app->params['template']))
        {
            \Yii::$app->params['template'] = array();
        }
        
        return isset(\Yii::$app->params['template'][$variable]) ? \Yii::$app->params['template'][$variable] : '';
    } 
    
    /**
     * Function to get the page title
     * 
     * @return string
     */
    public function getPageTitle( )
    {
        return $this->getTemplateVariable('page_title');
    }
    
    public function setPageTitle( $pageTitle )
    {
        $this->setTemplateVariable('page_title', $pageTitle);
    }
    
    /**
     * Function to get the page title
     * 
     * @return string
     */
    public function getPageSubTitle( )
    {
        return $this->getTemplateVariable('page_sub_title');
    }
    
    public function setPageSubTitle( $pageSubTitle )
    {
        $this->setTemplateVariable('page_sub_title', $pageSubTitle);
    }
    
    /*
     * Debug Variables
     */
    public function debug($value){
        echo "<pre>";
        print_r($value);
    }
    
    /* 
     * Return date function
     */
    public static function toDate($date){
        return date('d-m-Y',$date);
    }


    /*
     * Create slug able string
     */
    public static function createSlug($str, $delimiter = '-'){

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }
}
