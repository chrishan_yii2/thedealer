<?php
namespace backend\controllers;

use Yii;
use backend\controllers\AdminController;
use common\models\LoginForm;

/**
 * Class to handle the 
 *
 * @author Rohit Gupta
 */
class AuthController extends AdminController
{
    public function init( )
    {
        $this->layout = 'login';
        parent::init();
    }
    
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if(Yii::$app->user->identity->user_type == \common\models\Users::ADMIN_USER){
                return $this->redirect('/admin/classified/index?ClassifiedSearch[ad_type]=private');
            }
            return $this->goBack();
        } 
        else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
