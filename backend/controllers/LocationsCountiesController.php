<?php

namespace backend\controllers;

use Yii;
use backend\models\LocationsCounties;
use backend\models\LocationsCountiesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\AdminController;

/**
 * LocationsCountiesController implements the CRUD actions for LocationsCounties model.
 */
class LocationsCountiesController extends AdminController {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => [
                    // allow authenticated users
                        [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->user_type != \common\models\Users::ROLE_ADMIN) {
                                return $this->redirect('/login');
                            }else if(Yii::$app->user->identity->user_type == \common\models\Users::ROLE_ADMIN){
                                return true;
                            }
                        }
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all LocationsCounties models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new LocationsCountiesSearch();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LocationsCounties model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->redirect(Yii::$app->request->referrer);
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LocationsCounties model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new \backend\models\LocationsCounties();
        
        $post = Yii::$app->request->post();
        if(isset($post) && !empty($post)){
            $json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDlHHj7YvFQ4yid_RRm4v-A4-ryOdwtcwk&address='.$post['LocationsCounties']['county'].'&sensor=false');
            $data = json_decode($json);

            $lat = (isset($data->results[0]->geometry->location->lat) && !empty($data->results[0]->geometry->location->lat)) ? $data->results[0]->geometry->location->lat : 0;
            $lng = (isset($data->results[0]->geometry->location->lng) && !empty($data->results[0]->geometry->location->lng)) ? $data->results[0]->geometry->location->lng : 0;

            $post['LocationsCounties']['latitude'] = $lat; 
            $post['LocationsCounties']['longitude'] = $lng;
            $post['LocationsCounties']['zoom'] = 0;
            $post['LocationsCounties']['slug'] = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $post['LocationsCounties']['county'])));
            $post['LocationsCounties']['date_added'] = time();
        }
        
        if ($model->load($post) && $model->save()) {
            
            Yii::$app->session->setFlash('success', 'Record Created successfully.');
            return $this->redirect(Yii::$app->request->referrer);
                      
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing LocationsCounties model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        
        $post = Yii::$app->request->post();
        if(isset($post) && !empty($post)){
            $json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDlHHj7YvFQ4yid_RRm4v-A4-ryOdwtcwk&address='.$post['LocationsCounties']['county'].'&sensor=false');
            $data = json_decode($json);

            $lat = (isset($data->results[0]->geometry->location->lat) && !empty($data->results[0]->geometry->location->lat)) ? $data->results[0]->geometry->location->lat : 0;
            $lng = (isset($data->results[0]->geometry->location->lng) && !empty($data->results[0]->geometry->location->lng)) ? $data->results[0]->geometry->location->lng : 0;

            $post['LocationsCounties']['latitude'] = $lat; 
            $post['LocationsCounties']['longitude'] = $lng;
            $post['LocationsCounties']['zoom'] = 0;
            $post['LocationsCounties']['slug'] = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $post['LocationsCounties']['county'])));
        }
        
        if ($model->load($post) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Record Updated successfully.');
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LocationsCounties model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LocationsCounties model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LocationsCounties the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = LocationsCounties::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
