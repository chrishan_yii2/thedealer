<?php

namespace backend\controllers;

use Yii;
use common\models\Users;
use backend\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\AdminController;
use common\models\LocationsTowns;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'town-lists'],
                'rules' => [
                    // allow authenticated users
                        [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->user_type != \common\models\Users::ROLE_ADMIN) {
                                return $this->redirect('/login');
                            }else if(Yii::$app->user->identity->user_type == \common\models\Users::ROLE_ADMIN){
                                return true;
                            }
                        }
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;
        //Validate user type
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search($queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->redirect(Yii::$app->request->referrer);
        return $this->render('update', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user_type = $queryParams = Yii::$app->request->queryParams;
        $model = new Users();

        $model->setScenario($user_type['user_type']);

        if(isset(Yii::$app->request->isPost)) {
            if ($model->load(Yii::$app->request->post())) {

                $password = Yii::$app->security->generateRandomString(6);
                $model->setPassword($password);
                $post = Yii::$app->request->post();
                $model->date_added = time();
                if ($user = $model->save() && $model->sendAccountDetailsEmail($password)) {
                    Yii::$app->session->setFlash('success', 'Record created successfully.');
                    return $this->redirect(Yii::$app->request->referrer);
                } else {
                    Yii::$app->session->setFlash('error', 'Something went wrong.');
                }
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario($model->user_type);

        if (isset(Yii::$app->request->isPost) && $model->load(Yii::$app->request->post())){
            $posts = Yii::$app->request->post();
            $model->load(Yii::$app->request->post());
            if ($model->id == Yii::$app->user->identity->id ) {
                if ($model->change_password == 1){
                    $model->setPassword($posts['Users']['password']);
                }
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Record Updated successfully.');
                return $this->redirect(Yii::$app->request->referrer);
            } else {
                Yii::$app->session->setFlash('success', 'Record Not Updated.');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Your account has been Updated successfully.');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionTownLists($id) {
        $posts = LocationsTowns::find()
            ->where(['county_id' => $id])
            ->all();
        if (!empty($posts)) {
            echo '<option value="0"> Select Town </option>';
            foreach ($posts as $post) {
                echo '<option value="'.$post->id.'">'.$post->town.'</option>';
            }
        }
        else{
            echo '<option value="0"> - </option>';
        }
    }
}
