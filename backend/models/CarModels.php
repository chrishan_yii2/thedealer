<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "car_models".
 *
 * @property int $id
 * @property int $make_id
 * @property string $model
 * @property string $slug
 * @property string $status
 * @property int $date_added
 */
class CarModels extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_models';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make_id', 'model', 'slug', 'date_added'], 'required'],
            [['make_id', 'date_added'], 'integer'],
            [['status'], 'string'],
            [['model', 'slug'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'make_id' => Yii::t('app', 'Make ID'),
            'model' => Yii::t('app', 'Model'),
            'slug' => Yii::t('app', 'Slug'),
            'status' => Yii::t('app', 'Status'),
            'date_added' => Yii::t('app', 'Date Added'),
        ];
    }
}
