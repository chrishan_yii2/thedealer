<?php

namespace backend\models;

use components\behaviors\DateBehavior;
use Yii;

/**
 * This is the model class for table "locations_counties".
 *
 * @property int $id
 * @property string $county
 * @property string $status
 * @property double $latitude
 * @property double $longitude
 * @property int $zoom
 * @property int $date_added
 * @property string $slug
 */
class LocationsCounties extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locations_counties';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['county', 'status'], 'required'],
            [['id', 'zoom', 'date_added'], 'integer'],
            [['date_added', 'status', 'slug'], 'safe'],
            [['latitude', 'longitude'], 'number'],
            ['county', 'unique', 'targetClass' => LocationsCounties::class, 'message' => 'Record is already exist.'],
        ];
    }
}
