<?php

namespace backend\models;

use Yii;
use common\models\ClassifiedCategories;

/**
 * This is the model class for table "adsense_category".
 *
 * @property int $id
 * @property int $category_id
 * @property string $ad_location
 * @property string $adsense_code_1
 * @property string $adsense_code_2
 * @property string $adsense_code_3
 */
class AdsenseCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'adsense_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['ad_location', 'adsense_code_1', 'adsense_code_2', 'adsense_code_3'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'ad_location' => Yii::t('app', 'Ads Location'),
            'adsense_code_1' => Yii::t('app', 'Adsense Code 1'),
            'adsense_code_2' => Yii::t('app', 'Adsense Code 2'),
            'adsense_code_3' => Yii::t('app', 'Adsense Code 3'),
        ];
    }

    /**
     * @return \yii\db\ActivQuery
     */
    public function getCategories()
    {
        return $this->hasOne(ClassifiedCategories::className(),['id' => 'category_id']);
    }
}
