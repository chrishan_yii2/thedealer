<?php

namespace backend\models\base;

use backend\models\base\LocationsTowns as BaseLocationsTowns;

use Yii;

/**
 * This is the model class for table "locations_towns".
 *
 * @property int $id
 * @property int $county_id
 * @property string $town
 * @property string $status
 * @property int $date_added
 * @property double $latitude
 * @property double $longitude
 * @property string $slug
 */
class LocationsTowns extends BaseLocationsTowns
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locations_towns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();
        $myRules = [];
        if(count($myRules) > 0){
            return array_merge($rules, $myRules);
        }
        return true;
    }
}
