<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\base\CarMakes;
use common\models\MyActiveRecord;

/**
 * CarMakesSearch represents the model behind the search form of `backend\models\base\CarMakes`.
 */
class CarMakesSearch extends CarMakes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['make', 'slug', 'status', 'date_added'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarMakes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        //Search date added
        if (!empty($this->date_added)) {
            $dateAddedArray = explode(' - ', $this->date_added);
            $datesArray = ['fromDate' => $dateAddedArray[0], 'toDate' => $dateAddedArray[1]];
            $timestamps = MyActiveRecord::convertSearchDateInToTimestamp($datesArray);
            $createdAtFrom = $timestamps['fromDate'];
            $createdAtTo = $timestamps['toDate'];
            if (!empty($createdAtFrom) && !empty($createdAtTo) && ($createdAtFrom <= $createdAtTo)) {
                $query->andFilterWhere(['between', 'date_added', $createdAtFrom, $createdAtTo]);
            } else {
                $query->where('0=1');
            }
        }

        $query->andFilterWhere(['like', 'make', $this->make])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
