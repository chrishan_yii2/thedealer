<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ClassifiedListings;
use common\models\MyActiveRecord;

/**
 * ClassifiedSearch represents the model behind the search form of `common\models\ClassifiedListings`.
 */
class ClassifiedSearch extends ClassifiedListings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'classified_cat_id', 'user_id', 'image_count', 'duration', 'town_id', 'county_id', 'order', 'bump', 'bump_count', 'date_sold', 'views', 'make_id', 'model_id', 'year', 'date_added', 'edition_date', 'co2_emissions', 'mileage'], 'integer'],
            [['sub_category','parent_id', 'ad_type', 'title', 'description', 'search_options', 'currency', 'price', 'price_suffix', 'item_status', 'telephone', 'email', 'mobile', 'other', 'status', 'featured', 'auto_make', 'auto_model', 'fuel', 'body_type', 'colour', 'engine_size', 'version', 'business_plus_url', 'edition_one', 'edition_two', 'edition_three', 'edition_four', 'buy_now_url', 'transmission', 'nct_date', 'tax_date'], 'safe'],
            [['search_price'], 'number'],
            [['start_date', 'end_date'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClassifiedListings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->setSort([
            'defaultOrder' => [
                'id' => SORT_DESC
            ]
        ]);

        
        $query->joinWith('classifiedCat');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'classified_cat_id' => $this->classified_cat_id,
            'user_id' => $this->user_id,
            'image_count' => $this->image_count,
            'search_price' => $this->search_price,
            'duration' => $this->duration,
            'town_id' => $this->town_id,
            'county_id' => $this->county_id,
            'order' => $this->order,
            'bump' => $this->bump,
            'bump_count' => $this->bump_count,
            'date_sold' => $this->date_sold,
            'views' => $this->views,
            'make_id' => $this->make_id,
            'model_id' => $this->model_id,
            'year' => $this->year,
            'date_added' => $this->date_added,
            'edition_date' => $this->edition_date,
            'co2_emissions' => $this->co2_emissions,
            'mileage' => $this->mileage,
        ]);

        //Search start date
        if (!empty($this->start_date)) {
            $dateAddedArray = explode(' - ', $this->start_date);
            $datesArray = ['fromDate' => $dateAddedArray[0], 'toDate' => $dateAddedArray[1]];
            $timestamps = MyActiveRecord::convertSearchDateInToTimestamp($datesArray);
            $createdAtFrom = $timestamps['fromDate'];
            $createdAtTo = $timestamps['toDate'];
            if (!empty($createdAtFrom) && !empty($createdAtTo) && ($createdAtFrom <= $createdAtTo)) {
                $query->andFilterWhere(['between', 'start_date', $createdAtFrom, $createdAtTo]);
            } else {
                $query->where('0=1');
            }
        }
        //Search start date
        if (!empty($this->end_date)) {
            $dateAddedArray = explode(' - ', $this->end_date);
            $datesArray = ['fromDate' => $dateAddedArray[0], 'toDate' => $dateAddedArray[1]];
            $timestamps = MyActiveRecord::convertSearchDateInToTimestamp($datesArray);
            $createdAtFrom = $timestamps['fromDate'];
            $createdAtTo = $timestamps['toDate'];
            if (!empty($createdAtFrom) && !empty($createdAtTo) && ($createdAtFrom <= $createdAtTo)) {
                $query->andFilterWhere(['between', 'end_date', $createdAtFrom, $createdAtTo]);
            } else {
                $query->where('0=1');
            }
        }

        $query->andFilterWhere(['like', 'sub_category', $this->sub_category])
            ->andFilterWhere(['like', 'ad_type', $this->ad_type])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'search_options', $this->search_options])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'price_suffix', $this->price_suffix])
            ->andFilterWhere(['like', 'item_status', $this->item_status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'other', $this->other])
            ->andFilterWhere(['like', 'classified_listings.status', $this->status])
            ->andFilterWhere(['like', 'featured', $this->featured])
            ->andFilterWhere(['like', 'auto_make', $this->auto_make])
            ->andFilterWhere(['like', 'auto_model', $this->auto_model])
            ->andFilterWhere(['like', 'fuel', $this->fuel])
            ->andFilterWhere(['like', 'body_type', $this->body_type])
            ->andFilterWhere(['like', 'colour', $this->colour])
            ->andFilterWhere(['like', 'engine_size', $this->engine_size])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'business_plus_url', $this->business_plus_url])
            ->andFilterWhere(['like', 'edition_one', $this->edition_one])
            ->andFilterWhere(['like', 'edition_two', $this->edition_two])
            ->andFilterWhere(['like', 'edition_three', $this->edition_three])
            ->andFilterWhere(['like', 'edition_four', $this->edition_four])
            ->andFilterWhere(['like', 'buy_now_url', $this->buy_now_url])
            ->andFilterWhere(['like', 'transmission', $this->transmission])
            ->andFilterWhere(['like', 'nct_date', $this->nct_date])
             ->andFilterWhere(['like', 'classified_categories.category', $this->parent_id])
            ->andFilterWhere(['like', 'tax_date', $this->tax_date]);

        return $dataProvider;
    }
}
