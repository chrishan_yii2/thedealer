<?php

namespace backend\models;

use backend\models\base\CarMakes as baseCarMakes;
use Yii;

/**
 * This is the model class for table "car_makes".
 *
 * @property int $id
 * @property string $make
 * @property string $slug
 * @property string $status
 * @property int $date_added
 */
class CarMakes extends baseCarMakes
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_makes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();
        $myRules = [
            [['make'], 'required'],
            [['make', 'slug', 'date_added'],'safe']
        ];
        if(count($myRules) > 0){
            return array_merge($rules, $myRules);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'make' => 'Make',
            'slug' => 'Slug',
            'status' => 'Status',
            'date_added' => 'Date Added',
        ];
    }
}
