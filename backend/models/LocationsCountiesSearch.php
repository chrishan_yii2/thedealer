<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\LocationsCounties;

/**
 * LocationsCountiesSearch represents the model behind the search form of `\backend\models\base\LocationsCounties`.
 */
class LocationsCountiesSearch extends LocationsCounties
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'zoom'], 'integer'],
            [['county', 'date_added', 'status', 'slug'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LocationsCounties::find()->orderBy('date_added');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'zoom' => $this->zoom,
            'date_added' => $this->date_added,
        ]);

        $query->andFilterWhere(['like', 'county', $this->county])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'slug', $this->slug]);
        
        return $dataProvider;
    }
}
