<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\base\LocationsTowns;
use common\models\MyActiveRecord;

/**
 * LocationsTownsSearch represents the model behind the search form of `\backend\models\base\LocationsTowns`.
 */
class LocationsTownsSearch extends LocationsTowns
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['town','county_id', 'status', 'slug', 'date_added'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LocationsTowns::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('locationsCounties');
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ]);

        //Search date added
        if (!empty($this->date_added)) {
            $dateAddedArray = explode(' - ', $this->date_added);
            $datesArray = ['fromDate' => $dateAddedArray[0], 'toDate' => $dateAddedArray[1]];
            $timestamps = MyActiveRecord::convertSearchDateInToTimestamp($datesArray);
            $createdAtFrom = $timestamps['fromDate'];
            $createdAtTo = $timestamps['toDate'];
            if (!empty($createdAtFrom) && !empty($createdAtTo) && ($createdAtFrom <= $createdAtTo)) {
                $query->andFilterWhere(['between', 'locations_towns.date_added', $createdAtFrom, $createdAtTo]);
            } else {
                $query->where('0=1');
            }
        }

        $query->andFilterWhere(['like', 'town', $this->town])
            ->andFilterWhere(['like', 'locations_towns.status', $this->status])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'locations_counties.county', $this->county_id]);

        return $dataProvider;
    }
}
