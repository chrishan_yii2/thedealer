<?php

namespace backend\models\base;

use Yii;
/**
 * This is the model class for table "articals".
 *
 * @property int $id
 * @property int $ads_id
 * @property int $user_id
 * @property string $title
 * @property int $is_comment_able
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class Articals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();
        $myRules = [];
        if(count($myRules) > 0){
            return array_merge($rules, $myRules);
        }
        return true;
    }
}
