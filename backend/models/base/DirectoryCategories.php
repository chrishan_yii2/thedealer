<?php

namespace backend\models\base;

use Yii;

/**
 * This is the model class for table "directory_categories".
 *
 * @property int $id
 * @property string $category
 * @property string $status
 * @property int $date_added
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $slug
 */
class DirectoryCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directory_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['status','meta_title', 'meta_keywords', 'meta_description'], 'string'],
            [['date_added'], 'integer'],
            [['category', 'slug'], 'string', 'max' => 50],
            [['meta_title', 'meta_keywords'], 'string', 'max' => 200],
            [['meta_description'], 'string', 'max' => 150],
            ['category', 'unique', 'targetClass' => DirectoryCategories::class, 'message' => 'Record is already exist.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'status' => 'Status',
            'date_added' => 'Date Added',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'slug' => 'Slug',
        ];
    }
}
