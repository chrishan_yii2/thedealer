<?php

namespace backend\models\base;

use Yii;
use common\models\User;
/**
 * This is the model class for table "articals".
 *
 * @property int $id
 * @property int $ads_id
 * @property int $user_id
 * @property string $title
 * @property string $body
 * @property int $is_comment_able
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class Articals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ads_id', 'user_id', 'is_comment_able', 'created_at', 'updated_at'], 'integer'],
            [['user_id', 'title', 'body', 'created_at', 'updated_at'], 'required'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ads_id' => 'Ads ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'body' => 'Body',
            'is_comment_able' => 'Is Comment Able',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
