<?php

namespace backend\models\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\base\Settings;

/**
 * SettingsSearch represents the model behind the search form of `backend\models\base\Settings`.
 */
class SettingsSearch extends Settings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'town_id', 'county_id', 'date_added'], 'integer'],
            [['copyright', 'email', 'company_name', 'address_one', 'address_two', 'postcode', 'telephone', 'meta_title', 'meta_keywords', 'meta_description', 'theme', 'analytics_code'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Settings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'town_id' => $this->town_id,
            'county_id' => $this->county_id,
            'date_added' => $this->date_added,
        ]);

        $query->andFilterWhere(['like', 'copyright', $this->copyright])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'address_one', $this->address_one])
            ->andFilterWhere(['like', 'address_two', $this->address_two])
            ->andFilterWhere(['like', 'postcode', $this->postcode])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'theme', $this->theme])
            ->andFilterWhere(['like', 'analytics_code', $this->analytics_code]);

        return $dataProvider;
    }
}
