<?php

namespace backend\models\base;

use Yii;
use backend\models\LocationsCounties;
/**
 * This is the model class for table "locations_towns".
 *
 * @property int $id
 * @property int $county_id
 * @property string $town
 * @property string $status
 * @property int $date_added
 * @property double $latitude
 * @property double $longitude
 * @property string $slug
 */
class LocationsTowns extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locations_towns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['county_id', 'town', 'date_added'], 'required'],
            [['county_id', 'date_added'], 'integer'],
            [['status'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['town', 'slug'], 'string', 'max' => 40],
            [['county_id', 'town'], 'unique', 'targetAttribute' => ['county_id', 'town']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'county_id' => 'County',
            'town' => 'Town',
            'status' => 'Status',
            'date_added' => 'Date Added',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'slug' => 'Slug',
        ];
    }
    
    /**
     * @return \yii\db\ActivQuery
     */
    public function getLocationsCounties()
    {
        return $this->hasOne(LocationsCounties::className(),['id' => 'county_id']);
    }
}
