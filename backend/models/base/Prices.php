<?php

namespace backend\models\base;

use Yii;

/**
 * This is the model class for table "prices".
 *
 * @property int $id
 * @property string $price
 * @property string $price_type
 * @property int $duration
 * @property string $status
 * @property int $date_added
 */
class Prices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price','price_type', 'date_added'], 'required'],
            [['price'], 'number'],
            [['price_type', 'status'], 'string'],
            [['duration', 'date_added'], 'integer'],
            ['price_type', 'unique', 'targetClass' => Prices::class, 'message' => 'Record is already exist.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'price_type' => 'Price Type',
            'duration' => 'Duration',
            'status' => 'Status',
            'date_added' => 'Date Added',
        ];
    }
}
