<?php

namespace backend\models\base;

use Yii;

/**
 * This is the model class for table "car_makes".
 *
 * @property int $id
 * @property string $make
 * @property string $slug
 * @property string $status
 * @property int $date_added
 */
class CarMakes extends \yii\db\ActiveRecord
{
    const ACTIVE = 'active';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_makes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make','status'], 'required'],
            [['status'], 'string'],
            [['date_added'], 'integer'],
            [['make', 'slug'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'make' => 'Make',
            'slug' => 'Slug',
            'status' => 'Status',
            'date_added' => 'Date Added',
        ];
    }
}
