<?php

namespace backend\models\base;


use Yii;
use common\models\LocationsCounties;
/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $copyright
 * @property string $email
 * @property string $company_name
 * @property string $address_one
 * @property string $address_two
 * @property int $town_id
 * @property int $county_id
 * @property string $postcode
 * @property string $telephone
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $theme
 * @property string $analytics_code
 * @property int $date_added
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['copyright', 'email', 'address_one', 'town_id', 'county_id', 'telephone'], 'required'],
            [['town_id', 'county_id', 'date_added'], 'integer'],
            [['analytics_code'], 'string'],
            [['copyright'], 'string', 'max' => 60],
            [['email', 'address_one', 'address_two', 'theme'], 'string', 'max' => 100],
            [['company_name'], 'string', 'max' => 50],
            [['postcode'], 'string', 'max' => 8],
            [['telephone'], 'string', 'max' => 20],
            [['meta_title', 'meta_keywords'], 'string', 'max' => 200],
            [['meta_description'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'copyright' => 'Copyright',
            'email' => 'Email',
            'company_name' => 'Company Name',
            'address_one' => 'Address One',
            'address_two' => 'Address Two',
            'town_id' => 'Town ID',
            'county_id' => 'County ID',
            'postcode' => 'Postcode',
            'telephone' => 'Telephone',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'theme' => 'Theme',
            'analytics_code' => 'Analytics Code',
            'date_added' => 'Date Added',
        ];
    }
    
    /**
     * @return \yii\db\ActivQuery
     */
    public function getLocationsCounties()
    {
        return $this->hasOne(LocationsCounties::className(),['id' => 'county_id']);
    }
}
