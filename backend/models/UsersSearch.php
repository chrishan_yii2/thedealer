<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Users;
use common\models\MyActiveRecord;

/**
 * UsersSearch represents the model behind the search form of `common\models\Users`.
 */
class UsersSearch extends Users {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['id', 'image_count', 'town_id', 'county_id', 'marketing_id', 'directory_cat_id', 'directory_end_date', 'seo_id'], 'integer'],
                [['user_type', 'email', 'username', 'password', 'token', 'token_ip_address', 'hint', 'contact_name', 'company_name', 'address_one', 'address_two', 'town', 'county', 'postcode', 'telephone', 'mobile', 'fax', 'website', 'other', 'newsletter', 'user_lat', 'user_long', 'category', 'description', 'status', 'directory_status', 'directory_link', 'meta_title', 'meta_keywords', 'meta_description', 'business_plus', 'allow_buy_now'], 'safe'],
                [['account_balance'], 'number'],
                [['date_added'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {

        $query = Users::find();
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        
        $dataProvider->setSort([
            'defaultOrder' => [
                'date_added' => SORT_DESC
            ]
        ]);

        $this->load($params);

        
        
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }      
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'image_count' => $this->image_count,
            'town_id' => $this->town_id,
            'county_id' => $this->county_id,
            'marketing_id' => $this->marketing_id,
            'account_balance' => $this->account_balance,
            'directory_cat_id' => $this->directory_cat_id,
            'directory_end_date' => $this->directory_end_date,
            'status' => $this->status,
        ]);

        //Search date added
        if (!empty($this->date_added)) {
            $dateAddedArray = explode(' - ', $this->date_added);
            $datesArray = ['fromDate' => $dateAddedArray[0], 'toDate' => $dateAddedArray[1]];
            $timestamps = MyActiveRecord::convertSearchDateInToTimestamp($datesArray);
            $createdAtFrom = $timestamps['fromDate'];
            $createdAtTo = $timestamps['toDate'];
            if (!empty($createdAtFrom) && !empty($createdAtTo) && ($createdAtFrom <= $createdAtTo)) {
                $query->andFilterWhere(['between', 'date_added', $createdAtFrom, $createdAtTo]);
            } else {
                $query->where('0=1');
            }
        }
        $query->andFilterWhere(['like', 'user_type', $this->user_type])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'password', $this->password])
                ->andFilterWhere(['like', 'token', $this->token])
                ->andFilterWhere(['like', 'token_ip_address', $this->token_ip_address])
                ->andFilterWhere(['like', 'hint', $this->hint])
                ->andFilterWhere(['like', 'contact_name', $this->contact_name])
                ->andFilterWhere(['like', 'company_name', $this->company_name])
                ->andFilterWhere(['like', 'address_one', $this->address_one])
                ->andFilterWhere(['like', 'address_two', $this->address_two])
                ->andFilterWhere(['like', 'town', $this->town])
                ->andFilterWhere(['like', 'county', $this->county])
                ->andFilterWhere(['like', 'postcode', $this->postcode])
                ->andFilterWhere(['like', 'telephone', $this->telephone])
                ->andFilterWhere(['like', 'mobile', $this->mobile])
                ->andFilterWhere(['like', 'fax', $this->fax])
                ->andFilterWhere(['like', 'website', $this->website])
                ->andFilterWhere(['like', 'other', $this->other])
                ->andFilterWhere(['like', 'newsletter', $this->newsletter])
                ->andFilterWhere(['like', 'user_lat', $this->user_lat])
                ->andFilterWhere(['like', 'user_long', $this->user_long])
                ->andFilterWhere(['like', 'category', $this->category])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'directory_status', $this->directory_status])
                ->andFilterWhere(['like', 'directory_link', $this->directory_link])
                ->andFilterWhere(['like', 'meta_title', $this->meta_title])
                ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
                ->andFilterWhere(['like', 'meta_description', $this->meta_description])
                ->andFilterWhere(['like', 'business_plus', $this->business_plus])
                ->andFilterWhere(['like', 'allow_buy_now', $this->allow_buy_now]);

//     $rawquery =   $query->createCommand()->getRawSql(); 
//     print_r($rawquery);
//     exit;
        return $dataProvider;
    }

}
