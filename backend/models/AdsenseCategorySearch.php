<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AdsenseCategory;

/**
 * AdsenseCategorySearch represents the model behind the search form of `backend\models\AdsenseCategory`.
 */
class AdsenseCategorySearch extends AdsenseCategory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ad_location','category_id' , 'adsense_code_1', 'adsense_code_2', 'adsense_code_3'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdsenseCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('categories');
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id
        ]);

        $query->andFilterWhere(['like', 'ad_location', $this->ad_location])
            ->andFilterWhere(['like', 'adsense_code_1', $this->adsense_code_1])
            ->andFilterWhere(['like', 'adsense_code_2', $this->adsense_code_2])
            ->andFilterWhere(['like', 'adsense_code_3', $this->adsense_code_3])
            ->andFilterWhere(['like', 'classified_categories.category', $this->category_id]);

        return $dataProvider;
    }
}
