<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\base\CarMakes */

$this->title = 'Create Car Makes';
$this->params['breadcrumbs'][] = ['label' => 'Car Makes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-makes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
