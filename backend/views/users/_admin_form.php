<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\LocationsTowns;
use common\models\LocationsCounties;
use common\models\MarketResearch;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <h3>Contact Details</h3>
            <?= $form->field($model, 'contact_name')->textInput()->label('Full Name') ?>
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <h3>Account Details</h3>
            <?= $form->field($model, 'user_type')->dropDownList(
                [ 'admin' => 'Admin',
                ]) ?>
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive']) ?>

            <?php if ($model->id == Yii::$app->user->identity->id) : ?>

                <h3>Change Your Password</h3>
                <?= $form->field($model, 'change_password', [
                    'template' => '{input}',
                ])->checkbox()->label('Change Password'); ?>
                <div class="change-password hidden">
                    <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Enter New Password', 'value' => ''])->label('New Password') ?>
                </div>

            <?php endif; ?>
        </div>
        
    </div>
    <div class="form-group text-center">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
