<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\MyActiveRecord;
//use jino5577\daterangepicker\DateRangePicker; // add widget 
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$userTitle = Yii::$app->getRequest()->getQueryParam('UsersSearch');

$userTitle['user_type'] =  (isset($userTitle['user_type']) && !empty($userTitle['user_type'])) ? $userTitle['user_type'] : 'private';

$this->title = Yii::t('app', ucfirst($userTitle['user_type']).' Users');
$this->params['breadcrumbs'][] = $this->title;

//echo $model->date_added;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create '.ucfirst($userTitle['user_type']).' Users'), ['create','user_type' => $userTitle['user_type']], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],
            'contact_name',
            'username',
                ['label' => 'Account Balance',
                'attribute' => 'account_balance',
                'format' => 'html',
                'value' => function($data) {
                    return (!empty($data->account_balance)) ? '&euro;' . number_format($data->account_balance, 2) : '';
                },
            ],
            'telephone',
            'email:email',
                [
                // the attribute
                'attribute' => 'date_added',
                // format the value
                'value' => function ($model) {
                    return (!empty($model->date_added)) ? MyActiveRecord::convertTimeStampIntoDate($model->date_added) : '';
                },
                // some styling? 
                'headerOptions' => [
                //'class' => 'col-md-2'
                ],
                // here we render the widget
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_added',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'timePicker' => true,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'd-m-Y'
                        ]
                    ]
                ])
            ],[
                'attribute'=>'status',
                'filter' =>Html::activeDropDownList($searchModel, 'status', array("active"=>"Active","inactive"=>"InActive"),['class'=>'form-control','prompt' => 'Select Status']),
            ],
//            ['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'text-align:center;width: 100px !important;'],
                'template' => '{update} {delete}',
//                'buttons' => [
//                    'deposit' => function ($url, $model, $key) {
//                        return Html::a('<span class="glyphicon glyphicon-circle-arrow-down"></span>', ['deposit', 'id' => $model->id], [
//                                    'title' => Yii::t('yii', 'Deposit'),
//                        ]);
//                    },
//                    'purchase' => function ($url, $model, $key) {
//                        return Html::a('<span class="glyphicon glyphicon-shopping-cart"></span>', ['deposit', 'id' => $model->id], [
//                                    'title' => Yii::t('yii', 'Purchases'),
//                        ]);
//                    },
//                ],
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>