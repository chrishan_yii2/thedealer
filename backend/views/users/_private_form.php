<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\LocationsTowns;
use common\models\LocationsCounties;
use common\models\MarketResearch;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="users-form">
    <div class="clearfix">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-6">
            <h3>Contact Details</h3>
            <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            <h3>Address Details</h3>
            <?= $form->field($model, 'address_one')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'address_two')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'county_id')->dropDownList(
                ArrayHelper::map(LocationsCounties::find()->all(),'id','county'),
                [
                    'prompt' => 'Select Country',
                    'onchange'=> '$.post("town-lists?id='.'"+$(this).val(),function(data){
                                            $("select#users-town_id").html(data);}
                                            );'
                ]
                )->label('County') ?>

            <?= $form->field($model, 'town_id')->dropDownList(
                ArrayHelper::map(LocationsTowns::find()->all(),'id','town'),
                ['prompt' => 'Select Town']
                )->label('Town') ?>
            <?= $form->field($model, 'postcode')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <h3>Account Details</h3>
            <?= $form->field($model, 'user_type')->dropDownList(
                                                    [ 'private' => 'Private',
                                                    ]) ?>
            <?= $form->field($model, 'newsletter')->dropDownList([ 'no' => 'No', 'yes' => 'Yes', ]) ?>

            <?= $form->field($model, 'marketing_id')->dropDownList(
                ArrayHelper::map(MarketResearch::find()->all(),'id','market_research'),
                ['prompt' => 'Please Select']
            )->label('How user they found us') ?>
            <h3>Login Details</h3>
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive']) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-block']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>
    </div>

</div>
