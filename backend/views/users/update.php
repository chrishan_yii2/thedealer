<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
if ($model->user_type == \common\models\Users::ROLE_BUSINESS){
    $name = $model->company_name;
} else {
    $name = $model->contact_name;
}

$this->title = Yii::t('app', 'Update User: ' . $name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];

$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="users-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_'.$model->user_type.'_form', [
        'model' => $model,
    ]) ?>

</div>
