<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Users */

$userType = Yii::$app->getRequest()->getQueryParam('user_type');

$userType = (isset($userType) && !empty($userType)) ? $userType : 'admin';

$this->title = Yii::t('app', 'Create '.ucfirst($userType).' User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title ;


?>
<div class="users-create">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= $this->render('_'.$userType.'_form', [
        'model' => $model,
    ]) ?>

</div>
