<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_type',
            'email:email',
            'username',
            'password',
            'token',
            'token_ip_address',
            'hint',
            'contact_name',
            'company_name',
            'image_count',
            'address_one',
            'address_two',
            'town_id',
            'town',
            'county_id',
            'county',
            'postcode',
            'telephone',
            'mobile',
            'fax',
            'website',
            'marketing_id',
            'other',
            'account_balance',
            'newsletter',
            'user_lat',
            'user_long',
            'directory_cat_id',
            'category',
            'description:ntext',
            'status',
            'directory_status',
            'directory_link',
            'directory_end_date',
            'date_added',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'seo_id',
            'business_plus',
            'allow_buy_now',
        ],
    ]) ?>

</div>
