<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MarketResearch */

$this->title = 'Create Market Research';
$this->params['breadcrumbs'][] = ['label' => 'Market Researches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="market-research-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
