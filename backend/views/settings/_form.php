<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\LocationsTowns;
use common\models\LocationsCounties;
/* @var $this yii\web\View */
/* @var $model backend\models\base\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'copyright')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_two')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'county_id')->dropDownList(
            ArrayHelper::map(LocationsCounties::find()->all(), 'id', 'county'), [
        'prompt' => 'Select Country',
        'onchange' => '$.post("town-lists?id=' . '"+$(this).val(),function(data){
                                            $("select#users-town_id").html(data);}
                                            );'
            ]
    )->label('County')
    ?>

    <?=
    $form->field($model, 'town_id')->dropDownList(
            ArrayHelper::map(LocationsTowns::find()->all(), 'id', 'town'), ['prompt' => 'Select Town']
    )->label('Town')
    ?>



    <?= $form->field($model, 'postcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'analytics_code')->textarea(['rows' => 6]) ?>

    <div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
