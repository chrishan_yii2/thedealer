<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\base\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'copyright',
            'email:email',
            'company_name',
            'address_one',
            //'address_two',
            //'town_id',
            //'county_id',
            //'postcode',
            //'telephone',
            //'meta_title',
            //'meta_keywords',
            //'meta_description',
            //'theme',
            //'analytics_code:ntext',
            //'date_added',

            ['class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'text-align:center;width: 100px !important;'],
                'template' => '{update}',
            ],
        ],
    ]); ?>
</div>
