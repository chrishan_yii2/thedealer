<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\MyActiveRecord;
use kartik\daterange\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ClassifiedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$title = (isset($_GET['ClassifiedSearch']['ad_type']) && !empty($_GET['ClassifiedSearch']['ad_type'])) ? $_GET['ClassifiedSearch']['ad_type'] : "";



$this->title = ucfirst($title)." ".Yii::t('app', 'Classified Listings');
$this->params['breadcrumbs'][] = Yii::t('app', 'Classified Listings');



?>
<div class="classified-listings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Classified Listings'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute' => 'parent_id',
                'label' => 'Category',
                'value' => 'classifiedCat.category'
            ],
            [
                'attribute'=>'status',
                'filter' =>Html::activeDropDownList($searchModel, 'status', array("active"=>"Active","inactive"=>"InActive"),['class'=>'form-control','prompt' => 'Select Status']),
            ],
//            'bump',
//            'bump_count',
            [
                // the attribute
                'attribute' => 'start_date',
                // format the value
                'value' => function ($model) {
                    return (!empty($model->start_date)) ? MyActiveRecord::convertTimeStampIntoDate($model->start_date) : '';
                },
                // some styling?
                'headerOptions' => [
                    //'class' => 'col-md-2'
                ],
                // here we render the widget
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'timePicker' => true,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'd-m-Y'
                        ]
                    ]
                ])
            ],
            [
                // the attribute
                'attribute' => 'end_date',
                // format the value
                'value' => function ($model) {
                    return (!empty($model->end_date)) ? MyActiveRecord::convertTimeStampIntoDate($model->end_date) : '';
                },
                // some styling?
                'headerOptions' => [
                    //'class' => 'col-md-2'
                ],
                // here we render the widget
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'end_date',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'timePicker' => true,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'd-m-Y'
                        ]
                    ]
                ])
            ],

//            ['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'text-align:center;width: 100px !important;'],
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
