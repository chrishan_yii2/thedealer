<?php
/**
 * Created by PhpStorm.
 * User: abhi_
 * Date: 03-11-2018
 * Time: 15:41
 */


echo \kato\DropZone::widget([
    'options' => [
        'maxFilesize' => '2',
    ],
    'clientEvents' => [
        'complete' => "function(file){console.log(file)}",
        'removedfile' => "function(file){alert(file.name + ' is removed')}"
    ],
]);

?>