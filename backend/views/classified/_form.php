<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ClassifiedCategories;
use common\models\LocationsCounties;
use common\models\LocationsTowns;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedListings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classified-listings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
            $form->field($model, 'ad_type')
            ->dropDownList([
                'private' => 'Private',
                'business' => 'Business',
                    ], [
                'prompt' => 'Ad Type',
                'onchange' => '
                    $.post("' . Yii::$app->urlManager->createUrl('classified/users-list?user_type=') . '"+$(this).val(),function(data){
                        $("select#classifiedlistings-user_id").html(data);
                        });'
            ])
    ?>
    <?php
        $name = "";
        if(isset($model->user_id) && !empty($model->user_id)){
            $user = \common\models\Users::findIdentity($model->user_id);
            $name = $user->contact_name;
        }
        
        
    ?>
    <?=
    $form->field($model, 'user_id')->widget(Select2::classname(), [
        'initValueText' => $name, 
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => 'Enter Name of User'],
        'pluginOptions' => [
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => Yii::$app->urlManager->createUrl("classified/users-list-ajax"),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { 
                    var type = $("#classifiedlistings-ad_type").val();
                    return {
                        q:params.term,
                        ad_type:type
                    }; 
                }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(users) { console.log(users); return users.text; }'),
            'templateSelection' => new JsExpression('function (users) { return users.text; }'),
        ],
    ])->label('User');
    ?>

    

    <?=
    $form->field($model, 'county_id')->dropDownList(
            ArrayHelper::map(
                    LocationsCounties::find()->all(), 'id', 'county'), [
        'prompt' => 'Select Country',
        'onchange' => '$.post("/site/lists?id=' . '"+$(this).val(),function(data){
            $("select#classifiedlistings-town_id").html(data);}
        );'
    ])->label('County')
    ?>

    <?= $form->field($model, 'town_id')->dropDownList(ArrayHelper::map(LocationsTowns::find()->all(), 'id', 'town'), ['prompt' => 'Select Town'])->label('Town') ?>

    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])->label("Mobile *") ?>

    <?= $form->field($model, 'other')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label("Email *") ?>

    <?= $form->field($model, 'item_status')->dropDownList(['for sale' => 'For sale', 'wanted' => 'Wanted', 'sold' => 'Sold', 'to let' => 'To let', 'available' => 'Available',], ['prompt' => '']) ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'featured')->dropDownList(['no' => 'No', 'yes' => 'Yes',], ['prompt' => '']) ?>

    <?= $form->field($model, 'buy_now_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label("Title *") ?>

    <?=
    $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(
                    ClassifiedCategories::find()->getCategories()->all(), 'id', 'category'
            ), [
        'prompt' => 'Parent Category',
        'onchange' => '$.post("' . Yii::$app->urlManager->createUrl('classified-categories/child-cats?id=') . '"+$(this).val(),function(data){
                                                    $("select#classifiedlistings-classified_cat_id").html(data);
                                                    });'
    ])->label('Select Parent Category *');
    ?>

    <?=
    $form->field($model, 'classified_cat_id')->dropDownList(ArrayHelper::map(
                    ClassifiedCategories::find()->all(), 'id', 'category'
            ), [
        'prompt' => 'Select Child Category',
    ])->label('Child Category *');
    ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label("Description *") ?>

    <?= $form->field($model, 'currency')
                                    ->dropDownList([
                                            'EUR' => "€",
                                            'GBP' => "£",
                                            'FREE' => 'FREE',
                                    ])->label('Select currency *')?>

<?= $form->field($model, 'price')->textInput(['maxlength' => true])->label("Price *") ?>

    <div class="form-group">
<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
