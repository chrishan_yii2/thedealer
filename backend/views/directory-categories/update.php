<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\base\DirectoryCategories */

$this->title = 'Update Directory Categories: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Directory Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="directory-categories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
