<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\base\DirectoryCategories */

$this->title = 'Create Directory Categories';
$this->params['breadcrumbs'][] = ['label' => 'Directory Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directory-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
