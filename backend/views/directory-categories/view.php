<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\base\DirectoryCategories */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Directory Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directory-categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category',
            'status',
            'date_added',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'slug',
        ],
    ]) ?>

</div>
