<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DirectoryCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Directory Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directory-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Directory Categories', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'category',
            [
                'attribute'=>'status',
                'filter' =>Html::activeDropDownList($searchModel, 'status', array("active"=>"Active","inactive"=>"InActive"),['class'=>'form-control','prompt' => 'Select Status']),
            ],
//            ['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'text-align:center;width: 100px !important;'],
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
