<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\base\LocationsTowns */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="locations-towns-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'county_id')->dropDownList(
            yii\helpers\ArrayHelper::map(\common\models\LocationsCounties::find()->all(),'id','county'),
            ['prompt' => 'Select Country']
            )->label('County *') ?>

    <?= $form->field($model, 'town')->textInput(['maxlength' => true])->label('Town *') ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
