<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\base\LocationsTowns */

$this->title = 'Create Locations Towns';
$this->params['breadcrumbs'][] = ['label' => 'Locations Towns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locations-towns-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
