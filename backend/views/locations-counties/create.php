<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\base\LocationsCounties */

$this->title = 'Create Locations Counties';
$this->params['breadcrumbs'][] = ['label' => 'Locations Counties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locations-counties-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
