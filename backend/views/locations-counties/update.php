<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\base\LocationsCounties */

$this->title = 'Update Locations Counties: ' . $model->county;
$this->params['breadcrumbs'][] = ['label' => 'Locations Counties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->county, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="locations-counties-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
