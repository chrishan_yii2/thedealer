<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ClassifiedCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Classified Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Classified Categories', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'category',
            'order',
            [
                'attribute'=>'status',
                'filter' =>Html::activeDropDownList($searchModel, 'status', array("active"=>"Active","inactive"=>"InActive"),['class'=>'form-control','prompt' => 'Select Status']),
            ],
//            'car_listing',
            //'description:ntext',
            //'banner_code',
            //'code',
            //'date_added',
            //'meta_title',
            //'meta_keywords',
            //'meta_description',
            //'slug',

            ['class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'text-align:center;width: 100px !important;'],
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    
</div>
