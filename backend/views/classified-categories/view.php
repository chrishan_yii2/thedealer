<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedCategories */

$this->title = $model->category;
$this->params['breadcrumbs'][] = ['label' => 'Classified Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'category',
            'id',
            'parent_id',
            'temp_parent_id',
            'temp_parent_id_2',
            'image_count',
            'car_listing',
            'description:ntext',
            'banner_code',
            'status',
            'code',
            'order',
            'date_added',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'slug',
        ],
    ]) ?>

</div>
