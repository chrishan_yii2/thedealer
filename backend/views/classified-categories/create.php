<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedCategories */

$this->title = 'Create Classified Categories';
$this->params['breadcrumbs'][] = ['label' => 'Classified Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
