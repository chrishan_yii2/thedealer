<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ClassifiedCategories;

/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classified-categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')
                ->dropDownList(ArrayHelper::map(
                        ClassifiedCategories::find()->getCategories()->all(),
                        'id', 'category'
                ),[
                        'prompt' => 'Parent Category',
                        'onchange' => '
                                $.post("'.Yii::$app->urlManager->createUrl('classified-categories/child-cats?id=').'"+$(this).val(),function(data){
                                    $("select#classifiedcategories-temp_parent_id").html(data);
                                    });'
                ])->label('Select Parent Category'); ?>

    <?= $form->field($model, 'temp_parent_id')
                ->dropDownList(ArrayHelper::map(
                    ClassifiedCategories::find()->all(),
                    'id', 'category'
                ),[
                    'prompt' => 'Select Child Category',
                    'onchange' => '$.post("/admin/classified-categories/sub-child-cats?id='.'"+$(this).val(),function(data){$("select#classifiedcategories-temp_parent_id_2").html(data);});'
                ])->label('Child Category'); ?>

    <?= $form->field($model, 'temp_parent_id_2')
                ->dropDownList(ArrayHelper::map(
                    ClassifiedCategories::find()->all(),
                    'id', 'category'
                ),[
                    'prompt' => 'Select Sub Child Category',
                ])->label('Sub Child Category'); ?>

    <?= $form->field($model, 'image_count')->textInput() ?>

    <?= $form->field($model, 'car_listing')->dropDownList([ 'no' => 'No', 'yes' => 'Yes', ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'banner_code')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>

    <?= $form->field($model, 'code')->textInput() ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
