<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ClassifiedCategories;

/* @var $this yii\web\View */
/* @var $model backend\models\AdsenseCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adsense-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(
                ClassifiedCategories::find()->all(),
                'id', 'category'
            ),[
                'prompt' => 'Select Category',
            ])->label('Category') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'ad_location')->dropDownList(
                    array(
                        'Details Page' => 'Details Page',
                        'Listing Page' => 'Listing Page',
                        'Home Page' => 'Home Page',
                        ),[
                'prompt' => 'Select Location',
            ])->label('Page') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'adsense_code_1')->textarea(['rows' => 6])->label('First Ad') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'adsense_code_2')->textarea(['rows' => 6])->label('Second Ad') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'adsense_code_3')->textarea(['rows' => 6])->label('Third Ad') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
