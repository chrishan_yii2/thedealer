<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\AdsenseCategory */

$this->title = 'Create Adsense Category';
$this->params['breadcrumbs'][] = ['label' => 'Adsense Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adsense-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
