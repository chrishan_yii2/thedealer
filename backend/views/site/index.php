<?php

/* @var $this yii\web\View */

$this->title = 'TheDealer :: DashBoard';
?>
<div class="site-index">

    <div class="jumbotron">
        <h3>Welcome back <?= Yii::$app->user->identity->contact_name ?></h3>
        <p class="lead">
            You have successfully logged in to your admin control panel.
        </p>
        <div class="">
            <a href="/admin/classified/create" class="btn btn-primary">Create Ads</a>
            <a href="/admin/users/index?UsersSearch[user_type]=admin" class="btn btn-primary">View Users</a>
        </div>
    </div>
</div>
