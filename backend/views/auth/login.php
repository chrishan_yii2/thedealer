<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

?>


<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>


<div class="form-group has-feedback">
    <?= $form->field($model, 'username')->textInput(['autofocus' => true,'label' => false]) ?>
</div>
<div class="form-group has-feedback">
    <?= $form->field($model, 'password')->passwordInput() ?>
</div>
<div class="row">
    <div class="col-xs-8">
        <div class="checkbox icheck">
            <label class="">
                <div class="icheckbox_square-blue" style="position: relative;" aria-checked="false" aria-disabled="false">

                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                    <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;">
                    </ins>
                </div>
            </label>
        </div>
    </div>
</div>








<div class="form-group">
    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
