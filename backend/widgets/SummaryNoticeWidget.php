<?php

namespace backend\widgets;

use yii\base\Widget;

/**
 * Description of SummaryNoticeWidget
 *
 * @author Rohit Gupta
 */
class SummaryNoticeWidget extends Widget
{
    public $actions;
    public $customActions;
    public $messages;
    public $actionCssClass;
    
    public $insertMessage = '<strong>Success!</strong> Record inserted successfully.';
    public $updateMessage = '<strong>Success!</strong> Record updated successfully.';
    public $deleteMessage = '<strong>Success!</strong> Record deleted successfully.';
    
    public function init(  )
    {
        parent::init();
        
        if (!is_array($this->actions)) {
            $this->actions = ['insert', 'update', 'delete', 'pagecantdelete'];
        }
        
        if (is_array($this->customActions)) {
            $this->actions = \yii\helpers\ArrayHelper::merge($this->actions, $this->customActions);
        }
        
        //Default Messages
        if (!isset($this->messages['insert']) && in_array('insert', $this->actions)) {
            $this->messages['insert'] = $this->insertMessage;
        }
        
        if (!isset($this->messages['update']) && in_array('update', $this->actions)) {
            $this->messages['update'] = $this->updateMessage;
        }
        
        if (!isset($this->messages['delete']) && in_array('delete', $this->actions)) {
            $this->messages['delete'] = $this->deleteMessage;
        }
        
        //CSS Class
        if (!isset($this->actionCssClass['insert']) && in_array('insert', $this->actions)) {
            $this->actionCssClass['insert'] = 'alert-success';
        }
        
        if (!isset($this->actionCssClass['update']) && in_array('update', $this->actions)) {
            $this->actionCssClass['update'] = 'alert-success';
        }
        
        if (!isset($this->actionCssClass['delete']) && in_array('delete', $this->actions)) {
            $this->actionCssClass['delete'] = 'alert-success';
        }
    }
    
    public function run(  )
    {
        foreach ($this->actions as $action) {
            
            if (\Yii::$app->getRequest()->get($action) == 1) {
                
                $message = $this->messages[$action];
                $cssClass = isset($this->actionCssClass[$action]) ? $this->actionCssClass[$action] : 'alert-success';
                
                break;
            }
        }
        
        return (!empty($message)) ? $this->render('summary_notice', [
                    'message' => $message,
                    'cssClass' => $cssClass,
                ]) :  '';
    }
}