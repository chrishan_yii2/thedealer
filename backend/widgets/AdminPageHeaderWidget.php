<?php

namespace backend\widgets;

use yii\base\Widget;

/**
 *
 * @author Rohit Gupta
 */
class AdminPageHeaderWidget extends Widget
{
    public function init(  )
    {
        parent::init();
    }
    
    public function run(  )
    {
        return $this->render('admin_page_header', [
            
        ]);
    }
}