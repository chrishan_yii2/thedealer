<?php
/* @var $this \backend\widgets\AdminMainMenuWidget */
?>
<!-- BEGIN SIDEBAR MENU -->
<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<ul class="sidebar-menu" data-widget="tree">
    <?php
    $menuCounter = 0;
    foreach ($menuConfig as $controllerId => $controllerConfig):
        $startClass = ( ++$menuCounter == 1) ? 'start' : '';
        $link = isset($controllerConfig['link']) ? $controllerConfig['link'] : 'javascript:;';
        $hasSubMenu = (isset($controllerConfig['subMenus']) && count($controllerConfig['subMenus']) > 0) ? TRUE : FALSE;
        $controllerActive = (Yii::$app->controller->id == $controllerId) ? TRUE : FALSE;
        $displayArrow = $hasSubMenu ? '' : 'hide';

        if (isset($controllerConfig['hidden']) && $controllerConfig['hidden']) {
            continue;
        }

        $controllerActiveClass = ($controllerActive) ? ' active' : '';
        $controllerOpenClass = ($controllerActive && $hasSubMenu) ? ' open' : '';
        $displayArrowOpen = ($controllerActive) ? ' open' : '';
        ?>
        <li class="treeview <?= $startClass ?><?= $controllerActiveClass ?><?= $controllerOpenClass ?>">
            <a href="<?= $link ?>">
                <i class="<?= $controllerConfig['iconClass'] ?>"></i>
                <span class="title"><?= $controllerConfig['title'] ?></span>
                <span class="arrow<?= $displayArrowOpen ?> <?= $displayArrow ?>"></span>
            </a>
            <?php if ($hasSubMenu): ?>
                <ul class="sub-menu treeview-menu ">

                    <?php
                    foreach ($controllerConfig['subMenus'] as $actionId => $actionConfig):

                        $subMenuLink = \yii\helpers\Url::toRoute($controllerId . '/' . $actionId);
                        $actionActive = ($controllerActive && Yii::$app->controller->action->id == $actionId) ? TRUE : FALSE;
                        $actionActiveClass = ($actionActive) ? 'active' : '';

                        if (isset($actionConfig['hidden']) && $actionConfig['hidden']) {
                            continue;
                        }
                        ?>
                        <li class="<?= $actionActiveClass ?>">
                            <a href="<?= $subMenuLink ?>">
                                <i class="<?= $actionConfig['iconClass'] ?>"></i>
                        <?= $actionConfig['title'] ?></a>
                        </li>
                <?php endforeach; ?>
                </ul>
        <?php endif; ?>
        </li>
<?php endforeach; ?>
</ul>
<!-- END SIDEBAR MENU -->