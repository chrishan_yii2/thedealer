<?php
use yii\widgets\Breadcrumbs;

$pageTitle = isset(\Yii::$app->params['template']['page_title']) ? \Yii::$app->params['template']['page_title'] : '';
$pageSubTitle = isset(\Yii::$app->params['template']['page_sub_title']) ? \Yii::$app->params['template']['page_sub_title'] : '';
?>
            <!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?= $pageTitle ?> <small><?= $pageSubTitle ?></small></h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
            <?= Breadcrumbs::widget([
                'options' =>  [
                    'class' => 'page-breadcrumb breadcrumb'
                ],
                'itemTemplate' => '<li>{link}<i class="fa fa-circle"></i></li>',
                'activeItemTemplate' => '<li><a href="#">{link}</a></li>',
                'homeLink' => [
                    'label' => 'Home',
                    'url' => \yii\helpers\Url::toRoute(Yii::$app->homeUrl)
                ],
                'links' => isset(\Yii::$app->params['breadcrumbs']) ? \Yii::$app->params['breadcrumbs'] : [],
            ]) ?>
            <!-- END PAGE BREADCRUMB -->