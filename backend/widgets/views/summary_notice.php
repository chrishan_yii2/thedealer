<?php
/*@var $this \backend\widgets\SummaryNoticeWidget */
?>

<div class="alert <?= $cssClass ?> alert-dismissable">
    <button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
    <?= $message ?>
</div>

