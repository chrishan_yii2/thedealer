<?php

namespace backend\widgets;

use Yii;
use yii\base\Widget;

/**
 * Description of AdminMainMenuWidget
 *
 * @author Rohit Gupta
 */
class AdminMainMenuWidget extends Widget
{
    public $configFilePath;
    private $menuConfig;
    
    public function init(  )
    {
        parent::init();
        
        if (empty($this->configFilePath)) {
            $this->configFilePath = Yii::getAlias('@backend') . '/config/menu.php';
        }
        
        $this->menuConfig = require($this->configFilePath);
    }
    
    public function run(  )
    {
        return $this->render('admin_main_menu', [
            'menuConfig' => $this->menuConfig
        ]);
    }
}
