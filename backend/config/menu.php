<?php
use yii\helpers\Url;
use common\models\Users;

return [
    'classified' => [
        'hidden' => FALSE,
        'title' => 'Online Ads',
        'iconClass' => 'fa fa-dot-circle-o',
        'subMenus' => [
            'index?ClassifiedSearch[ad_type]='.Users::ROLE_PRIVATE => [
                'title' => 'Private Ads',
                'iconClass' => 'fa fa-list',
            ],
            'index?ClassifiedSearch[ad_type]='.Users::ROLE_BUSINESS => [
                'title' => 'Business Ads',
                'iconClass' => 'fa fa-list',
            ],
            'create' => [
                'title' => 'Add Online Ad',
                'iconClass' => 'fa fa-plus',
                'hidden' => FALSE
            ]
        ]
    ],
    'users' => [
        'hidden' => FALSE,
        'title' => 'Users',
        'iconClass' => 'fa fa-dot-circle-o',
        'subMenus' => [
            'index?UsersSearch[user_type]='.Users::ROLE_ADMIN => [
                'title' => 'Admin Users',
                'iconClass' => 'fa fa-list',
            ],
            'index?UsersSearch[user_type]='.Users::ROLE_BUSINESS => [
                'title' => 'Business Users',
                'iconClass' => 'fa fa-list',
            ],
            'index?UsersSearch[user_type]='.Users::ROLE_PRIVATE => [
                'title' => 'Private Users',
                'iconClass' => 'fa fa-list',
            ],
        ]
    ],
    'locations-counties' => [
        'hidden' => FALSE,
        'title' => 'Counties Manager',
        'iconClass' => 'fa fa-globe',
        'subMenus' => [
            'index' => [
                'title' => 'Summary',
                'iconClass' => 'fa fa-list',
            ],
            'create' => [
                'title' => 'Create',
                'iconClass' => 'fa fa-plus',
                'hidden' => FALSE
            ]
        ]
    ],
    'locations-towns' => [
        'hidden' => FALSE,
        'title' => 'Towns Manager',
        'iconClass' => 'fa fa-map-marker',
        'subMenus' => [
            'index' => [
                'title' => 'Summary',
                'iconClass' => 'fa fa-list',
            ],
            'create' => [
                'title' => 'Create',
                'iconClass' => 'fa fa-plus',
                'hidden' => FALSE
            ]
        ]
    ],
    'directory-categories' => [
        'hidden' => FALSE,
        'title' => 'Directory Categories',
        'iconClass' => 'fa fa-dot-circle-o',
        'subMenus' => [
            'index' => [
                'title' => 'Summary',
                'iconClass' => 'fa fa-list',
            ],
            'create' => [
                'title' => 'Create',
                'iconClass' => 'fa fa-plus',
                'hidden' => FALSE
            ]
        ]
    ],

    'classified-categories' => [
        'hidden' => FALSE,
        'title' => 'Classified Categories',
        'iconClass' => 'fa fa-dot-circle-o',
        'subMenus' => [
            'index' => [
                'title' => 'All Categories',
                'iconClass' => 'fa fa-list',
            ],
            'create' => [
                'title' => 'Add New Category',
                'iconClass' => 'fa fa-plus',
                'hidden' => FALSE
            ]
        ]
    ],
    'prices' => [
        'hidden' => FALSE,
        'title' => 'Prices',
        'iconClass' => 'fa fa-money',
        'subMenus' => [
            'index' => [
                'title' => 'Summary',
                'iconClass' => 'fa fa-list',
            ],
            'create' => [
                'title' => 'Create',
                'iconClass' => 'fa fa-plus',
                'hidden' => FALSE
            ]
        ]
    ],
     'adsense-category' => [
        'hidden' => FALSE,
        'title' => 'Adsense',
        'iconClass' => 'fa fa-credit-card',
        'subMenus' => [
            'index' => [
                'title' => 'Summary',
                'iconClass' => 'fa fa-list',
            ],
            'create' => [
                'title' => 'Create',
                'iconClass' => 'fa fa-plus',
                'hidden' => FALSE
            ]
        ]
    ],
    'car-makes' => [
        'hidden' => FALSE,
        'title' => 'Cars makes & models',
        'iconClass' => 'fa fa-car',
        'subMenus' => [
            'index' => [
                'title' => 'Summary',
                'iconClass' => 'fa fa-list',
            ],
            'create' => [
                'title' => 'Create',
                'iconClass' => 'fa fa-plus',
                'hidden' => FALSE
            ]
        ]
    ],
    'settings' => [
        'hidden' => FALSE,
        'title' => 'System Setting',
        'iconClass' => 'fa fa-cogs',
        'subMenus' => [
            'index' => [
                'title' => 'Summary',
                'iconClass' => 'fa fa-list',
            ],
        ]
    ],
    'market-research' => [
        'hidden' => FALSE,
        'title' => 'Market Research',
        'iconClass' => 'fa fa-cogs',
        'subMenus' => [
            'index' => [
                'title' => 'Summary',
                'iconClass' => 'fa fa-list',
            ],
            'create' => [
                'title' => 'Create',
                'iconClass' => 'fa fa-plus',
                'hidden' => FALSE
            ]
        ]
    ],
    
    
];
