<?php

use yii\db\Migration;

/**
 * Class m181016_115419_add_artical_column
 */
class m181016_115419_add_artical_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->db->CreateCommand("ALTER TABLE `articals` ADD `body` TEXT NOT NULL AFTER `title`;")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181016_115419_add_artical_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181016_115419_add_artical_column cannot be reverted.\n";

        return false;
    }
    */
}
