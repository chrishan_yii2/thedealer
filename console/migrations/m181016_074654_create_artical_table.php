<?php

use yii\db\Migration;

/**
 * Handles the creation of table `artical`.
 */
class m181016_074654_create_artical_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->db->CreateCommand("CREATE TABLE `realweb3_thedealer`.`articals` ( `id` INT NOT NULL AUTO_INCREMENT , `ads_id` INT NULL , `user_id` INT NOT NULL , `title` VARCHAR(255) NOT NULL , `is_comment_able` INT NOT NULL DEFAULT '1' , `created_at` INT NOT NULL , `updated_at` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articals');
    }
}
