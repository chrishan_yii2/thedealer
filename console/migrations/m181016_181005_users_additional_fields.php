<?php

use yii\db\Migration;

/**
 * Class m181016_181005_users_additional_fields
 */
class m181016_181005_users_additional_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'auth_key', $this->integer()->after('password'));
        $this->addColumn('users', 'created_at', $this->integer());
        $this->addColumn('users', 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181016_181005_users_additional_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181016_181005_users_additional_fields cannot be reverted.\n";

        return false;
    }
    */
}
