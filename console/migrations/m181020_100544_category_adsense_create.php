<?php

use yii\db\Migration;

/**
 * Class m181020_100544_category_adsense_create
 */
class m181020_100544_category_adsense_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->db->CreateCommand("CREATE TABLE `adsense_category` ( `id` INT NOT NULL AUTO_INCREMENT , `category_id` INT NOT NULL , `adsense_code` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->db->CreateCommand("DROP TABLE `adsense_category`")->execute();

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181020_100544_category_adsense_create cannot be reverted.\n";

        return false;
    }
    */
}
