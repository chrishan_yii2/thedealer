<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 * This is the model class for table "classified_categories".
 *
 * @property int $id
 * @property int $parent_id
 * @property int $temp_parent_id
 * @property int $temp_parent_id_2
 * @property int $image_count
 * @property string $category
 * @property string $car_listing
 * @property string $description
 * @property int $banner_code
 * @property string $status
 * @property int $code
 * @property int $order
 * @property int $date_added
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $slug
 *
 * @property ClassifiedListings[] $classifiedListings
 * @property ClassifiedListings[] $classifiedListings0
 */
class ClassifiedCategories extends \yii\db\ActiveRecord
{

    public $depth = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'classified_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'temp_parent_id', 'temp_parent_id_2', 'image_count', 'banner_code', 'code', 'order', 'date_added'], 'integer'],
            [['category', 'description', 'banner_code', 'code', 'slug'], 'required'],
            [['image_count', 'slug'], 'safe'],
            [['car_listing', 'description', 'status'], 'string'],
            [['category', 'slug'], 'string', 'max' => 50],
            [['meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'temp_parent_id' => 'Temp Parent ID',
            'temp_parent_id_2' => 'Temp Parent Id 2',
            'image_count' => 'Image Count',
            'category' => 'Category',
            'car_listing' => 'Car Listing',
            'description' => 'Description',
            'banner_code' => 'Banner Code',
            'status' => 'Status',
            'code' => 'Code',
            'order' => 'Order',
            'date_added' => 'Date Added',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'slug' => 'Slug',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ClassifiedCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClassifiedCategoriesQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassifiedListings()
    {
        return $this->hasMany(ClassifiedListings::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassifiedListings0()
    {
        return $this->hasMany(ClassifiedListings::className(), ['classified_cat_id' => 'id']);
    }

    public function getChildren()
    {
        return $this->hasMany(ClassifiedCategories::className(), ['parent_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(ClassifiedCategories::className(), ['id' => 'parent_id']);
    }
    
    /**
     * @return \yii\db\ActivQuery
     */
    public function getAdsenseCategory()
    {
        return $this->hasOne(\backend\models\base\AdsenseCategory::className(),['category_id' => 'id']);
    }
}
