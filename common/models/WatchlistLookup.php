<?php

namespace common\models;

use Yii;
use common\models\ClassifiedListings;
/**
 * This is the model class for table "watchlist_lookup".
 *
 * @property int $user_id
 * @property int $listing_id
 */
class WatchlistLookup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'watchlist_lookup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'listing_id'], 'required'],
            [['user_id', 'listing_id'], 'integer'],
            [['user_id', 'listing_id'], 'unique', 'targetAttribute' => ['user_id', 'listing_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'listing_id' => 'Listing ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassifiedListings()
    {
        return $this->hasOne(ClassifiedListings::className(), ['id' => 'listing_id']);
    }
}
