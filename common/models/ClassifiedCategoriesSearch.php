<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ClassifiedCategories;

/**
 * ClassifiedCategoriesSearch represents the model behind the search form of `common\models\ClassifiedCategories`.
 */
class ClassifiedCategoriesSearch extends ClassifiedCategories
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'temp_parent_id', 'temp_parent_id_2', 'image_count', 'banner_code', 'code', 'order', 'date_added'], 'integer'],
            [['category', 'car_listing', 'description', 'status', 'meta_title', 'meta_keywords', 'meta_description', 'slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClassifiedCategories::find()->where(['parent_id' => 0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);
        
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'temp_parent_id' => $this->temp_parent_id,
            'temp_parent_id_2' => $this->temp_parent_id_2,
            'image_count' => $this->image_count,
            'banner_code' => $this->banner_code,
            'code' => $this->code,
            'order' => $this->order,
            'date_added' => $this->date_added,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'car_listing', $this->car_listing])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'slug', $this->slug]);
        
        return $dataProvider;
    }

    public function actionLists($id) {
        $countPosts = LocationsTowns::find()
            ->where(['county_id' => $id])
            ->count();
        $posts = LocationsTowns::find()
            ->where(['county_id' => $id])
            ->all();
        if ($countPosts > 0) {
            echo '<option> Select Town </option>';
            foreach ($posts as $post) {
                echo '<option value="'.$post->id.'">'.$post->town.'</option>';
            }
        }
        else{
            echo '<option> - </option>';
        }
    }

}
