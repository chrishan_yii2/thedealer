<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "market_research".
 *
 * @property int $id
 * @property string $market_research
 * @property string $status
 * @property int $date_added
 */
class MarketResearch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'market_research';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['market_research'], 'required'],
            [['status'], 'string'],
            [['date_added'], 'integer'],
            [['market_research'], 'string', 'max' => 100],
            ['market_research', 'unique', 'targetClass' => '\common\models\MarketResearch', 'message' => 'Record is already exist.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'market_research' => 'Market Research',
            'status' => 'Status',
            'date_added' => 'Date Added',
        ];
    }
}
