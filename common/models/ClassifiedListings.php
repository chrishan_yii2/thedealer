<?php

namespace common\models;
use common\models\WatchlistLookup;
use Yii;

/**
 * This is the model class for table "classified_listings".
 *
 * @property int $id
 * @property int $parent_id
 * @property int $classified_cat_id
 * @property string $sub_category
 * @property int $user_id
 * @property int $image_count
 * @property string $ad_type
 * @property string $title
 * @property string $description
 * @property string $search_options
 * @property string $currency
 * @property string $price
 * @property string $search_price
 * @property string $price_suffix
 * @property int $duration
 * @property int $town_id
 * @property int $county_id
 * @property string $item_status
 * @property string $telephone
 * @property string $email
 * @property string $mobile
 * @property string $other
 * @property string $status
 * @property int $order
 * @property int $bump
 * @property int $bump_count
 * @property string $featured
 * @property int $date_sold
 * @property int $views
 * @property int $start_date
 * @property int $end_date
 * @property int $make_id
 * @property string $auto_make
 * @property int $model_id
 * @property string $auto_model
 * @property int $year
 * @property string $fuel
 * @property string $body_type
 * @property string $colour
 * @property string $engine_size
 * @property string $version
 * @property int $date_added
 * @property string $business_plus_url
 * @property string $edition_one
 * @property string $edition_two
 * @property string $edition_three
 * @property string $edition_four
 * @property int $edition_date
 * @property string $buy_now_url
 * @property string $transmission
 * @property int $co2_emissions
 * @property string $nct_date
 * @property string $tax_date
 * @property int $mileage
 *
 * @property LocationsCounties $county
 * @property LocationsTowns $town
 * @property ClassifiedCategories $parent
 * @property ClassifiedCategories $classifiedCat
 * @property Users $user
 */
class ClassifiedListings extends \yii\db\ActiveRecord
{
    const CLASSIFIED_ADVERT = "classified advert";
    public $private_count;
    public $business_count;
    public $images;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'classified_listings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_count', 'duration', 'order', 'bump', 'bump_count', 'date_sold', 'views', 'make_id', 'model_id', 'year', 'date_added', 'edition_date', 'co2_emissions', 'mileage'], 'integer'],
            [['parent_id', 'classified_cat_id', 'title', 'description', 'currency', 'price', 'email', 'mobile'], 'required'],
            [['ad_type', 'description', 'search_options', 'item_status', 'status', 'featured', 'edition_one', 'edition_two', 'edition_three', 'edition_four', 'transmission','search_price'], 'string'],
            [['sub_category'], 'string', 'max' => 60],
            ['images', 'image', 'minWidth' => 768, 'minHeight' => 400, 'extensions' => 'jpg, gif, png', 'maxSize' => 20 * 20 * 2],
            [['title', 'email', 'version'], 'string', 'max' => 100],
            [['currency'], 'string', 'max' => 5],
            [['price', 'price_suffix'], 'string', 'max' => 15],
            [['telephone', 'mobile', 'other', 'body_type', 'colour', 'engine_size'], 'string', 'max' => 20],
            [['auto_make', 'auto_model'], 'string', 'max' => 30],
            [['fuel'], 'string', 'max' => 6],
            [['business_plus_url'], 'string', 'max' => 150],
            [['buy_now_url'], 'string', 'max' => 255],
            [['nct_date', 'tax_date'], 'string', 'max' => 10],
            [['county_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocationsCounties::className(), 'targetAttribute' => ['county_id' => 'id']],
            [['town_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocationsTowns::className(), 'targetAttribute' => ['town_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClassifiedCategories::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['classified_cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClassifiedCategories::className(), 'targetAttribute' => ['classified_cat_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'classified_cat_id' => 'Classified Cat ID',
            'sub_category' => 'Sub Category',
            'user_id' => 'User ID',
            'image_count' => 'Image Count',
            'ad_type' => 'Ad Type',
            'title' => 'Title',
            'description' => 'Description',
            'search_options' => 'Search Options',
            'currency' => 'Currency',
            'price' => 'Price',
            'search_price' => 'Search Price',
            'price_suffix' => 'Price Suffix',
            'duration' => 'Duration',
            'town_id' => 'Town ID',
            'county_id' => 'County ID',
            'item_status' => 'Item Status',
            'telephone' => 'Telephone',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'other' => 'Other',
            'status' => 'Status',
            'order' => 'Order',
            'bump' => 'Bump',
            'bump_count' => 'Bump Count',
            'featured' => 'Featured',
            'date_sold' => 'Date Sold',
            'views' => 'Views',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'make_id' => 'Make ID',
            'auto_make' => 'Auto Make',
            'model_id' => 'Model ID',
            'auto_model' => 'Auto Model',
            'year' => 'Year',
            'fuel' => 'Fuel',
            'body_type' => 'Body Type',
            'colour' => 'Colour',
            'engine_size' => 'Engine Size',
            'version' => 'Version',
            'date_added' => 'Date Added',
            'business_plus_url' => 'Business Plus Url',
            'edition_one' => 'Edition One',
            'edition_two' => 'Edition Two',
            'edition_three' => 'Edition Three',
            'edition_four' => 'Edition Four',
            'edition_date' => 'Edition Date',
            'buy_now_url' => 'Buy Now Url',
            'transmission' => 'Transmission',
            'co2_emissions' => 'Co2 Emissions',
            'nct_date' => 'Nct Date',
            'tax_date' => 'Tax Date',
            'mileage' => 'Mileage',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounty()
    {
        return $this->hasOne(LocationsCounties::className(), ['id' => 'county_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTown()
    {
        return $this->hasOne(LocationsTowns::className(), ['id' => 'town_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarsMakes()
    {
        return $this->hasMany(\backend\models\CarMakes::className(), ['id' => 'make_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ClassifiedCategories::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassifiedCat()
    {
        return $this->hasOne(ClassifiedCategories::className(), ['id' => 'classified_cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWatchList()
    {
        return $this->hasMany(WatchlistLookup::className(), ['listing_id' => 'id']);
    }
}
