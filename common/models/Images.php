<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property string $table_name
 * @property int $parent_id
 * @property string $image_name
 * @property int $order
 * @property int $rotate
 * @property int $date_added
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['table_name', 'image_name'], 'required'],
            [['parent_id', 'order', 'rotate', 'date_added'], 'integer'],
            [['table_name'], 'string', 'max' => 30],
            [['image_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Table Name',
            'parent_id' => 'Parent ID',
            'image_name' => 'Image Name',
            'order' => 'Order',
            'rotate' => 'Rotate',
            'date_added' => 'Date Added',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ImagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImagesQuery(get_called_class());
    }
}
