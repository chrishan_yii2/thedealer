<?php

namespace common\models\data;

use Yii;

/**
 *
 */
class Adtype
{
    const AD_PRIVATE = "private";
    const AD_BUSINESS = "business";
    
    public static function getAll()
    {
        return [self::AD_PRIVATE => 'Private', self::AD_BUSINESS => 'Business'];
    }
    
    public static function IsPrivate($input)
    {
        return self::AD_PRIVATE == $input;   
    }
    
    public static function IsBusiness($input)
    {
        return self::AD_BUSINESS == $input;
    }
}
