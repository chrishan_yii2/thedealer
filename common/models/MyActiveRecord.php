<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
USE yii\helpers\Json;

class MyActiveRecord extends \yii\db\ActiveRecord {

    const STATUS_INACTIVE = 'inactive';
    const STATUS_ACTIVE = 'active';
    const PRIVATE_USER = 'private';
    const BUSINESS_USER = 'business';
    const ADMIN_USER = 'admin';

    //Validate timestamp
    public static function is_timestamp($timestamp) {
        if (strtotime(date('d-m-Y H:i:s', $timestamp)) === (int) $timestamp) {
            return TRUE;
        } else
            return FALSE;
    }

    //Get Date Format
    public static function getDateFormat() {
        return 'd-m-Y';
    }

    //Convert timestamp into Date
    public static function convertTimeStampIntoDate($timestamp) {

        if (self::is_timestamp($timestamp)) {
            return date(self::getDateFormat(), $timestamp);
        } else
            return;
    }
    
    //Convert Search Date InTo Timestamp
    public static function convertSearchDateInToTimestamp($datesArray) {
        // Date array contain two elements FromDate and toDate    
        
//         echo "<pre>";
//        print_r($datesArray);
//        exit;
        $fromDate = \DateTime::createFromFormat('d-m-Y', trim($datesArray['fromDate']));
      
        $createdAtFrom = strtotime($fromDate->format('Y-m-d'));
         
        
        $toDate = \DateTime::createFromFormat('d-m-Y', trim($datesArray['toDate']));
        $createdAtTo = strtotime($toDate->format('Y-m-d 23:59:59'));
        return $timeStamps = ['fromDate' => $createdAtFrom, 'toDate' => $createdAtTo];
    }
 /*   public static function convertSearchDateInToTimestamp($datesArray) {
        // Date array contain two elements FromDate and toDate    
        $fromDate = \DateTime::createFromFormat('d/m/Y', trim($datesArray['fromDate']));
      
        $createdAtFrom = strtotime($fromDate->format('Y-m-d'));
//          echo "<pre>";
//        print_r($createdAtFrom);
//        exit;
        
        $toDate = \DateTime::createFromFormat('d/m/Y', trim($datesArray['toDate']));
        $createdAtTo = strtotime($toDate->format('Y-m-d 23:59:59'));
        return $timeStamps = ['fromDate' => $createdAtFrom, 'toDate' => $createdAtTo];
    }
*/
}

?>