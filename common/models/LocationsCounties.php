<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "locations_counties".
 *
 * @property int $id
 * @property string $county
 * @property string $status
 * @property double $latitude
 * @property double $longitude
 * @property int $zoom
 * @property int $date_added
 * @property string $slug
 *
 * @property ClassifiedListings[] $classifiedListings
 * @property LocationsTowns[] $locationsTowns
 * @property Users[] $users
 */
class LocationsCounties extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locations_counties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['county', 'latitude', 'longitude', 'zoom', 'date_added', 'slug'], 'required'],
            [['status'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['zoom', 'date_added'], 'integer'],
            [['county'], 'string', 'max' => 60],
            [['slug'], 'string', 'max' => 40],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'county' => 'County',
            'status' => 'Status',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'zoom' => 'Zoom',
            'date_added' => 'Date Added',
            'slug' => 'Slug',
        ];
    }

    /**
     * {@inheritdoc}
     * @return LocationsCountiesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LocationsCountiesQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassifiedListings()
    {
        return $this->hasMany(ClassifiedListings::className(), ['county_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocationsTowns()
    {
        return $this->hasMany(LocationsTowns::className(), ['county_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['county_id' => 'id']);
    }
}
