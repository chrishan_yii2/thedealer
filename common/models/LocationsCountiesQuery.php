<?php
/**
 * Created by PhpStorm.
 * User: i5 Developer
 * Date: 9/12/2018
 * Time: 1:22 PM
 */

namespace common\models;


use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[LocationsCounties]].
 *
 * @see LocationsCounties
 */

class LocationsCountiesQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LocationsCounties[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function getCounty($id)
    {
        return $this->andWhere(['id'=>$id]);
    }

}