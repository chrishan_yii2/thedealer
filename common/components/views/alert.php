<?php
/**
 * Created by PhpStorm.
 * User: i5 Developer
 * Date: 9/18/2018
 * Time: 4:35 PM
 */


if($flashes){
    foreach ($flashes as $type => $flash) {
        if (!isset($alertTypes[$type])) {
            continue;
        }
        $classes = "alert alert-dismissible fade show mt-3 ".$alertTypes[$type];
        foreach ((array) $flash as $i => $message) {
            ?>
            <div id="<?= $type . '-' . $i ?>" class="<?= $classes; ?>" role="alert">
                <?= $message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php
        }
    }
}

?>