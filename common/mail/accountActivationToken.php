<?php
/**
 * @var $this yii\web\View
 * @var $users common\models\Users
 */
use yii\helpers\Html;
?>
<?php
$returnUrl = Yii::$app->user->returnUrl == Yii::$app->homeUrl ? null : rtrim(Yii::$app->homeUrl, '/') . Yii::$app->user->returnUrl;
$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/verify', 'token' => $users->token, 'returnUrl' => $returnUrl]);
?>

    Hello, you have been registered on <?= Yii::$app->name ?>

    <br/><br/>
    Follow this link to confirm your E-mail and activate account:

<?= Html::a('confirm E-mail', $confirmLink) ?>
