<?php
/**
 * @var $this yii\web\View
 * @var $users common\models\Users
 */
use yii\helpers\Html;
use common\models\Users;
?>
<?php
if($user->user_type == Users::ROLE_ADMIN){
    $loginLink = Yii::$app->urlManager->createAbsoluteUrl(['/admin']);
} else {
    $loginLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/login']);
}
?>

Hello <?= Html::encode($user->contact_name) ?>,

Your new <?= Html::encode($user->user_type) ?> account created for <?= Html::a(Yii::$app->name, Yii::$app->homeUrl) ?>.

<br/><br/>
Your username: <?= Html::encode($user->username) ?>

Your password: <?= Html::encode($password) ?>

To login, please visit our website: <?= $loginLink ?>
