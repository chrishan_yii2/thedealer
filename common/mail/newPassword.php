<?php
/**
 * @var $this yii\web\View
 * @var $users common\models\Users
 */
use yii\helpers\Html;
?>
<?php
$loginLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/login']);
?>

    Hello <?= Html::encode($user->contact_name) ?>, 
    
    Your password has been reset for you, as requested on <?= Html::a(Yii::$app->name, Yii::$app->homeUrl) ?>.

    <br/><br/>
     Your username: <?= Html::encode($user->username) ?>

     Your password: <?= Html::encode($password) ?>

     To login, please visit our website: <?= $loginLink ?>
