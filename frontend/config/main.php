<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'THEDEALER.ie',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'homeUrl' => '/',
    'modules' => [ 

        'mailchimp' => [
            'class' => 'cinghie\mailchimp\Mailchimp',
            'apiKey' => '83c260b2c7b8981fe8960bc82aea4610-us18',
            'showFirstname' => false,
            'showLastname' => false
        ]

    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '/',
        ],
        'user' => [
            'identityClass' => 'common\models\Users',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'socialShare' => [
            'class' => \ymaker\social\share\configurators\Configurator::class,
            'socialNetworks' => [
                'pinterest' => [
                    'class' => \ymaker\social\share\drivers\Pinterest::class,
                    'label' => \yii\helpers\Html::tag('i', '', ['class' => 'fab fa-pinterest-p fa-3x', 'data-fa-transform'=>'shrink-6', 'data-fa-mask' => 'fas fa-square']),
                    'options' => ['class' => 'mr-2'],
                ],
                'twitter' => [
                    'class' => \ymaker\social\share\drivers\Twitter::class,
                    'label' => \yii\helpers\Html::tag('i', '', ['class' => 'fab fa-twitter fa-3x', 'data-fa-transform'=>'shrink-6', 'data-fa-mask' => 'fas fa-square']),
                    'options' => ['class' => 'mr-2'],
                ],
                'facebook' => [
                    'class' => \ymaker\social\share\drivers\Facebook::class,
                    'label' => \yii\helpers\Html::tag('i', '', ['class' => 'fab fa-facebook-f fa-3x', 'data-fa-transform'=>'shrink-6', 'data-fa-mask' => 'fas fa-square']),
                    'options' => ['class' => 'mr-2'],
                ],
                'googlePlus' => [
                    'class' => \ymaker\social\share\drivers\GooglePlus::class,
                    'label' => \yii\helpers\Html::tag('i', '', ['class' => 'fab fa-google-plus-g fa-3x', 'data-fa-transform'=>'shrink-6', 'data-fa-mask' => 'fas fa-square']),
                    'options' => ['class' => 'mr-2'],
                ],
            ],
            'options' => [
                'class' => 'social-network',
                'target' => '_blank'
            ],
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => '',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'classified/index',
                '<alias:index|view|create>' => 'classified/<alias>',
                '<alias:login|signup|logout|about|contact|faq|term-conditions|password-reset>' => 'site/<alias>',
            ],
        ],
    ],
    'params' => $params,
];
