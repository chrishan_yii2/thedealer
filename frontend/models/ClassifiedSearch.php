<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ClassifiedListings;

/**
 * ClassifiedSearch represents the model behind the search form of `common\models\ClassifiedListings`.
 */
class ClassifiedSearch extends ClassifiedListings
{
    public $slug;
    public $county;
    public $make;
    const STATUS_INACTIVE = 'inactive';
    const STATUS_ACTIVE = 'active';


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'classified_cat_id', 'user_id', 'image_count', 'duration', 'town_id', 'county_id', 'order', 'bump', 'bump_count', 'date_sold', 'views', 'start_date', 'end_date', 'make_id', 'model_id', 'year', 'date_added', 'edition_date', 'co2_emissions', 'mileage'], 'integer'],
            [['sub_category','make','slug','county', 'ad_type', 'title', 'description', 'search_options', 'currency', 'price', 'price_suffix', 'item_status', 'telephone', 'email', 'mobile', 'other', 'status', 'featured', 'auto_make', 'auto_model', 'fuel', 'body_type', 'colour', 'engine_size', 'version', 'business_plus_url', 'edition_one', 'edition_two', 'edition_three', 'edition_four', 'buy_now_url', 'transmission', 'nct_date', 'tax_date'], 'safe'],
            [['search_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $nextWeek = time() - (3 * 24 * 60 * 60);
        $query = ClassifiedListings::find()
                                ->where(['classified_categories.status' => self::STATUS_ACTIVE])
                                ->andWhere(['classified_listings.status' => self::STATUS_ACTIVE])
                                ->andWhere(['>=', 'classified_listings.end_date', $nextWeek])
                                ->andWhere(['!=', 'classified_listings.price', '']);
                                
        if(!isset($params['sort'])){
            $query->orderBy(['classified_listings.id' => SORT_DESC]);
        }
        
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        
        if(isset($params['ClassifiedSearch']['slug'])){
            $ifExist = \common\models\ClassifiedCategories::find()
                                                    ->where(['slug' => $params['ClassifiedSearch']['slug']])
                                                    ->andWhere(['status' => 'active'])
                                                    ->one();
        }
        
        
        if(isset($ifExist->parent_id) && $ifExist->parent_id == 0){
            $query->joinWith('parent');
        }else{
            $query->joinWith('classifiedCat');
        }
        
        $query->joinWith('carsMakes');
        $query->joinWith('town');
        
//        echo "<pre>";
//        print_r($query);exit;
        
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where("0=1");
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'classified_listings.id' => $this->id,
            'parent_id' => $this->parent_id,
            'classified_cat_id' => $this->classified_cat_id,
            'user_id' => $this->user_id,
            'image_count' => $this->image_count,
            'search_price' => $this->search_price,
            'duration' => $this->duration,
            'town_id' => $this->town_id,
            'county_id' => $this->county_id,
            'order' => $this->order,
            'bump' => $this->bump,
            'bump_count' => $this->bump_count,
            'date_sold' => $this->date_sold,
            'views' => $this->views,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'make_id' => $this->make_id,
            'model_id' => $this->model_id,
            'year' => $this->year,
            'date_added' => $this->date_added,
            'edition_date' => $this->edition_date,
            'co2_emissions' => $this->co2_emissions,
            'mileage' => $this->mileage,
        ]);

        $query->andFilterWhere(['like', 'sub_category', $this->sub_category])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'ad_type', $this->ad_type])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'search_options', $this->search_options])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'price_suffix', $this->price_suffix])
            ->andFilterWhere(['like', 'item_status', $this->item_status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'other', $this->other])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'featured', $this->featured])
            ->andFilterWhere(['like', 'auto_make', $this->auto_make])
            ->andFilterWhere(['like', 'auto_model', $this->auto_model])
            ->andFilterWhere(['like', 'fuel', $this->fuel])
            ->andFilterWhere(['like', 'body_type', $this->body_type])
            ->andFilterWhere(['like', 'colour', $this->colour])
            ->andFilterWhere(['like', 'engine_size', $this->engine_size])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'business_plus_url', $this->business_plus_url])
            ->andFilterWhere(['like', 'edition_one', $this->edition_one])
            ->andFilterWhere(['like', 'edition_two', $this->edition_two])
            ->andFilterWhere(['like', 'edition_three', $this->edition_three])
            ->andFilterWhere(['like', 'edition_four', $this->edition_four])
            ->andFilterWhere(['like', 'buy_now_url', $this->buy_now_url])
            ->andFilterWhere(['like', 'transmission', $this->transmission])
            ->andFilterWhere(['like', 'nct_date', $this->nct_date])
            ->andFilterWhere(['like', 'locations_towns.slug', $this->county])
            ->andFilterWhere(['like', 'classified_categories.slug', $this->slug])
            ->andFilterWhere(['like', 'car_makes.slug', $this->make])
            
            ->andFilterWhere(['like', 'tax_date', $this->tax_date]);

        return $dataProvider;
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function Carsearch($params)
    {
        $query = ClassifiedListings::find();
                                
                                
        
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 9
            ]
        ]);
        
        if(isset($params['ClassifiedSearch']['slug'])){
            $ifExist = \common\models\ClassifiedCategories::find()
                                                    ->where(['slug' => $params['ClassifiedSearch']['slug']])
                                                    ->andWhere(['status' => 'active'])
                                                    ->one();
        }
        
        
        if(isset($ifExist->parent_id) && $ifExist->parent_id == 0){
            $query->joinWith('parent');
        }else{
            $query->joinWith('classifiedCat');
        }
        
        $query->joinWith('carsMakes');
        $query->joinWith('town');
        
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where("0=1");
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'classified_listings.id' => $this->id,
            'parent_id' => $this->parent_id,
            'classified_cat_id' => $this->classified_cat_id,
            'user_id' => $this->user_id,
            'image_count' => $this->image_count,
            'search_price' => $this->search_price,
            'duration' => $this->duration,
            'town_id' => $this->town_id,
            'county_id' => $this->county_id,
            'order' => $this->order,
            'bump' => $this->bump,
            'bump_count' => $this->bump_count,
            'date_sold' => $this->date_sold,
            'views' => $this->views,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'make_id' => $this->make_id,
            'model_id' => $this->model_id,
            'year' => $this->year,
            'date_added' => $this->date_added,
            'edition_date' => $this->edition_date,
            'co2_emissions' => $this->co2_emissions,
            'mileage' => $this->mileage,
        ]);

        $query->andFilterWhere(['like', 'sub_category', $this->sub_category])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'ad_type', $this->ad_type])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'search_options', $this->search_options])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'price_suffix', $this->price_suffix])
            ->andFilterWhere(['like', 'item_status', $this->item_status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'other', $this->other])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'featured', $this->featured])
            ->andFilterWhere(['like', 'auto_make', $this->auto_make])
            ->andFilterWhere(['like', 'auto_model', $this->auto_model])
            ->andFilterWhere(['like', 'fuel', $this->fuel])
            ->andFilterWhere(['like', 'body_type', $this->body_type])
            ->andFilterWhere(['like', 'colour', $this->colour])
            ->andFilterWhere(['like', 'engine_size', $this->engine_size])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'business_plus_url', $this->business_plus_url])
            ->andFilterWhere(['like', 'edition_one', $this->edition_one])
            ->andFilterWhere(['like', 'edition_two', $this->edition_two])
            ->andFilterWhere(['like', 'edition_three', $this->edition_three])
            ->andFilterWhere(['like', 'edition_four', $this->edition_four])
            ->andFilterWhere(['like', 'buy_now_url', $this->buy_now_url])
            ->andFilterWhere(['like', 'transmission', $this->transmission])
            ->andFilterWhere(['like', 'nct_date', $this->nct_date])
            ->andFilterWhere(['like', 'locations_towns.slug', $this->county])
            ->andFilterWhere(['like', 'classified_categories.slug', $this->slug])
                ->andFilterWhere(['like', 'car_makes.slug', $this->make])
            
            ->andFilterWhere(['like', 'tax_date', $this->tax_date]);

        return $dataProvider;
    }
    
    /**
     * Creates data provider instance with User Id
     *
     *
     * @return ActiveDataProvider
     */
    public function searchByUser($id)
    {
        $query = ClassifiedListings::find()
                                ->where(['status' => self::STATUS_ACTIVE])
                                ->andWhere(['user_id' => $id])
                                ->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 9
            ]
        ]);
        
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function recentAds($params)
    {
        $nextWeek = time() - (300 * 24 * 60 * 60);
        $query = ClassifiedListings::find()
                                ->where(['classified_categories.status' => self::STATUS_ACTIVE])
                                ->andWhere(['classified_listings.status' => self::STATUS_ACTIVE])
                                ->andWhere(['>=', 'classified_listings.date_added', $nextWeek])
                                ->andWhere(['!=', 'classified_listings.price', ''])
                                ->orderBy(['classified_listings.id' => SORT_DESC]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6
            ]
        ]);
        
        if(isset($ifExist->parent_id) && $ifExist->parent_id == 0){
            $query->joinWith('parent');
        }else{
            $query->joinWith('classifiedCat');
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
