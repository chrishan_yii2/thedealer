<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Users;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $user_type = 'private';
    public $contact_name;
    public $telephone;
    public $address_one;
    public $county_id;
    public $town_id;
    public $company_name;
    public $show_pass;
    public $activeNL = false;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            ['company_name', 'required', 'when'=> function($model){
                return $model->user_type == 'business';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#signupform-user_type input:checked').val() == 'business';
                }"],
            ['company_name', 'string', 'max' => 100],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\Users', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Users', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['contact_name', 'required'],
            ['contact_name', 'string', 'max' => 20],

            ['telephone', 'required'],
            ['telephone', 'integer'],

            ['address_one', 'required'],
            ['address_one', 'string'],

            ['county_id', 'required', 'message' => 'Please select your County'],
            ['county_id', 'integer', 'message' => 'Please select your County'],

            ['town_id', 'required', 'message' => 'Please select your Town'],
            ['town_id', 'integer', 'message' => 'Please select your Town'],
            
            ['activeNL', 'boolean'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return Users|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $users = new Users();
        $users->user_type = $this->user_type;
        $users->company_name = $this->company_name;
        $users->username = $this->username;
        $users->email = $this->email;
        $users->contact_name = $this->contact_name;
        $users->telephone = $this->telephone;
        $users->setPassword($this->password);
        $users->generateActivationToken();
        $users->token_ip_address = $_SERVER['SERVER_ADDR'];
        $users->address_one = $this->address_one;
        $users->county_id = $this->county_id;
        $users->town_id = $this->town_id;
        $users->newsletter = $this->activeNL ? Users::YES : Users::NO;
        $users->setStatus();
        $users->date_added = time();
        
        return  $users->save(false) ? $users : null;
////        $users->save();
//        var_dump($this->activeNL);
    }
    
    /**
     * Sends email to registered user with account activation link.
     *
     * @param  object $user Registered user.
     * @return bool         Whether the message has been sent successfully.
     */
    public function sendAccountActivationEmail()
    {
        /* @var $users Users */
        $users = Users::findOne([
            'status' => Users::STATUS_INACTIVE,
            'email' => $this->email,
        ]);
        \Yii::$app->mailer->htmlLayout = "@common/mail/layouts/html";
        return Yii::$app->mailer->compose('@common/mail/accountActivationToken', ['users' => $users])
                                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                                ->setTo($this->email)
                                ->setSubject('Account activation for ' . Yii::$app->name)
                                ->send();
    }
}
