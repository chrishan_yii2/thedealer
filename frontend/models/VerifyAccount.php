<?php
namespace frontend\models;

use yii\base\Model;
use yii\base\InvalidParamException;
use common\models\Users;

/**
 * Password reset form
 */
class VerifyAccount extends Model
{

    /**
     * @var \common\models\Users
     */
    private $_users;


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Activation token cannot be blank.');
        }
        $this->_users = Users::findByActivationToken($token);
        if (!$this->_users) {
            throw new InvalidParamException('Wrong activation reset token.');
        }
        parent::__construct($config);
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function verifyAccount()
    {
        $user = $this->_users;
        $user->removeActivationToken();
        $user->status = Users::STATUS_ACTIVE;

        return $user->save(false);
    }
}
