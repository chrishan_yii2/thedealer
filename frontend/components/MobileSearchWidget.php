<?php

namespace frontend\components;

use yii\base\Widget;

class MobileSearchWidget extends Widget
{
    public function run()
    {
        return $this->render('mobileSearch');
    }
}