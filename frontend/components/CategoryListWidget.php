<?php

namespace frontend\components;

use common\models\ClassifiedCategories;
use yii\base\Widget;



class CategoryListWidget extends Widget
{
    public $parent_id;

    public function run()
    {
        return $this->render('categoryList',[
            'categories' => ClassifiedCategories::find()->where(['status' => 'active'])->getCategories()->all(),
        ]);
    }
}
