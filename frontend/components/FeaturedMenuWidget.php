<?php

namespace frontend\components;

use yii\base\Widget;

class FeaturedMenuWidget extends Widget
{
    public function run() {
        return $this->render('featuredMenu');
    }
}