<?php

namespace frontend\components;


use yii\base\Widget;

class HeaderMenusWidget extends Widget
{
    
    public function run()
    {
        return $this->render('headerMenus');
    }
}
