<?php

namespace frontend\components;

use yii\base\Widget;

class LogoWidget extends Widget
{

    public function run()
    {
        return $this->render('logo');
    }

}