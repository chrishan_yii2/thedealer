<?php

use common\models\Images;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\WatchlistLookup;
use common\models\ClassifiedListings;
use ymaker\social\share\widgets\SocialShare;

$images = Images::find()->select('image_name')->getImages($model::tableName(), $model->id)->all();
$ifExist = WatchlistLookup::find()->where(['listing_id' => $model->id])->one();

$params = Yii::$app->request->queryParams;

$relatedAds = common\models\ClassifiedListings::find()
        ->where(['classified_cat_id' => $model->classified_cat_id])
        ->orWhere(['parent_id' => $model->parent_id])
        ->andWhere(['status' => 'active'])
        ->andWhere(['!=', 'id', $params['ClassifiedSearch']['id']])
        ->orderBy(['date_added' => SORT_DESC])
        ->limit(8)
        ->all();
?>

<div class="container">
    <div class="breadcrumbs mt-1 mb-5 d-none d-md-block">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item back"><a onclick="history.go(-1)">Back</a></li>
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= Url::to('/classified/search?ClassifiedSearch[slug]=' . Html::encode($model->parent->slug)) ?>"><?= $model->parent->category ?></a></li>
                <li class="breadcrumb-item"><a href="<?= Url::to('/classified/search?ClassifiedSearch[slug]=' . Html::encode($model->classifiedCat->slug)) ?>"><?= $model->classifiedCat->category ?></a></li>
                <li class="breadcrumb-item"><a href="<?= Url::to('/classified/search?ClassifiedSearch[county]=' . Html::encode($model->county->slug)) ?>"><?= $model->county->county ?></a></li>
                <li class="breadcrumb-item"><a href="<?= Url::to('/classified/search?ClassifiedSearch[ad_type]=' . Html::encode($model->ad_type)) ?>"><?= $model->ad_type ?></a></li>
                <li class="breadcrumb-item active"><?= $model->title ?></li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <div class="row">
                <div class="col-md-3 order-12 order-md-1 item-img-slider-wrapper">
                    <div class="item-img-slider">
                        <?php
                        if (is_array($images) && count($images) > 0):
                            foreach ($images as $img => $image):
                                $image_url = Yii::$app->request->hostInfo . '/uploads/large/' . $image->image_name;
                                ?>
                                <?php if (@GetImageSize($image_url)): ?>
                                    <div class="item-img-slide">
                                        <img class="img-fluid" src="<?= $image_url ?>" alt="">
                                    </div>

                                    <?php
                                endif;
                            endforeach;
                        else:
                            ?>
                            <div class="item-img-slide">
                                <?= Html::img('/images/no_images_thumb.jpg', ['alt' => $model->title, 'title' => $model->title, 'class' => 'img-fluid']) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-9 order-1 order-md-12 ad-img-wrapper">
                    <div class="ad-img">
                        <?php
                        $image_url2 = "";
                        if (is_array($images) && count($images) > 0):
                            foreach ($images as $img => $image):
                                $image_url2 = Yii::$app->request->hostInfo . '/uploads/large/' . $image->image_name;
                                ?>
                                <?php if (@GetImageSize($image_url2)): ?>
                                    <div class="item-img-view">
                                        <a data-fancybox="gallery" href="<?= $image_url2 ?>">
                                            <img class="img-fluid" src="<?= $image_url2 ?>" alt="<?= $model->title ?>">
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <div class="item-img-slide">
                                        <?= Html::img('/images/no_images_thumb.jpg', ['alt' => $model->title, 'title' => $model->title, 'class' => 'img-fluid']) ?>
                                    </div>
                                <?php
                                endif;
                            endforeach;
                        else:
                            ?>
                            <div class="item-img-slide">
                                <?= Html::img('/images/no_images_thumb.jpg', ['alt' => $model->title, 'title' => $model->title, 'class' => 'img-fluid']) ?>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                </div>
            </div>
            <?php if (Yii::$app->user->isGuest): ?>
                <div class="make-offer mt-4 mb-5">
                    <div class="auth-require">
                        <p class="mb-3">You must be logged in to post a question or make an offer.</p>
                        <a class="btn btn-grey mr-2" href="<?= Url::to('/login'); ?>">LOGIN</a>
                        <a class="btn btn-success" href="<?= Url::to('/signup'); ?>">SIGNUP</a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="product-meta-info mb-4">
                <h3 class="product-title"><?= $model->title ?></h3>
                <span class="location"><?= $model->town->town . ', ' . $model->county->county ?></span>
                <div class="product-price">
                    <span>Price: <strong><sup><?= Yii::$app->params[$model->currency] ?></sup><?= $model->price ?></strong></span>
                    <!-- <a href="#" class="btn align-middle btn-secondry float-right">Buy Now</a> -->
                </div>
                <div class="row contact border-top border-bottom">
                    <div class="col border-right">
                        <p>Mobile: <span> <?= $model->mobile ?></span></p>
                    </div>
                    <div class="col">
                        <p>Landline: <span> <?= $model->telephone ?></span></p>
                    </div>
                </div>
            </div>
            <div class="row desc-wrap">
                <div class="desc col-md-8">
                    <?= $model->description ?>
                </div>
                <div class="col-md-4">
                    <div class="adverts mb-3">
                        <?php $adsense = \frontend\models\AdsenseCategory::find()->where(['category_id' => $model->parent_id, 'ad_location' => 'Listing Page'])->all(); ?>
                        <?php if (!empty($adsense)): ?>
                            <?= $adsense[0]->adsense_code_1 ?>
                        <?php endif; ?>
                    </div>
                    <a href="mailto:<?= (!empty($model->email)) ? $model->email : $model->user->email ?>" class="btn btn-success btn-block">Email</a>
                    <a href="<?= Url::toRoute(['/users/watch-list', 'id' => $model->id]) ?>" class="btn btn-default btn-block"><?= (!empty($ifExist) && count($ifExist) > 0) ? "Already Watched" : "Watch"; ?></a>
                </div>
            </div>
            <div class="viewd mt-4 border-top">
                <p class="mt-2">This ad has been viewed <?= $model->views ?> times</p>
            </div>
            <?=
            SocialShare::widget([
                'configurator' => 'socialShare',
                'url' => Url::to('/classified/view?ClassifiedSearch[id]=' . Html::encode($model->id), true),
                'title' => Html::encode($model->title),
                'description' => Html::encode($model->description),
                'imageUrl' => Url::to($image_url2, true),
                'containerOptions' => ['tag' => 'div', 'class' => 'social-share mb-4'],
                'linkContainerOptions' => ['tag' => false],
            ]);
            ?>

        </div>
    </div>
    <div class="related-ads mb-4">
        <h3 class="sec-heading">Recent Ads</h3>
        <div class="ads-listing">
            <div class="row no-gutters">
                <?php
                if (isset($relatedAds) && count($relatedAds) > 0):
                    foreach ($relatedAds as $r => $relatedAd):
                        ?>
                        <div class="col-md-3 col-sm-4 col-xs-6 ads-grid">
                            <a class="ads-grid-content border" href="<?= Url::to('/classified/view?ClassifiedSearch[id]=' . Html::encode($relatedAd->id)) ?>">
                                <?php
                                $images = Images::find()->select('image_name')->getImages(ClassifiedListings::tableName(), $relatedAd->id)->one();
                                if (!empty($images)) {
                                    $imagePath = Yii::$app->request->hostInfo . '/uploads/small/' . $images->image_name;
                                } else {
                                    $imagePath = "";
                                }
                                ?>
                                <?php if (@GetImageSize($imagePath)) { ?>
                                    <?= Html::img($imagePath, ['alt' => $relatedAd->title, 'title' => $relatedAd->title, 'class' => 'ads_img']); ?>
                                <?php } else { ?>
                                    <?= Html::img('/images/no_images_thumb.jpg', ['alt' => $relatedAd->title, 'title' => $relatedAd->title, 'class' => 'ads_img']) ?>
        <?php } ?>
                                <div class="meta-info border-bottom">
                                    <span class="price"><?= Yii::$app->params[$relatedAd->currency] ?></sup><?= $relatedAd->price ?></span>
                                    <span class="watch ajaxSave float-right" data-url="<?= Url::toRoute(['/users/watch-list']) ?>" data-id="<?= $relatedAd->id ?>">
                                        <i class="far fa-heart fa-fw fa-lg"></i>
                                    </span>
                                </div>
                                <h4 class="ad-title"><?= $relatedAd->title ?></h4>
                                <?php
                                $town = isset($relatedAd->town->town) ? $relatedAd->town->town : '';
                                $county = isset($relatedAd->county->county) ? $relatedAd->county->county : '';
                                ?>
                                <p class="location"><?= $town . "," . $county ?></p>
                            </a>
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>