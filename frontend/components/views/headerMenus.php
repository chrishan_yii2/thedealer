<?php

use frontend\components\HeaderMenusWidget;
use yii\helpers\Url;

?>

<div id="mobilenavbar" class="collapse navbar-collapse mt-3 mt-sm-0">
    <div class="container">
        <div class="nav-wrapper">
            <ul class="navbar-nav nav">
                <li><a class="nav-link" href="<?= Url::home(); ?>"><i class="fas fa-fw fa-home fa-lg"></i> Home</a></li>
                <li class="d-block d-sm-none"><a class="nav-link" href="<?= Url::to(['#']); ?>"><i class="far fa-fw fa-list-alt fa-lg"></i> Browse ads</a></li>
                <li class="d-block d-sm-none"><a class="nav-link" href="<?= Url::to('/create') ?>"><i class="far fa-fw fa-edit fa-lg"></i> Place an Ad</a></li>
                <li class="d-block d-sm-none"><a class="nav-link" href="<?= Url::to('/business'); ?>"><i class="far fa-fw fa-building fa-lg"></i> Business Directory</a></li>
                <li><a class="nav-link" href="<?= Url::to('/faq') ?>"><i class="fas fa-fw fa-bullhorn fa-lg"></i> FAQ</a></li>
                <li><a class="nav-link" href="<?= Url::to('/term-conditions'); ?>"><i class="fas fa-fw fa-book fa-lg"></i> Terms &amp; Condtions</a></li>
                <li><a class="nav-link" href="<?= Url::to('/contact'); ?>"><i class="far fa-fw fa-envelope fa-lg"></i> Get In Touch</a></li>

            </ul>
            <div class="row mt-2">
                <div class="col">
                    <div class="btn-wrapper ml-3 mr-3">
                        <a class="btn btn-block btn-default" href="<?= Url::to(['/signup']); ?>">Signup</a>
                    </div>
                </div>
                <div class="col">
                    <div class="btn-wrapper ml-3 mr-3">
                        <a class="btn btn-block btn-grey" href="<?= Url::to(['/login']); ?>">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>