<?php

/**
 * @var common\models\ClassifiedCategories $model
 */

use yii\helpers\Html;
use common\models\ClassifiedCategories;
use common\models\Images;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>

<div class="categories" id="categories">
    <?php
        $count = 1;
        foreach ($categories as $category){ ?>
        <div class="categories-grid" data-count="<?= $count; ?>">
            <a href="#<?= preg_replace("/[^a-zA-Z]+/", "_", strtolower($category->category)) ?>" data-toggle="collapse" class="collapsed">
                <div class="cat-images">
                    <?php
                        $images = Images::find()->select('image_name')->getImages(ClassifiedCategories::tableName(),$category->id)->one();
                        if(isset($images->image_name) && !empty($images->image_name)){
                            $imagePath = \Yii::getAlias('@webroot').'/uploads/small/'.$images->image_name;
                        }else{
                            $imagePath = \Yii::getAlias('@webroot').'/uploads/small/demo.jpg';
                        }
                        
                     ?>
                    <?php if (file_exists($imagePath)) { ?>
                        <?= Html::img('/uploads/small/'.$images->image_name); ?>
                    <?php } else { ?>
                        <?= Html::img('/images/no_images_thumb.jpg') ?>
                    <?php } ?>
                </div>
                <h4 class="category-title"><?= Html::encode($category->category); ?></h4>
            </a>
            <div class="sub-categories collapse" id="<?= preg_replace("/[^a-zA-Z]+/", "_", strtolower($category->category)) ?>" data-parent="#categories">
                <div class="sub-category">
                    <h2 class="subcat-head d-md-block d-none">View All <?= Html::encode($category->category); ?></h2>
                    <div class="row no-gutters">
                        <div class="col-md-2 d-md-block d-none">
                            <a href="#" class="main-cat">
                            <?php if (file_exists($imagePath)) { ?>
                                <?= Html::img('/uploads/small/'.$images->image_name); ?>
                            <?php } else { ?>
                                <?= Html::img('/images/no_images_thumb.jpg') ?>
                            <?php } ?>
                                
                            </a>
                        </div>
                        <div class="col-md-10">
                            <ul class="cat-lists">
                                <li class="parent-cat d-block d-md-none">
                                    <i class="fas fa-caret-right"></i>
                                    <a href="<?= Url::to('/classified/search?ClassifiedSearch[slug]=') ?>">View All <?= Html::encode($category->category); ?></a>
                                </li>
                                <?php
                                    $subCategories = ClassifiedCategories::find()->where(['status' => 'active'])->childCategories($category->id)->all();
                                    foreach ($subCategories as $subCategory) {
                                        if ($subCategory->slug == 'cars') {
                                            ?>
                                            <li <?= $subCategory->order; ?>><i class="fas fa-caret-right"></i><a
                                                        href="<?= Url::to('/classified/search-car') ?>"><?= Html::encode($subCategory->category); ?></a>
                                            </li>
                                            <?php
                                        } else {
                                            ?>
                                            <li <?= $subCategory->order; ?>><i class="fas fa-caret-right"></i><a
                                                        href="<?= Url::to('/classified/search?ClassifiedSearch[slug]=' . Html::encode($subCategory->slug)) ?>"><?= Html::encode($subCategory->category); ?></a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    $subCategories = ClassifiedCategories::find()->where(['status' => 'active'])->subChildCategories($category->id)->all();
                                    foreach ($subCategories as $subCategory) {
                                        if ($subCategory->slug == 'cars') {
                                            ?>
                                            <li <?= $subCategory->order; ?>><i class="fas fa-caret-right"></i><a
                                                        href="<?= Url::to('/classified/search-car') ?>"><?= Html::encode($subCategory->category); ?></a>
                                            </li>
                                            <?php
                                        } else {
                                            ?>
                                            <li <?= $subCategory->order; ?>><i class="fas fa-caret-right"></i><a
                                                        href="<?= Url::to('/classified/search?ClassifiedSearch[slug]=' . Html::encode($subCategory->slug)) ?>"><?= Html::encode($subCategory->category); ?></a>
                                            </li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
        $count++;
        } ?>
</div>


