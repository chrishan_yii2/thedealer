<?php
/**
 * Created by PhpStorm.
 * User: i5 Developer
 * Date: 9/11/2018
 * Time: 5:10 PM
 */

use common\models\ClassifiedListings;
use common\models\Images;
use common\models\LocationsTowns;
use common\models\LocationsCounties;
use yii\helpers\Html;
use yii\helpers\Url;

$itemUrl = Url::to(['view', 'id' => $model->id]);

?>

<div class="<?= $cssClass; ?>">
    <div class="ads-grid-content border">
        <a href="<?= Url::to('/classified/view?ClassifiedSearch[id]='.Html::encode($model->id)) ?>">
        <?php
        $images = Images::find()->select('image_name')->getImages(ClassifiedListings::tableName(),$model->id)->one();
        if(!empty($images)){
            $imagePath = Yii::$app->request->hostInfo.'/uploads/small/'.$images->image_name;
        }else{
            $imagePath = "";
        }
        
        ?>
        <?php if (@GetImageSize($imagePath)) { ?>
            <?= Html::img($imagePath, ['alt'=> $model->title, 'title'=>$model->title, 'class'=>'ads_img' ]); ?>
        <?php } else { ?>
            <?= Html::img('/images/no_images_thumb.jpg', ['alt'=> $model->title, 'title'=>$model->title, 'class'=>'ads_img' ]) ?>
        <?php } ?>
        <?php
            $price = (isset(Yii::$app->params[$model->currency])) ? Yii::$app->params[$model->currency].$model->price : "";
        ?>
        </a>
        <div class="meta-info border-bottom">
            <span class="ads_type"><?= $model->ad_type; ?></span>
            <span class="price"><?= $price; ?></span>
            <span class="watch float-right ajaxSave" data-url="<?= Url::toRoute(['/users/watch-list']) ?>" data-id="<?= $model->id ?>">
                <i class="<?= (isset($model->watchList[0]->listing_id) && !empty($model->watchList[0]->listing_id))? 'fas': 'fal' ?>   fa-heart fa-fw fa-lg"></i>
            </span>
        </div>
        <a href="<?= Url::to('/classified/view?ClassifiedSearch[id]='.Html::encode($model->id)) ?>">
        <h4 class="ad-title"><?= $model->title; ?></h4>
        <?php
            $town = LocationsTowns::find()->select('town')->getTown($model->town_id)->one();
            $county = LocationsCounties::find()->select('county')->getCounty($model->county_id)->one();
            $location = "";
            if(is_array($town) && count($town) > 0){
                $location = $town->town;
            }
            if(is_array($county) && count($county) > 0){
                $location .= ', '.$county->county;
            }
            
        ?>
        <p class="location"><?= (isset($location) && !empty($location)) ? $location : "Not mention"; ?></p>
        </a>
    </div>
</div>
