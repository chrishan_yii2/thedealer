<?php

use frontend\components\MobileSearchWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(['id'=>'search','options' =>['class'=>'form-inline d-flex d-md-none']]); ?>
<input class="form-control" type="search" placeholder="Search for a product, service or business.." aria-label="Search">
<?= Html::submitButton('Search', ['class' => 'btn my-2 my-sm-0', 'name' => 'search-button']) ?>
<?php ActiveForm::end(); ?>
