<?php

use frontend\components\FeaturedMenuWidget;
use yii\helpers\Url;
?>

<ul class="main-nav nav justify-content-md-center">
    <li>
        <a class="nav-link" href="<?= Url::to(['/']); ?>"><i class="fal fa-search"></i> Browse<span class="d-lg-inline d-none"> Ads</span></a>
    </li>
    <li class="default">
        <a class="nav-link" href="<?= Url::to(['/create']); ?>"><i class="fal fa-pencil-alt"></i> Place<span class="d-lg-inline d-none"> an</span> Ad</a>
    </li>
    <li>
        <a class="nav-link" href="<?= Url::to(['/users/business-directory?Users[user_type]='.common\models\Users::BUSINESS_USER]); ?>"><i class="flaticon-pin-s"></i> Business<span class="d-lg-inline d-none"> Directory</span></a>
    </li>
</ul>