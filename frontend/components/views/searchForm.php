<?php

use frontend\components\SearchFormWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$title = (isset($_GET['ClassifiedSearch']['title']) && !empty($_GET['ClassifiedSearch']['title'])) ? $_GET['ClassifiedSearch']['title'] : '';

?>

<?php $form = ActiveForm::begin(['id'=>'search', 'method' => 'get', 'action' => "/classified/search",'options' =>['class'=>'form-inline my-0']]); ?>
<input class="form-control" type="search" value="<?= $title ?>" placeholder="Enter your search term..." name="ClassifiedSearch[title]" aria-label="Search">
<?= Html::submitButton('Search', ['class' => 'btn my-2 my-sm-0', 'name' => '']) ?>
<?php ActiveForm::end(); ?>
