<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="col-md-2 border-left pt-3 pb-3">
    <div class="ad-duration">
        <p>From: <?= date('d M Y', $model->start_date) ?><br>To: <?= date('d M Y', $model->end_date) ?></p>
    </div>
</div>
<div class="col-md-6 border-left border-right pt-3 pb-3">
    <div class="media ad-info">
        <img class="mr-3" src="images/golf-ad.png" alt="">
        <div class="media-body">
            <h5 class="mt-0"><?= $model->title ?></h5>
            <h5 class="mb-3"><?= Yii::$app->params[$model->currency] ?><?= $model->price ?></h5>
            <span><?php 

                $one = (string)date("Y-m-d",$model->end_date);
                $two = (string)date("Y-m-d");
                
//                print_r($one);exit;
                $date1 = date_create($one);
                $date2 = date_create($two);
                $diff = date_diff($date1,$date2);
                echo $diff->format("%a");
//        $datetime1 =  new Datetime(date('Y-m-d', $model->start_date));
//$datetime2 =  new Datetime(date('Y-m-d', $model->end_date));
//$interval = $datetime1->diff($datetime2)->days;

        ?> Days Left</span>
            <ul>
                <li><?= $model->town->town ?></li>
                <li><?= $model->county->county ?></li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-4 border-right pt-3 pb-3">
    <div class="actions">
        <div class="action-link mb-2">
            <a href="#">
                <img src="images/inactive.png" alt="">
                <span><?= ucfirst($model->status) ?></span>
            </a>
        </div>
<!--        <div class="action-link mb-2">
            <a href="#">
                <img src="images/bump.png" alt="">
                <span>Bump</span>
            </a>
        </div>-->
        <div class="action-link mb-2">
            <a href="<?= Url::to('/classified/update?id='.Html::encode($model->id)) ?>">
                <i class="fas fa-2x fa-pencil-alt" style="color:#c1a1f4;"></i>
                <span>Edit</span>
            </a>
        </div>
        <div class="action-link mb-2">
<!--            <a href="<?= Url::to('/classified/delete?id='.Html::encode($model->id)) ?>">
                <i class="fas fa-2x fa-times" style="color:#e21b1b;"></i>
                <span>Delete</span>
            </a>-->
            <a href="<?= Url::to('/classified/delete?id='.Html::encode($model->id)) ?>" title="Delete" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?" data-method="post"> <i class="fas fa-2x fa-times" style="color:#e21b1b;"></i>
                <span>Delete</span></a>
        </div>
    </div>
</div>