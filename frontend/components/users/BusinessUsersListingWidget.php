<?php
/**
 * Created by PhpStorm.
 * User: i5 Developer
 * Date: 9/11/2018
 * Time: 4:56 PM
 */

namespace frontend\components\users;


use yii\base\Widget;
use common\models\Users;

/**
 * Class ItemWidget
 * @property ClassifiedListings $model
 * @property string $cssClass
 * @package frontend\components
 */

class BusinessUsersListingWidget extends Widget
{
    public $model;
    public $cssClass;
    public $index;
    
    function run()
    {
        return $this->render('business-user-list', [
            'model' => $this->model,
            'cssClass' => $this->cssClass,
            'index' => $this->index
        ]);
    }

}