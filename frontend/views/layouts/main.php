<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\components\AlertWidgets;
use common\models\ClassifiedCategories;
use frontend\components\LogoWidget;
use frontend\components\HeaderMenusWidget;
use frontend\components\SearchFormWidget;
use frontend\components\FeaturedMenuWidget;
use frontend\components\MobileSearchWidget;
use frontend\components\CategoryListWidget;
use cinghie\mailchimp\widgets\Subscription;

$glbSetting = backend\models\base\Settings::find()->one();
\Yii::$app->params['glb'] = $glbSetting;
$county = "";
$town = "";
if(isset(\Yii::$app->params['glb']['county_id']) && !empty(\Yii::$app->params['glb']['county_id'])){
    $county = common\models\LocationsCounties::findOne(['id' => \Yii::$app->params['glb']['county_id']]);
}
if(isset(\Yii::$app->params['glb']['town_id']) && !empty(\Yii::$app->params['glb']['town_id'])){
    $town = common\models\LocationsTowns::findOne(['id' => \Yii::$app->params['glb']['town_id']]);
}

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="title" content="<?= (isset(\Yii::$app->params['glb']['meta_title']) && !empty(\Yii::$app->params['glb']['meta_title'])) ? \Yii::$app->params['glb']['meta_title'] : ""; ?>">
        <meta name="keywords" content="<?= (isset(\Yii::$app->params['glb']['meta_keywords']) && !empty(\Yii::$app->params['glb']['meta_keywords'])) ? \Yii::$app->params['glb']['meta_keywords'] : ""; ?>">
        <meta name="description" content="<?= (isset(\Yii::$app->params['glb']['meta_description']) && !empty(\Yii::$app->params['glb']['meta_description'])) ? \Yii::$app->params['glb']['meta_description'] : ""; ?>">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <?php if(Yii::$app->controller->action->id == 'create'): ?>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
        <?php endif; ?>
        
        <?php
        if(isset(\Yii::$app->params['glb']['analytics_code']) && !empty(\Yii::$app->params['glb']['analytics_code'])){
            echo \Yii::$app->params['glb']['analytics_code'];
        }
        ?>
        <style>
            .loader{
                position: fixed;
                z-index: 99999999;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: rgba(0,0,0,0.6);
                color: #fff;
                text-align: center;
            }
            .loader svg{
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                -webkit-transform: translate(-50%,-50%);
                -moz-transform: translate(-50%,-50%);
            }
        </style>
        
    </head>
    <body id="<?= (Yii::$app->controller->action->id == 'business-directory') ? 'directory-page' : 'contact-page' ?>">
        <?php $this->beginBody() ?>

        <div class="wrap">
            <header>
                <div class="top-bar">
                    <div class="container">
                        <div class="top-bar-content navbar navbar-default justify-content-between">
                            <?= LogoWidget::widget(); ?>
                            <div class="topbar-left d-md-flex flex-row d-none">
                                <?= SearchFormWidget::widget(); ?>
                                <ul>
                                    <li class="account d-none d-md-inline-block">
                                        <?php if (Yii::$app->user->isGuest) { ?>
                                            <a href="<?= Url::to('/signup'); ?>">Sign up</a> | <a href="<?= Url::to('/login') ?>">Login</a>

                                        <?php } else { ?>
                                            <div class="dropdown">
                                                <?= html::a('My Account', Url::to('/users/dashboard'), ['class'=>'dropdown-toggle', 'data-toggle'=>"dropdown"] ); ?>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <?= html::a("Dashboard", Url::to('/users/dashboard'), ['class'=>'dropdown-item'] ); ?>
                                                    <?= html::a(Yii::$app->user->identity->username, Url::to('/users/edit-profile'), ['class'=>'dropdown-item'] ); ?>
                                                </div>
                                            </div>
                                                | <a href="<?= Url::to('/site/logout') ?>" data-method="post">Logout</a>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <?= HeaderMenusWidget::widget(); ?>
            <div class="container">
                <div class="d-block d-lg-none">
                    <?= FeaturedMenuWidget::widget(); ?>
                </div>
                <?= MobileSearchWidget::widget(); ?>
            </div>
            <?php if (Yii::$app->controller->action->id == 'view-by-category' || Url::current() == '/classified/index' || Url::current() == Url::home()) { ?>
                <div class="categories-filter">
                    <div class="container pt-5 pb-5">
                        <div class="row">
                            <div class="col-md-10">
                                <?= CategoryListWidget::widget(); ?>
                            </div>
                            <div class="col-md-2">
                                <a href="<?= Url::to('/create') ?>" class="btn btn-default btn-block btn-lg">Place an ad <i class="fas fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="container">
                <?= AlertWidgets::widget(); ?>
                <div class="d-lg-block d-none">
                    <?= FeaturedMenuWidget::widget(); ?>
                </div>
            </div>
            <div class="content">
                <?= $content ?>
            </div>
        </div>
        <?php if (Yii::$app->controller->action->id != 'business-directory'): ?>
            <footer class="footer">
                <div class="container">
                    <div class="row">

                        <div class="col-md-4 col-sm-6 site-map mb-4">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-6">
                                    <h4>TheDealer.ie</h4>
                                    <div class="empty-space"></div>
                                    <ul class="nav flex-column">
                                        <li><?= html::a('Place an Ad', Url::to('/create')); ?></li>
                                        <li><?= html::a('Find Business', Url::to('/business')); ?></li>
                                        <!--<li><?= html::a('Latest Edition', '#'); ?></li>-->
                                        <li><?= html::a('Terms &amp; condtions', Url::to('/term-conditions')); ?></li>
                                        <li><?= html::a('FAQ', Url::to('/faq')); ?></li>
                                        <li><?= html::a('About', Url::to('/about')); ?></li>
                                        <li><?= html::a('Get in Touch', Url::to('/contact')); ?></li>
                                    </ul>
                                    <div class="empty-space"></div>
                                    <div class="empty-space"></div>
                                    <h4>My Account</h4>
                                    <div class="empty-space"></div>
                                    <ul class="nav flex-column">
                                        <?php if (Yii::$app->user->isGuest) { ?>
                                            <li><?= html::a('Login/Register', Url::to('/login')); ?></li>
                                        <?php } ?>
                                        <li><?= html::a('Dashboard', Url::to('/users/dashboard')); ?></li>
                                        <li><?= html::a('My Ads', Url::to('/users/my-ads')); ?></li>
                                        <li><?= html::a('My Whatchlist', '/users/my-watchlist'); ?></li>
                                        <li><?= html::a('My Online Issues', '#'); ?></li>
                                        <li><?= html::a('Buy Credit', Url::to('/users/buy-credit')); ?></li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-sm-6 col-6 border-left">
                                    <h4>Search Ads</h4>
                                    <div class="empty-space"></div>
                                    <?php $categories = ClassifiedCategories::find()->getCategories()->all(); ?>
                                    <ul class="nav flex-column">
                                        <?php foreach ($categories as $category) : ?>
                                            <li>
                                                <?= html::a($category->category, Url::to('/classified/search?ClassifiedSearch[slug]=' . Html::encode($category->slug))); ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 contact-us mb-4">
                            <h4>Sign up to get the latest news</h4>
                            <div class="empty-space"></div>
                            <div class="subscribe">
                                <div class="row mb-3">
                                    <?=
                                    Subscription::widget([
                                        'apiKey' => '83c260b2c7b8981fe8960bc82aea4610-us18',
                                        'list_id' => '73d3940554' // if not set raise Error
                                    ])
                                    ?>
                                </div>
                                <a class="btn btn-lg btn-block feedback" href="<?= Url::to('/contact') ?>">Give us your feedback</a>
                            </div>
                            <div class="empty-space"></div>
                            <div class="empty-space"></div>
                            <div class="empty-space"></div>
                            <div class="contact">
                                <h4>Need Help?</h4>
                                <p>We're here to<br>answer any questions you have</p>
                                <div class="info mail-us">
                                    <i class="fal fa-envelope fa-fw fa-lg"></i>
                                    <a href="mailto:<?= (isset(\Yii::$app->params['glb']['email']) && !empty(\Yii::$app->params['glb']['email'])) ? \Yii::$app->params['glb']['email'] : ""; ?>">
                                        <?= (isset(\Yii::$app->params['glb']['email']) && !empty(\Yii::$app->params['glb']['email'])) ? \Yii::$app->params['glb']['email'] : ""; ?>
                                    </a>
                                </div>
                                <div class="info call-us">
                                    <span class="fa-layers fa-fw fa-lg">
                                        <i class="far fa-circle"></i>
                                        <i class="fa-inverse fas fa-phone" data-fa-transform="shrink-8  flip-h"></i>
                                    </span>
                                    <a href="tel:<?= (isset(\Yii::$app->params['glb']['telephone']) && !empty(\Yii::$app->params['glb']['telephone'])) ? \Yii::$app->params['glb']['telephone'] : ""; ?>">
                                        <?= (isset(\Yii::$app->params['glb']['telephone']) && !empty(\Yii::$app->params['glb']['telephone'])) ? \Yii::$app->params['glb']['telephone'] : ""; ?>
                                    </a>
                                </div>
                                <div class="follow-us mt-3">
                                    <a href="https://www.facebook.com/TheDealerFreeAdsPaper">
                                        <i class="fab fa-facebook-f fa-4x" data-fa-transform="shrink-6" data-fa-mask="fas fa-circle"></i>
                                    </a>
                                    <a href="https://twitter.com/thedealer_ie">
                                        <i class="fab fa-twitter fa-4x" data-fa-transform="shrink-6" data-fa-mask="fas fa-circle"></i>
                                    </a>
                                    <a href="https://www.facebook.com/TheDealerFreeAdsPaper">
                                        <i class="fab fa-google-plus-g fa-4x" data-fa-transform="shrink-6" data-fa-mask="fas fa-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 mb-4">
                            <div class="row">
                                <div class="col-md-12 col-sm-6 mb-3">
                                    <div class="contact-office">
                                        <h4>Our Offices</h4>
                                        <div class="media mb-4">
                                                <?= Html::img('/images/map.jpg', ['alt' => 'Address', 'class' => 'map mr-2']); ?>
                                            <div class="media-body">
                                                <p><?= $town['town'] ?>, <?= $county['county'] ?> <br> <?= (isset(\Yii::$app->params['glb']['telephone']) && !empty(\Yii::$app->params['glb']['telephone'])) ? \Yii::$app->params['glb']['telephone'] : ""; ?></p>
    <?= Html::a('Get Direction', '#', ['class' => 'btn direction-link']) ?>
                                            </div>
                                        </div>
                                        <div class="sperator"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <div class="fb-page" data-href="https://www.facebook.com/TheDealerFreeAdsPaper" data-tabs="timeline" data-height="250" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true">
                                        <blockquote cite="https://www.facebook.com/TheDealerFreeAdsPaper" class="fb-xfbml-parse-ignore">
                                            <a href="https://www.facebook.com/TheDealerFreeAdsPaper"><?= (isset(\Yii::$app->params['glb']['company']) && !empty(\Yii::$app->params['glb']['company'])) ? \Yii::$app->params['glb']['company'] : ""; ?></a>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="copy-rights">
                <div class="container">
                    <p class="text-center"><?= \Yii::$app->params['glb']['copyright'] ?></p>
                </div>
            </div>
        <?php endif; ?>


<?php $this->endBody() ?>
        <div id="fb-root"></div>
        <script>
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlHHj7YvFQ4yid_RRm4v-A4-ryOdwtcwk"></script>    
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
        </script>                
        <?php
        if(Yii::$app->controller->action->id == 'business-directory'):
        ?>
        <script src="/js/map.js">  </script>
        <?php endif; ?>
        <?php
        if(Yii::$app->controller->action->id == 'business-details'):
        ?>
        <script src="/js/single-map.js">  </script>
        <?php endif; ?>
        
        <?php if(Yii::$app->controller->action->id == 'create'): ?>
        <script src="js/piexif.min.js" type="text/javascript"></script>
        <script src="/js/fileinput.min.js"></script>
        
        <script>
        $("#adsimages").fileinput({
            uploadUrl: "/",
            'showUpload':false,
            'previewFileType':'image',
            allowedFileTypes: ['image'],
            allowedFileExtensions: ["jpg","jpeg","png", "gif"],
            minImageWidth: '768px',
            maxFileCount: 6,
            fileActionSettings: {
                showRemove: true,
                showUpload: false,
                removeIcon: '<i class="fa fa-trash"></i>',
                removeTitle: 'title',
                showZoom: false
            }
             
        });
        
        </script>
        
        <?php endif; ?>
        
    </body>
</html>
<?php $this->endPage() ?>
