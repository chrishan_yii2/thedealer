<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\LinkPager;

$this->title = Yii::t('app', 'My Ads');

?>

<div class="my-adverts">
    <div class="container">
        <div class="adverts-wrapper mt-5">
            <h3 class="page-title mb-5">My Adverts</h3>
            <div class="adverts-btn text-center mb-4">
                <a href="<?= Url::to('/create') ?>" class="btn btn-default btn-lg mr-2"><i class="fas fa-plus fa-fw"></i> PLACE AD</a>
                <!--<a href="" class="btn btn-grey btn-lg">MY DASH</a>-->
                <?= Html::a('MY DASH', Url::to('dashboard'), ['class'=> 'btn btn-grey btn-lg']) ?>
            </div>
            <div class="adverts-list">
                <div class="row">
                                            
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView'     => '_item',
                        'layout'       => '<div class="col">'.
                                                '<div class="row ad-list-head border-bottom pt-2 pb-2">
                                                    <div class="col-md-2">Date</div>
                                                    <div class="col-md-6">Title</div>
                                                    <div class="col-md-4">Action</div>
                                                </div>'.
                                            '{items}</div>',
                        'itemOptions'  => [
                                'class' => 'row ad-list border-bottom',
                        ],
                        'options' => [
                            'class' => 'w-100'
                        ]
                    ]); ?>
                </div>
            </div>
            <div class="empty-space"></div>
            <div class="empty-space"></div>
            <nav aria-label="page navigation">
                <?php
                
                echo LinkPager::widget([
                    'pagination' => $dataProvider->pagination,
                    'options' => [
                        'class' => 'pagination justify-content-end'
                    ],
                    'linkContainerOptions' => [
                        'class' => 'page-item'
                    ],
                    'linkOptions' => [
                        'class' => 'page-link'
                    ]
                ]);
                
                ?>
            </nav>
            
        </div>
    </div>
</div>
