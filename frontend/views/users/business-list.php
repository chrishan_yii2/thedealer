<?php

use yii\widgets\ListView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = "Business Directory";

?>
<div class="directory-wrapper">
    <div class="container">
        <div class="search-form">
            <div class="form">
                <form id="directory_find" action="" method="get">
                    <div class="row no-gutters">
                        <div class="col-md-7 col-sm-7 mb-2">
                            <div class="form-group required mr-0 mr-sm-2">
                                <input type="text" class="form-control directory_category" name="UsersSearch[company_name]" placeholder="Search for a business or service">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-8 mb-2">
                            <div class="form-group required mr-2 ">
                                <!--<input type="text" class="form-control directory_location" name="UsersSearch[town]" placeholder="Town or County">-->
                                <select class="form-control select2 directory_location">
                                    <option value="">Select County or Town</option>
                                    <?php 
                                    foreach ($countyTowns as $t => $country):
                                    ?>
                                    <option value="<?= $t ?>"><?= $country ?></option>
                                    <?php 
                                        endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-4 text-center">
                            <button type="submit" class="btn btn-block btn-default">GO</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="directories-location">
        <div id="map_canvas" style="width:100%;height:100%;"></div>
        
        <div class="card directories-list">
            <div class="card-header">
                Results around Ireland & Northern Ireland
            </div>
            <div id="scroll-div" class="card-body p-0">
                <ul id="bus_listings" class="list-group">
                    
                </ul>
                <?php 
                
                /*ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView'     => function ($model, $key, $index, $widget) {
                        return $this->render('business-user-list',['model' => $model,'index' => $index]);
                    },
                    'layout'       => '<ul id="bus_listings" class=" bus_listings">{items}</ul>',
                    'itemOptions'  => [
                            'class' => 'list-group-item',
                    ],
                ]);*/ ?>
            </div>
            <div class="card-footer">
                <nav id="pagenav" aria-label="page navigation" style="max-width: 100%; overflow-x: auto">
                    <?php
                    /*
                    echo LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'options' => [
                            'class' => 'page_numbers pagination justify-content-start'
                            
                        ],
                        'linkContainerOptions' => [
                            'class' => 'page-item'
                        ],
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ]);
                    */
                    ?>
                </nav>
            </div>
        </div>
    </div>
</div>