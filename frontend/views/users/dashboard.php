<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;



$this->title = Yii::t('app', 'Dashboard');

?>
<div class="dashboard">
    <div class="container">
        <div class="dash-content-wrapper mt-5">
            <h3 class="page-title mb-5">What would you like to do today?</h3>
            <div class="dash-content mt-4">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="row dash-menus mb-md-0 mb-4">
                            <div class="col-md-6 col-sm-6 col-6 mx-auto pr-2">
                                <div class="my-ads menus-item">
                                    <a href="<?= yii\helpers\Url::to('my-ads'); ?>">
                                        <div class="menus-img">
                                            <?= Html::img('/images/myads.png', ['alt'=>'My Ads', 'class'=>'img-fluid']); ?>
                                        </div>
                                        <h4 class="action-head">My Ads</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6 mx-auto pl-2">
                                <div class="whatchlist menus-item">
                                    <a href="<?= yii\helpers\Url::to('my-watchlist'); ?>"> 
                                        <div class="menus-img">
                                            <?= Html::img('/images/watchlist.png', ['alt'=>'My Ads', 'class'=>'img-fluid']); ?>
                                        </div>
                                        <h4 class="action-head">Watchlist</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6 mx-auto pr-2">
                                <div class="credit menus-item">
                                    <a href="<?= yii\helpers\Url::to('/users/buy-credit'); ?>">
                                        <div class="menus-img">
                                            <?= Html::img('/images/credit-card.png', ['alt'=>'My Ads', 'class'=>'img-fluid']); ?>
                                        </div>
                                        <h4 class="action-head">Credit</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6 mx-auto pl-2">
                                <div class="edit_account menus-item">
                                    <a href="<?= yii\helpers\Url::to('edit-profile'); ?>">
                                        <div class="menus-img">
                                            <?= Html::img('/images/edit_account.png', ['alt'=>'My Ads', 'class'=>'img-fluid']); ?>
                                        </div>
                                        <h4 class="action-head">Edit Account</h4>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="start_selling">
                            <h4>Place a free Advert</h4>
                            <p>Four months online, ten large photos and listing in local printed edition.</p>
                            <a href="/create" class="btn btn-default btn-lg mt-3"><strong>START SELLING</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>