<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\LocationsCounties;
use common\models\LocationsTowns;

$this->title = 'Edit Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="form-wrapper">
        <div class="form signup-form">
            <h2 class="form-head"><?= Html::encode('Edit Account') ?></h2>
        </div>
        <?php $form = ActiveForm::begin(); ?>
        <div class="row m-0">
            <div class="col-md-6">
                <h3>Contact Details</h3>
                <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>
                <?php if ($model->user_type == 'business'){ ?>
                    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
                <?php } ?>
                <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'email')->textInput(['maxlength' => true,'disabled' => true]) ?>
                <h3>Address Details</h3>
                <?= $form->field($model, 'address_one')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'address_two')->textInput(['maxlength' => true]) ?>
                <?=
                $form->field($model, 'county_id')->dropDownList(
                        yii\helpers\ArrayHelper::map(\common\models\LocationsCounties::find()->all(), 'id', 'county'), ['prompt' => 'Select Country']
                )
                ?>

                <?=
                $form->field($model, 'town_id')->dropDownList(
                        yii\helpers\ArrayHelper::map(\common\models\LocationsTowns::find()->all(), 'id', 'town'), ['prompt' => 'Select Town']
                )
                ?>
                <?= $form->field($model, 'postcode')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="col-md-6">
                <h3>Account Details</h3>
                <?= $form->field($model, 'newsletter')->dropDownList(['no' => 'No', 'yes' => 'Yes',], ['prompt' => '']) ?>

                <?=
                $form->field($model, 'marketing_id')->dropDownList(
                        yii\helpers\ArrayHelper::map(\common\models\MarketResearch::find()->all(), 'id', 'market_research'), ['prompt' => 'Please Select']
                )->label('How user they found us')
                ?>
                <h3>Login Details</h3>
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true,'disabled' => true]) ?>
                <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-block']) ?>
                </div>
            </div>
        </div>


    <?php ActiveForm::end(); ?>

    </div>
</div>