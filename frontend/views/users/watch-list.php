<?php

use common\models\Images;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\ClassifiedListings;

?>
<div class="my-watchlist">
    <div class="container">
        <div class="watchlist-wrapper mt-5">
            <h3 class="page-title mb-5">Watchlist</h3>
            <div class="watchlist-list">
                <div class="row">
                    <div class="col">
                        <div class="row watchlist-head pt-2 pb-2">
                            <div class="col-md-5 col-sm-5 col-8">Product Item</div>
                            <div class="col-md-1 col-sm-2 col-4">Price</div>
                            <div class="col-md-6 col-sm-5 col-12"></div>
                        </div>
                        <?php
//                        echo "<pre>";
//                        print_r($model);exit;
                        foreach ($model as $w => $watchlist):
                        ?>
                        <div class="row watchlist">
                            <div class="col-md-5 col-sm-5 col-8 pb-2 pt-2">
                                <div class="media watchlist-info">
                                    <?php
                                    $images = Images::find()->select('image_name')->getImages(ClassifiedListings::tableName(),$watchlist->classifiedListings->id)->one();
                                    if(!empty($images)){
                                        $imagePath = \Yii::getAlias('@webroot').'/uploads/small/'.$images->image_name;
                                    }else{
                                        $imagePath = "";
                                    }

                                    ?>
                                    <?php if (file_exists($imagePath)) { ?>
                                        <?= Html::img('/uploads/small/'.$images->image_name, ['alt'=> $watchlist->classifiedListings->title, 'title'=>$watchlist->classifiedListings->title, 'class'=>'mr-3' ]); ?>
                                    <?php } else { ?>
                                        <?= Html::img('/images/no_images_thumb.jpg', ['alt'=> $watchlist->classifiedListings->title, 'title'=>$watchlist->classifiedListings->title, 'class'=>'mr-3' ]) ?>
                                    <?php } ?>
                                    <div class="media-body">
                                        <h5 class="product-title"><?= $watchlist->classifiedListings->title ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-2 col-4 pb-2 pt-2">
                                <h5 class="price"><?= Yii::$app->params[$watchlist->classifiedListings->currency].$watchlist->classifiedListings->price ?></h5>
                            </div>
                            <div class="col-md-6 col-sm-5 col-12 pb-2 pt-2">
                                <div class="actions">
                                    <!--<a href="add-to-cart.html" class="btn btn-default buy-now mr-md-1 mb-2">Buy Now</a>-->
                                    
                                    <a href="<?= Url::toRoute(['/users/remove-watchlist','id' => $watchlist->listing_id]) ?>" class="btn btn-grey remove mb-2">Remove</a>
                                    
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
