<?php

/** @var Item $model */

use frontend\components\users\UsersAdsListingWidget;

echo UsersAdsListingWidget::widget(['model' => $model]);