<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use common\models\Images;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\WatchlistLookup;
use common\models\ClassifiedListings;
use ymaker\social\share\widgets\SocialShare;

$this->title = $dataProvider->models[0]->company_name;
//echo "<pre>";
//print_r($dataProvider->models);
?>

<div class="content-wrapper directory-detais-wrap mt-5">
    <div class="container">
        <div class="directory-details mb-5">
            <h2 class="sec-heading mb-4"><?= $dataProvider->models[0]->company_name ?></h2>
            <div class="direction-form">
                <div class="form">
                    <form id="direction-form" action="#" onsubmit="setDirections(this.location.value); return false">
                        <div class="row no-gutters">
                            <div class="col-lg-10 col-md-9 col-sm-8 mb-2">
                                <div class="form-group required mb-0 mr-0 mr-sm-2">
                                    <input type="text" class="form-control" name="location" placeholder="Enter Your Location">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-4 mb-2">
                                <button type="submit" class="btn btn-lg btn-block btn-grey" name="direction-button">Get Direction</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="directory-info-wrap">
                <div class="row">
                    <div class="col-md-5 col-lg-4 mb-3 mb-md-0">
                        <div class="directory-info border">
                            <div class="contact-name border-bottom">
                                <h4 class="sub-sec-heading">Contact</h4>
                                <p class="sec-content"><?= $dataProvider->models[0]->company_name ?></p>
                            </div>
                            <div class="address border-bottom">
                                <h4 class="sub-sec-heading">Address</h4>
                                <p class="sec-content"><?= $dataProvider->models[0]->address_one ?></p>
                                <p class="sec-content"><?= (isset($dataProvider->models[0]->address_two) && !empty($dataProvider->models[0]->address_two)) ? $dataProvider->models[0]->address_two : "";  ?></p>
                            </div>
                            <div class="phone border-bottom">
                                <h4 class="sub-sec-heading">Tel</h4>
                                <p class="sec-content"><?= $dataProvider->models[0]->telephone ?></p>
                            </div>
                            <div class="web-info border-bottom">
                                <h4 class="sub-sec-heading">Website</h4>
                                <p class="sec-content">
                                    <?php $website = (isset($dataProvider->models[0]->website) && !empty($dataProvider->models[0]->website)) ? $dataProvider->models[0]->website : "";  ?>
                                    <a href="<?= $website ?>"><?= $website ?></a>
                                </p>
                            </div>
                            <div class="email-info">
                                <h4 class="sub-sec-heading">Email</h4>
                                <p class="sec-content">
                                    <a href="mailto:<?= $dataProvider->models[0]->email ?>"><?= $dataProvider->models[0]->email ?></a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-7 col-lg-8 location-map">
                        <div id="map_canvas_small" style="width:100%;height:400px;"></div>
                        <div id="directions-panel"></div>
                        <p class="location-cords">
                            <span class="mr-3">Lat:<span id="user-lat"><?= $dataProvider->models[0]->user_lat ?></span>
                            <span>Long:<span id="user-long"><?= $dataProvider->models[0]->user_long ?></span></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="page-action">
                <a class="mr-2" href="javascript:window.print()">Print</a> | <a class="ml-2" href="#">Share</a>
                <?= SocialShare::widget([
                    'configurator'  => 'socialShare',
                    'url'           => Url::to('/users/business-details?UsersSearch[id]='.Html::encode($dataProvider->models[0]->id), true),
                    'title'         => Html::encode($dataProvider->models[0]->company_name),
                    'description'   => Html::encode($dataProvider->models[0]->description),
                    'containerOptions' => ['tag' => 'div', 'class' => 'social-share mb-4'],
                    'linkContainerOptions' => ['tag' => false],
                ]); ?>
            </div>
        </div>
        <?php if(isset($ads) && count($ads) > 0 ): ?>
        <div class="user-ads mb-4">
            <h3 class="sec-heading">User Ads</h3>
            <div class="ads-listing">
                <div class="row no-gutters">
                    <?php
                        foreach ($ads as $r => $relatedAd):

                    ?>
                    <div class="col-md-3 col-sm-4 col-xs-6 ads-grid">
                        <a class="ads-grid-content border" href="<?= Url::to('/classified/view?ClassifiedSearch[id]='.Html::encode($relatedAd->id)) ?>">
                            <?php
                            $images = Images::find()->select('image_name')->getImages(ClassifiedListings::tableName(),$relatedAd->id)->one();
                            if(!empty($images)){
                                $imagePath = \Yii::getAlias('@webroot').'/uploads/small/'.$images->image_name;
                            }else{
                                $imagePath = "";
                            }

                            ?>
                            <?php if (file_exists($imagePath)) { ?>
                                <?= Html::img('/uploads/small/'.$images->image_name, ['alt'=> $relatedAd->title, 'title'=>$relatedAd->title, 'class'=>'ads_img' ]); ?>
                            <?php } else { ?>
                                <?= Html::img('/images/no_images_thumb.jpg', ['alt'=> $relatedAd->title, 'title'=>$relatedAd->title, 'class'=>'ads_img' ]) ?>
                            <?php } ?>
                            <div class="meta-info border-bottom">
                                <span class="price"><?= Yii::$app->params[$relatedAd->currency] ?></sup><?= $relatedAd->price ?></span>
                                <span class="watch ajaxSave float-right" data-url="<?= Url::toRoute(['/users/watch-list']) ?>" data-id="<?= $relatedAd->id ?>">
                                    <i class="far fa-heart fa-fw fa-lg"></i>
                                </span>
                            </div>
                            <h4 class="ad-title"><?= $relatedAd->title ?></h4>
                            <p class="location"><?= $relatedAd->town->town."," .$relatedAd->county->county ?></p>
                        </a>
                    </div>
                    <?php
                        endforeach;
                    ?>
                </div>
            </div>
            <?php if(count($ads) > 8): ?>
            <div class="text-center mt-4 mb-5">
                <a href="<?= Url::to('/classified/search?ClassifiedSearch[user_id]='.$dataProvider->models[0]->id) ?>" class="btn btn-default btn-lg">See All</a>
            </div>
            <?php endif; ?>
        </div>
        <?php  endif; ?>
    </div>
</div>