<div class="buy-credit">
    <div class="container">
        <div class="credit-wrapper mt-4">
            <h3 class="page-title mb-3">Your Account Balance is &#8364;0.00</h3>
            <p class="page-title-desc">You require a minimum of €3 in your account to purchase this online issue.</p>
            <p class="page-title-desc">Please top up your account with a minimum of €3.</p>
            <div class="credit-method mt-4">
                <h3>Buy Credit</h3>
                <div class="form buy-credit-form"> 
                    <form action="#" method="post" accept-charset="utf-8" class="form">
                        <div class="form-group required">
                            <div class="cstm-radio" aria-required="true">
                                <label class="radio-inline d-block">
                                    <input type="radio" name="BuyCredit[method]" value="paypal" checked> 
                                    <img src="/images/paypal_logo.png" alt="">
                                </label>
                                <label class="radio-inline d-block">
                                    <input type="radio" name="BuyCredit[method]" value="visa"> 
                                    <img src="/images/visa_logo.png" alt="">
                                </label>
                                <label class="radio-inline d-block">
                                    <input type="radio" name="BuyCredit[method]" value="mastercard"> 
                                    <img src="/images/mastercard_logo.png" alt="">
                                </label>
                            </div>
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>