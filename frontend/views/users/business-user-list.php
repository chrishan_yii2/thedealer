<?php

/** @var Item $model */

use frontend\components\users\BusinessUsersListingWidget;

echo BusinessUsersListingWidget::widget(['model' => $model,'index' => $index]);