<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\LocationsCounties;
use common\models\LocationsTowns;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="form-wrapper">
        <div class="form signup-form">
            <h2 class="form-head"><?= Html::encode('Get Started with a Free Account') ?></h2>
            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'enableAjaxValidation' => false,]); ?>

            <?= $form->field($model, 'user_type', ['inlineRadioListTemplate'=>'<div class="cstm-radio" id="signupform-user_type">{beginLabel}{input}{labelTitle}{endLabel}{error}{hint}</div>'])->inline()->radioList(['private'=>'Private Seller', 'business'=>'Business Seller'])->label(false); ?>

            <?= $form->field($model, 'company_name', ['inputOptions' => ['placeholder' => 'Company Name*',],])->label(false) ?>
            <div class="row username-wrapper">
                <div class="col-md-6">
                    <?= $form->field($model, 'username', ['inputOptions' => ['placeholder' => 'User Name*',],])->label(false) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => 'Password*',],])->passwordInput()->label(false) ?>
                </div>
            </div>
            <div class="show_pass">
                <?= $form->field($model, 'show_pass', [
                                        'inputTemplate' => '
                                            <i class="far fa-square fa-lg unchecked"></i>
                                            <i class="far fa-check-square fa-lg checked"></i>
                                                {input}',
                                    ])->checkbox()->label('Show Password'); ?>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'contact_name', ['inputOptions' => ['placeholder' => 'Full Name*',],])->label(false) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'Email*',],])->label(false) ?>
                </div>
            </div>
            <?= $form->field($model, 'telephone', ['inputOptions' => ['placeholder' => 'Primary Telephone*',],])->label(false) ?>
            <?= $form->field($model, 'address_one', ['inputOptions' => ['placeholder' => 'Address*',],])->label(false) ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'county_id')
                                        ->dropDownList(
                                                ArrayHelper::map(
                                                        LocationsCounties::find()->all(),
                                                        'id', 'county'),
                                                [
                                                    'prompt'=>'Select Country',
                                                    'onchange'=> '$.post("/site/lists?id='.'"+$(this).val(),function(data){
                                                            $("select#signupform-town_id").html(data);}
                                                            );'
                                                ])->label('County*') ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'town_id')->dropDownList(ArrayHelper::map(LocationsTowns::find()->all(), 'id', 'town'),['prompt'=>'Select Town'])->label('Town*') ?>
                </div>
            </div>
            <div class="empty-space"></div>
            <div class="empty-space"></div>
            <div class="form-group text-center">
                <?= Html::submitButton('Create My Account', ['class' => 'btn btn-lg btn-default', 'name' => 'signup-button']) ?>
            </div>
            <div class="text-center">
                <?= $form->field($model, 'activeNL', [
                                        'inputTemplate' => '
                                            <i class="far fa-square fa-lg unchecked"></i>
                                            <i class="far fa-check-square fa-lg checked"></i>
                                                {input}',
                                        ])->checkbox()->hint('In submitting this form you agree to our terms and conditions.', ['class'=>'submit-info'])->label('Subscribe to our Newslatter'); ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>
