<div class="container">
    <div class="terms-wrapper">
        <h4 class="page-heading">FREQUENTLY ASKED QUESTIONS</h4>
        <div class="terms-accordion" id="accordion">
            <div class="accordion-group mb-2">
                <h4 class="heading" data-toggle="collapse" data-target="#accordion-1" aria-expanded="true" aria-controls="accordion-1">
                    How do I report a scam?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse show accordion-desc" id="accordion-1" data-parent="#accordion">
                    <p>We are always eager to hear of unscrupulous traders/advertisers. If you have any information you feel would be important, please contact us on 074 9126410. Please be assured that your confidentiality would be guaranteed by law under the Data Protection Act (1988).</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-2" aria-expanded="true" aria-controls="accordion-2">
                    How do I report an advert which may be illegal or abusive?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-2" data-parent="#accordion">
                    <p>All adverts online have a 'Report Ad' link which allows you to highlight adverts which may be abusive or illegal. The link can be found on the right hand side, above the advert copy. The information you provide will help our customer services team decide the legality of the advert.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-3" aria-expanded="true" aria-controls="accordion-3">
                    Can you remove an advert for an illegal item?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-3" data-parent="#accordion">
                    <p>As and when we can establish the illegality of an advert appearing either online or within our publications, we will remove the advert and put steps in place to ensure that it doesn't appear again.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-4" aria-expanded="true" aria-controls="accordion-4">
                    What can I do if I think these goods may be stolen?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-4" data-parent="#accordion">
                    <p>If you have reason to believe that items purchased through The Dealer "Free-Ads" Paper or TheDealer.ie were stolen, please contact your local Garda Station. You'll find contact details for your area in the local phone book and the Gardai will be able to advise you on the correct course of action to employ.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-5" aria-expanded="true" aria-controls="accordion-5">
                    What can I do if an item stolen from me has appeared on your site or paper?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-5" data-parent="#accordion">
                    <p>If you have reason to believe that items stolen from you have appeared in The Dealer "Free-Ads" Paper or TheDealer.ie, please contact your local Garda Station. You'll find contact details for your area in the local phone book and the Gardai will be able to advise you on the correct course of action to employ. Please also send information relating to this to the The Dealer "Free-Ads" Paper, High Road, Letterkenny, Co. Donegal or phone us on 074 9126410 or email info@thedealer.ie. Please be assured that your confidentiality would be guaranteed by law under the Data Protection Act (1988).</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-6" aria-expanded="true" aria-controls="accordion-6">
                    How can I be careful when meeting buyers at home?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-6" data-parent="#accordion">
                    <p>Most private advertisers sell their goods from home. For personal ease and safety, always ensure that there is a friend with you before you allow someone into your home.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-7" aria-expanded="true" aria-controls="accordion-7">
                    Are there any extra safety precautions I can take when selling valuable items?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-7" data-parent="#accordion">
                    <p>When selling valuable items, such as a car, you could ask for a personal cheque to be cleared before you part with your goods, and offer a receipt as proof of purchase. In the case of bankers' drafts, it is wise to go to the bank together and watch it being drawn up.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-8" aria-expanded="true" aria-controls="accordion-8">
                    What is the best way to accept money?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-8" data-parent="#accordion">
                    <p>For items that are worth only a few pounds, it's easier and more convenient to pay in cash. To be safe, when you go to visit the person selling the item you are interested in, don’t take more money than you are planning on spending.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-9" aria-expanded="true" aria-controls="accordion-9">
                    Should I send the goods before receiving the money?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-9" data-parent="#accordion">
                    <p>In both cases, the cheque will clear into your bank, only to be stopped/refused weeks later. At this point, the Banks/Building Societies will take the full cheque amount back out of your account. Not only will you have lost the goods, you will be out of pocket for the amount of their original cheque.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-10" aria-expanded="true" aria-controls="accordion-10">
                    What can I do when I've sent the advertiser my money but he/she has not sent the goods?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-10" data-parent="#accordion">
                    <p>Unfortunately, we are unable to interfere in the private transactions between a buyer and a seller. However, if you can provide us with a letter of complaint, we will keep this on permanent file under the advertiser's records. You may wish to contact your local citizen’s advice bureau, who will be able to advise you on the best course of action to take. The number for your local office can be found in the phone book.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-11" aria-expanded="true" aria-controls="accordion-11">
                    What should I do when the advertiser wants me to send cash up front?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-11" data-parent="#accordion">
                    <p>Don't do it. We recommend that buyers NEVER send cash or cheques through the post, or place money directly into a seller's bank account, in advance of receiving goods.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-12" aria-expanded="true" aria-controls="accordion-12">
                    What if the item turns out to be faulty?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-12" data-parent="#accordion">
                    <p>Once you have bought an item from a private seller, you have no legal comeback if it turns out to be faulty. Therefore, check the goods thoroughly before handing over the money. If the seller still has the receipts, or a warranty, so much the better.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-13" aria-expanded="true" aria-controls="accordion-13">
                    How can I be careful when meeting sellers at their home?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-13" data-parent="#accordion">
                    <p>Most private advertisers sell their goods from home. For personal ease and safety, always ensure you take someone with you. If you're answering a home service ad, ask the advertiser to produce identity and proof of qualification before inviting them into your home.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-14" aria-expanded="true" aria-controls="accordion-14">
                    What if travel is involved in a transaction?
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-14" data-parent="#accordion">
                    <p>If travel is involved in the transaction, always remember to write down the contact details of the people you are visiting, or the people who are visiting you. If, for whatever reason, you are unable to make the appointment, notify them in advance</p>
                </div>
            </div>
        </div>
    </div>
</div>