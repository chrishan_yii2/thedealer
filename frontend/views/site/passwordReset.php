<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="wrapper mt-5 mb-5">
        <div class="form pass-rest-form">
            <h2 class="form-head"><?= Html::encode('Forgot your Password?') ?></h2>
            <h2 class="form-head"><?= Html::encode('No Problem.') ?></h2>
            <p class="text-center">Enter your email address and we'll send you a new password.</p>
            <?php $form = ActiveForm::begin(['id' => 'password-reset-form']); ?>
            <?= $form->field($model, 'email', [
                                        'inputTemplate' => '
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fas fa-fw fa-2x fa-envelope"></i>
                                                    </span>
                                                </div>
                                                {input}
                                            </div>',
                                        'inputOptions' => [
                                            'placeholder' => 'Email*',
                                        ],
            ])->label(false) ?>
            <div class="empty-space"></div>
            <div class="empty-space"></div>
            <div class="form-group text-center">
                <?= Html::submitButton('RESET PASSWORD', ['class' => 'btn btn-lg btn-default', 'name' => 'pass-reset-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
