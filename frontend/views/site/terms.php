<div class="container">
    <div class="terms-wrapper">
        <h4 class="page-heading">Terms & Conditions</h4>
        <div class="terms-accordion" id="accordion">
            <div class="accordion-group mb-2">
                <h4 class="heading" data-toggle="collapse" data-target="#accordion-1" aria-expanded="true" aria-controls="accordion-1">
                    General
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse show accordion-desc" id="accordion-1" data-parent="#accordion">
                    <p>This website is owned and operated by The Dealer “Free-Ads” Paper LTD, High Road, Letterkenny, Co Donegal. For the purpose of these terms and conditions "we", "our" and "us" refers to The Dealer “Free-Ads” Paper LTD. Your use of this website is subject to our terms and conditions of use set out below ("Terms of Use"), and by accessing and using this website you are agreeing, and you are signifying your agreement, to be bound by these Terms of Use. If you do not read or agree our Terms of Use, you should not access or use this website.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-2" aria-expanded="true" aria-controls="accordion-2">
                    Changes in Terms &amp; Conditions
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-2" data-parent="#accordion">
                    <p>The Dealer “Free-Ads” Paper LTD may modify or terminate any facilities and/or services offered through this website from time to time, for any reason and without notice, and without liability to you, any other user or any third party. The Dealer “Free-Ads” Paper LTD reserves the right to change the content, presentation, performance, user facilities and/or availability of any part of this website at its sole discretion, including these Terms of Use from time to time. You should check these Terms of Use for any changes each time you access this website. Your continued use of the website will signify your acceptance of any revised Terms of Use.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-3" aria-expanded="true" aria-controls="accordion-3">
                    Age and Responsibility
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-3" data-parent="#accordion">
                    <p>If you are under the age of 16, you may only use this website in conjunction with and under the supervision of your parents or guardian. By using this website you understand that you are financially responsible for all use of this website by you and those using your log-on information. You agree to notify us immediately of any unauthorised use or any other breach of security.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-4" aria-expanded="true" aria-controls="accordion-4">
                    Use of Website
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-4" data-parent="#accordion">
                    <p>Unless otherwise specified, this website and any of its facilities and/or services are for your personal and non-commercial use only. You may download, display or print part of this website for your own personal, non-commercial use. You may save a copy of this page to your local hard disk for the purposes of creating one personal back-up copy. You agree not to copy, modify, transmit, display, distribute, perform, reproduce, licence, publish, create derivative works from, transfer or sell any information, software, products and services contained on or forming part of this website, or otherwise use such content of this website for resale, re-distribution or for any other commercial use for including any surveys, contests or pyramid schemes, nor use the service to participate in or cause others to participate in sending chain letters, junk email, spam, duplicate or unsolicited communications.</p>
                    <p>You will not use this website or any of its facilities and/or services for any purpose that is unlawful or prohibited by these Terms of Use. You acknowledge that this website has been specifically designed for use in the Republic of Ireland and Northern Ireland, and you agree not to use or access it in and from jurisdictions in which it or its contents are restricted or prohibited by local law. You will not use this website in any manner which could damage, disable, overburden or impair this website, its facilities and/or services or interfere with any other party's use and/or enjoyment of this website, its facilities and/or services. You may not attempt to gain unauthorised access to this website, its facilities and/or services or any accounts, computer systems and networks connected to any of The Dealer “Free-Ads” Paper LTD websites, its facilities and/or services through hacking, password miming or any other means.</p>
                    <p>You agree not to use this website to harvest or otherwise collect by any means any program material or information (including without limitation email addresses or details of other users) from this website or as otherwise authorised under these Terms of Use or to monitor, mirror or copy any content of this website without the prior written consent of The Dealer “Free-Ads” Paper LTD. Advertising of other websites through thedealer.ie will only be allowed with the express permission of The Dealer "Free-Ads" Paper LTD.</p>
                    <p>Such permission granted will be in writing. The pages contained in this website may contain technical inaccuracies and typographical errors. The information in this website may be updated from time to time but we do not accept any responsibility for keeping the information in these pages up-to-date nor any liability for any failure to do so. The Dealer “Free-Ads” Paper LTD makes no representation as to the accuracy, reliability or completeness of the contents of this website or the advertisements posted on this website. The use of this website is at your own risk.</p>
                    <p>The Dealer “Free-Ads” Paper LTD reserves the right, at its sole discretion, to pursue all of its legal remedies upon breach by you of these Terms of Use, including but not limited to deletion of your postings from this website, immediate termination of your registration and restricting your ability to access this website, or if The Dealer “Free-Ads” Paper LTD is unable to verify or authenticate any information you submit to this website.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-5" aria-expanded="true" aria-controls="accordion-5">
                    Registration
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-5" data-parent="#accordion">
                    <p>Registration is not required if you simply want to browse through the advertisements on this website. However, registration is required to place advertisements, save searches etc. In order to complete the registration you must choose a User ID and password. You will be asked to provide information such as your name, address, telephone number and email address. All information supplied must be complete and accurate. The Dealer “Free-Ads” Paper LTD may restrict the User ID or password you can choose.</p>
                    <p>You are responsible for all actions taken under your account and you agree only to use or utilise this website using your own User ID and password. You must make reasonable efforts to keep your password safe and not disclose it to any other person or permit either directly or indirectly any other person to utilise your User ID or password. You agree to immediately notify The Dealer “Free-Ads” Paper LTD of any unauthorised use of your User ID and/or password. The Dealer “Free-Ads” Paper LTD can terminate your registration without prior notice at the discretion of The Dealer “Free-Ads” Paper LTD.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-6" aria-expanded="true" aria-controls="accordion-6">
                    Placing Ads
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-6" data-parent="#accordion">
                    <p>All advertisements offered for publication in The Dealer “Free-Ads” Paper must be legal, honest and truthful and must comply with the Irish Code of Advertising Practice and The Dealer “Free-Ads” Paper LTD’s advertising and editorial policies and are accepted for publication, at The Dealer “Free-Ads” Paper LTD's absolute discretion. All advertisements may be edited and classified at The Dealer “Free-Ads” Paper LTD's sole and absolute discretion. All advertisements contained on this website are accepted for publication and published in good faith, but it is emphasised that The Dealer “Free-Ads” Paper LTD does not, in any circumstances, accept responsibility for the accuracy or otherwise of any advertisement or message published either online or off-line (nor is any kind of warranty expressed or implied by such publication).</p>
                    <p>All private, trade &amp; business advertisements in The Dealer “Free-Ads” Paper or on this website must be prepaid where payment is required. Multiple items for sale in a single online classified advert, is not permitted. The Publisher cannot refund Advertisers who wish to cancel their advertisement once their advertisement has run for one issue. All users are advised to check details of each advertisement carefully before entering into any agreements of any kind. If in doubt please seek legal advice.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-7" aria-expanded="true" aria-controls="accordion-7">
                    Commercial Advertisers
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-7" data-parent="#accordion">
                    <p>The Consumer Information Advertisements Disclosure of Business Interest Order 1984 requires all advertisements from persons selling goods in the course of a business to make that fact clear. This requirement applies where the advertiser is acting on its own behalf or for some other person in the course of his business goods are to be sold. Employers (not agencies) advertising employment vacancies in the job section may do so free of change. Multi-level marketing agents do not qualify for free ads. The Dealer “Free-Ads” Paper LTD may in its absolute discretion request whatever information or documentation it deems appropriate from an advertiser to verify that an advertiser is a bonafide employer and not an agency or multi-level marketing agency. We reserve the right to suspend the services offered to you by The Dealer “Free-Ads” Paper LTD if we have reasonable cause to suspect that you are in breach of any of the terms of use. Multiple items for sale in a single online classified advert, is not permitted.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-8" aria-expanded="true" aria-controls="accordion-8">
                    Period of Advertisement
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-8" data-parent="#accordion">
                    <p>Trade advertisements will remain active on the website for the time specified at the time of purchase.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-9" aria-expanded="true" aria-controls="accordion-9">
                    Suspension/Termination
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-9" data-parent="#accordion">
                    <p>The Dealer “Free-Ads” Paper LTD may at any time, without notice to you, suspend or terminate your access to this website or any service forming part of this website, wholly or partially for any reason, including without limitation, where you are in breach of these Terms of Use, or if The Dealer “Free-Ads” Paper LTD cannot verify or authenticate any information submitted to the website, or if for any reason The Dealer “Free-Ads” Paper LTD suspends or discontinues the website or this service, or is unable to supply a service to you. The Dealer “Free-Ads” Paper LTD is not liable to you or any third party for any suspension or termination of access to this website.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-10" aria-expanded="true" aria-controls="accordion-10">
                    Copyright Notice &amp; Limited Licence
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-10" data-parent="#accordion">
                    <p>The information, content, graphics, text, sounds, images, buttons, trademarks, service marks, trade names and logos (the "Materials") contained in this website are protected by copyright, trade mark, database right, sui generis right and other intellectual property laws, under national laws and international treaties.</p>
                    <p>The Dealer “Free-Ads” Paper LTD and/or its licensors (as the case may be) retain all right, title, interest and intellectual property rights in and to the Materials. Any other use of the Materials on this website, including in any form of copying or reproduction (for any purposes other than those noted above) modification, distribution, uploading, re-publication, extraction, re-utilisation, incorporation or integration with other Materials or works or re-delivery using framing technology, without the prior written permission of The Dealer “Free-Ads” Paper LTD is strictly prohibited and is a violation of the proprietary rights of The Dealer “Free-Ads” Paper LTD. Other than as expressly provided herein nothing in these Terms of Use should be construed as conferring by implication or otherwise any licence or right under any copyright, patent, trade mark, database right, sui generis right or other intellectual property or proprietary interest of The Dealer “Free-Ads” Paper LTD, its licensors or any third party.</p>
                    <p>You agree to grant The Dealer “Free-Ads” Paper LTD a non-exclusive, royalty free, world-wide, perpetual licence with the right to sub-licence, reproduce, distribute, transmit, create derivative works of, publicly display and publicly perform any Materials and other information (including without limitation ideas contained therein for new or improved goods and services) you submit to any public areas of this website (such as bulletin boards, forms in news groups or by email to The Dealer “Free-Ads” Paper LTD). You also grant The Dealer “Free-Ads” Paper LTD the right to use your name in connection with the submitted content.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-11" aria-expanded="true" aria-controls="accordion-11">
                    Indemnity
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-11" data-parent="#accordion">
                    <p>You agree to defend, indemnify and hold The Dealer “Free-Ads” Paper LTD and (as applicable) its officers, directors, employees, agents, subsidiaries, affiliates, suppliers and any of our third party information service providers or other representatives harmless against any and all claims demands, losses, expenses, damages and costs, including legal costs, however arising resulting from any violation or breach by you of these Terms of Use or any claims made by or liabilities to any third party resulting from any activities conducted under your account, your use or misuse of this website, including but not limited to posting content on this website, entering into transactions with other website users, contacting others as a result of their postings on this website, infringing any third party's intellectual property or other rights, or otherwise arising out of your breach of these Terms of Use.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-12" aria-expanded="true" aria-controls="accordion-12">
                    Export Control
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-12" data-parent="#accordion">
                    <p>You acknowledge that any goods or services sold to you under these terms of use may be subject to export control laws and regulations in jurisdictions including but not limited to the countries of the US and the European Economic Area. You confirm that, should this be the case, you will not export or re-export such goods or services in breach of such laws or regulations.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-13" aria-expanded="true" aria-controls="accordion-13">
                    Limited Dealings with Content
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-13" data-parent="#accordion">
                    <p>The Dealer “Free-Ads” Paper LTD acts as a passive conduit for the online distribution and publication of information and advertisements submitted by users, and has no obligation to screen advertisements or information in advance and is not responsible for screening or monitoring material posted by users. The Dealer “Free-Ads” Paper LTD does not exercise editorial control over the content of this website or other material created or accessible over or through this website. However, The Dealer “Free-Ads” Paper LTD retains the right to remove advertisements which are abusive, illegal or disruptive. The Dealer “Free-Ads” Paper LTD may take any action with respect to user submitted information that it deems necessary or appropriate in its sole discretion if it believes it may create liability for The Dealer “Free-Ads” Paper LTD. The Dealer “Free-Ads” Paper LTD does not accept any responsibility for failure to contact an advertiser or to notify the advertisers of any changes to or rejection of advertisements which The Dealer “Free-Ads” Paper LTD deem necessary to comply with The Dealer “Free-Ads” Paper LTD's production and editorial requirements, if such contact is not practical before the publication or due publication date.</p>
                    <p>The Dealer “Free-Ads” Paper LTD does not accept responsibility for the behaviour of users on the website or the information made available on this website by other users. Therefore, The Dealer “Free-Ads” Paper LTD does not guarantee or endorse the authenticity, quality, safety, or legality of any items offered or sold, the truth or accuracy of any adverts, or the ability of sellers to sell items.</p>
                    <p>The Dealer “Free-Ads” Paper LTD cannot guarantee that all transactions will be completed nor can we guarantee the ability or intent of users to fulfil their obligations in any transactions. If The Dealer “Free-Ads” Paper LTD is notified by a user of this website that certain communications do not conform to these Terms of Use, The Dealer “Free-Ads” Paper LTD may investigate the allegation and determine in good faith and at its sole discretion whether to remove or request the removal of the advertisement.</p>
                    <p>The Dealer “Free-Ads” Paper LTD has no liability or responsibility to users for the performance or non-performance of such activity. The Dealer “Free-Ads” Paper LTD reserves the right to expel users and to prevent their further access to this website for violations of these Terms of Use or the law. You agree to grant The Dealer “Free-Ads” Paper LTD a non-exclusive, royalty free, world-wide, perpetual licence with the right to sub-licence, reproduce, distribute, transmit, create derivative works of, publicly display and publicly perform any Materials and other information (including without limitation ideas contained therein for new or improved goods and services) you submit to any public areas of this website (such as bulletin boards, forms in news groups or by email toThe Dealer “Free-Ads” Paper LTD). You also grant The Dealer “Free-Ads” Paper LTD the right to use your name in connection with the submitted advertisements.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-14" aria-expanded="true" aria-controls="accordion-14">
                    Links to Third Party Websites
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-14" data-parent="#accordion">
                    <p>This website contains links to third party websites. Your use of third party websites is subject to the terms and conditions of use contained within each of those websites. Access to any other website through this website is at your own risk. The Dealer “Free-Ads” Paper LTD is not responsible or liable for the accuracy of any information, data, opinions, statements made on these websites or the security of any link or communication with those websites. The Dealer “Free-Ads” Paper LTD reserves the right to terminate a link to a third party website at any time. The fact that The Dealer “Free-Ads” Paper LTD provides a link to a third party website does not mean that The Dealer “Free-Ads” Paper LTD endorses, authorises or sponsors that website, nor does it mean that The Dealer “Free-Ads” Paper LTD is affiliated with the third party websites, owners or sponsors. The Dealer “Free-Ads” Paper LTD provides these links merely as a convenience for those who use this website.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-15" aria-expanded="true" aria-controls="accordion-15">
                    Disclaimers
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-15" data-parent="#accordion">
                    <p>All use by you of this website is at your own risk. You assume complete responsibility for, and for all risk of loss resulting from, your downloading and/or using of, or referring to or relying on facilities, services or materials provided on this website, or any other information obtained from your use of this website. You agree that The Dealer “Free-Ads” Paper LTD and providers of telecommunications and network services to The Dealer “Free-Ads” Paper LTD will not be liable for damages arising out of your use or your inability to use this website, and you hereby waive any and all claims with respect thereto, whether based on contract, tort or other grounds.</p>
                    <p>This website is available to all users "as is" and without any representations or warranties of any kind, either express or implied. The Dealer “Free-Ads” Paper LTD makes no representations, warranties or undertakings that this website, or the server that makes it available, will be free from defects, including, but not limited to viruses or other harmful elements. The Dealer “Free-Ads” Paper LTD accepts no liability for any infection by computer virus, bug, tampering, unauthorised access, intervention, alteration or use, fraud, theft, technical failure, error, omission, interruption, deletion, defect, delay, or any event or occurrence beyond the control of The Dealer “Free-Ads” Paper LTD, which corrupts or effects the administration, security, fairness and the integrity or proper conduct of any aspect of this website.</p>
                    <p>The Dealer “Free-Ads” Paper LTD makes no representations, warranties or undertakings about any of the facilities, services and/or materials on this website (including without limitation, their accuracy, their completeness or their merchantability, quality or fitness for a particular purpose.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-16" aria-expanded="true" aria-controls="accordion-16">
                    Limitation of Liability
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-16" data-parent="#accordion">
                    <p>To the fullest extent permitted by applicable law, neither The Dealer “Free-Ads” Paper LTD or any of its officers, directors, employees, affiliates or other representatives will be liable for loss or damages arising out of or in connection with your use of any facilities, services and/or the materials offered or transactions entered into through or from this website or services through this website, including, but not limited to, indirect or consequential loss or damages, loss of data, loss of income, profit or opportunity, loss of or damage to property and claims of third parties, even if The Dealer “Free-Ads” Paper LTD has been advised of the possibility of such loss or damages, or such loss or damages were reasonably foreseeable.</p>
                    <p>In no event shall The Dealer “Free-Ads” Paper LTD nor any of its officers, directors, employees, affiliates or other representatives be liable for any damages whatsoever resulting from the statements or conduct of any third party or the interruption, suspension or termination of the services, whether such interruption, suspension or termination was justified or not, negligent or intentional, inadvertent or advertent. Without limiting the foregoing, under no circumstances shall The Dealer “Free-Ads” Paper LTD or any of its officers, directors, employees, affiliates or other representatives be held liable for any delay or failure in performance resulting directly or indirectly from acts of nature, forces or causes beyond its reasonable control, including, without limitation, internet failure, computer equipment failures, telecommunication failures, other equipment failures, electrical power failures, strikes, lay-way disputes, riots, interactions, civil disturbances, shortages of labour or materials, fires, floats, storms, explosions, acts of God, war, governmental actions, orders of domestic or foreign courts or tribunals, non-performance of third party.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-17" aria-expanded="true" aria-controls="accordion-17">
                    Offline Conduct
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-17" data-parent="#accordion">
                    <p>Although The Dealer “Free-Ads” Paper LTD cannot monitor the conduct of its users offline, it is a violation of these Terms of Use to use any information obtained from this website in order to harass, abuse or harm another person, or in order to contact, advertise to, solicit or sell to any user or person without their prior expressive consent.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-18" aria-expanded="true" aria-controls="accordion-18">
                    Privacy Statement
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-18" data-parent="#accordion">
                    <p>The Dealer “Free-Ads” Paper LTD and its associated companies respect and protect the privacy of the individuals who access this website and use its facilities and/or services. For full details of the manner in which The Dealer “Free-Ads” Paper LTD uses cookies, the type of information we collect, how we use it and under what circumstances we disclose information, please read the Privacy Statement which is hereby incorporated into and forms part of these Terms of Use.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-19" aria-expanded="true" aria-controls="accordion-19">
                    Jurisdiction and Governing Law
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-19" data-parent="#accordion">
                    <p>This website is controlled and operated by The Dealer “Free-Ads” Paper LTD from Ireland. The Dealer “Free-Ads” Paper LTD does not make any representation that the facilities, services and/or Materials offered through this website are appropriate or suitable for use in countries other than Ireland, or that they comply with any legal or regulatory requirements in any such other countries. In accessing this website, you do so at your own risk and on your own initiative, and are responsible for compliance with local laws, to the extent any local laws are applicable. If it is prohibited to make this website, facilities, services and/or Materials offered through this website or any part of them available in your country, or to you (whether by reason of nationality, residence or otherwise, this website, the facilities, services and/or Materials offered through this website or any part of them are not directed at you).</p>
                    <p>These Terms of Use shall be governed by and construed in accordance with the laws of Ireland, and you hereby agree for the benefit of The Dealer “Free-Ads” Paper LTD and without prejudice to the right of The Dealer “Free-Ads” Paper LTD to take proceedings in relation to these Terms of Use before any court of competent jurisdiction, that courts of Ireland shall have jurisdiction to hear and determine any actions or proceedings that may arise out of or in connection with these Terms of Use, or for such purposes you irrevocably summit to the jurisdiction of such courts.</p>
                </div>
            </div>
            <div class="accordion-group mb-2">
                <h4 class="heading collapsed" data-toggle="collapse" data-target="#accordion-20" aria-expanded="true" aria-controls="accordion-20">
                    Miscellaneous
                    <i class="flaticon-down-arrow"></i>
                    <i class="flaticon-up-arrow"></i>
                </h4>
                <div class="collapse accordion-desc" id="accordion-20" data-parent="#accordion">
                    <p>Any waiver of any provision of these Terms of Use must be in writing signed by The Dealer “Free-Ads” Paper LTD to be valid. Any waiver of any provision hereunder shall not operate as a waiver of any other provision, or a continuing waiver of any provision in the future. If any court of competent jurisdiction finds any provision of these Terms of Use to be void or unenforceable for any reason, then such provision shall be ineffective to the extent of the court's finding effecting the validity and enforceability of any remaining provision. These Terms of Use represent the entire understanding and agreement between you and The Dealer “Free-Ads” Paper LTD relating to use of this website, its facilities and/or services and supersede any and all prior statements, understandings and agreements whether oral or written, and shall not be modified except in writing, signed by you and by The Dealer “Free-Ads” Paper LTD.</p>
                </div>
            </div>
        </div>
    </div>
</div>