<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-wrapper">
    <div class="contact-info">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 address-wrapper">
                    <div class="office-address">
                        <h2>R.O.I Office</h2>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="addr">
                                    <h3>Address</h3>
                                    <p>Webtown House</p>
                                    <p>Port Road</p>
                                    <p>Letterkenny</p>
                                    <p>Co. Donegal</p>
                                    <p>F92 YND8</p>

                                    <h3>Email</h3>
                                    <a href="mailto:info@thedealer.ie">info@thedealer.ie</a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contact-lines">
                                    <h3>Tel</h3>
                                    <p>074 912 6410</p>

                                    <h3>Fax</h3>
                                    <p>074 912 8764</p>

                                    <h3>Text</h3>
                                    <p>51444</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-ads d-block d-sm-none">
                        <p> <i class="fas fa-2x fa-mobile-alt"></i> Text <strong>TheDealer</strong> to <strong>51444</strong> followed by a space and your ad details. </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 address-wrapper">
                    <div class="office-address">
                        <h2>N.I Office</h2>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="addr">
                                    <h3>Address</h3>
                                    <p>North West Business Complex</p>
                                    <p>Skeoge Industrial Park</p>
                                    <p>Beraghmore Road</p>
                                    <p>Derry</p>
                                    <p>BT48 8SE</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contact-lines">
                                    <h3>Tel</h3>
                                    <p>028 71 260088</p>

                                    <h3>Fax</h3>
                                    <p>028 71 271068</p>

                                    <h3>Text</h3>
                                    <p>88990</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-ads d-block d-sm-none">
                        <p> <i class="fas fa-2x fa-mobile-alt"></i> Text <strong>TheDealer</strong> to <strong>88990</strong> followed by a space and your ad details. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6  d-none d-sm-block">
                <div class="text-ads">
                    <p> <i class="fas fa-2x fa-mobile-alt"></i> Text <strong>TheDealer</strong> to <strong>51444</strong> followed by a space and your ad details. </p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 d-none d-sm-block">
                <div class="text-ads">
                    <p> <i class="fas fa-2x fa-mobile-alt"></i> Text <strong>TheDealer</strong> to <strong>88990</strong> followed by a space and your ad details. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="get-in-touch">
        <div class="container">
            <div class="form contact-form">
                <h2 class="form-head">Get In Touch</h2>
                
                <?php $form = ActiveForm::begin(['id' => 'form-contact']); ?>
                    <div class="row no-gutters mb-4">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group required mb-2 mr-sm-2 mr-0">
                                <?= $form->field($model, 'name')->textInput(['autofocus' => true,'placeholder' => "Name"])->label(false) ?>
                            </div>
                            <div class="form-group mb-2 mr-sm-2 mr-0">
                                <?= $form->field($model, 'subject')->textInput(['autofocus' => true,'placeholder' => "Tel"])->label(false) ?>
                            </div>
                            <div class="form-group required mb-2 mr-sm-2 mr-0">
                                <?= $form->field($model, 'email')->textInput(['autofocus' => true,'placeholder' => "Email"])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group required">
                                <?= $form->field($model, 'body')->textarea(['rows' => 4,'placeholder' => "Message"])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <?=
                            $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                            ])
                            ?>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-lg btn-default', 'name' => 'contact-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2470168.214765811!2d-9.3884331326635!3d52.79940464455023!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4859bae45c4027fb%3A0xcf7c1234cedbf408!2sIreland!5e0!3m2!1sen!2sin!4v1530611757128" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>

