<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'THE DEALER :: thedealer.ie :: buy, sell, exchange online in Ireland, Northern Irelan';
?>
<div class="site-index">
    
    <div class="form-wrapper">
        <div class="form">
            <?php $form = ActiveForm::begin(['id' => 'search-form']); ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group required">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-lg fa-map-marker-alt"></i>
                            </span>
                            <input type="text" class="form-control" name="city" placeholder="City">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group required">
                        <input type="text" class="form-control" name="location" placeholder="Location">
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-block btn-lg btn-default', 'name' => 'search-button']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    
</div>
