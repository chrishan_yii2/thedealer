<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-wrapper mt-5 mb-5">
    <div class="container"> 
        <h2 class="page-head border-bottom pb-3 mb-5">About Us</h2>
        <div class="row">
            <div class="col-md-4">
                <h3 class="sec-heading mb-4">Our History</h3>
                <div class="desc">
                    <p>The Dealer "Free-Ads" Paper was established in Co. Donegal in 1996 as a classified "Free-Ads" paper, addressing a gap in the market for buying and selling goods within the local area, as well as the need for a small indigenous business to advertise at affordable rates.</p>
                    <p>Our paper allows the individual to publish what they like as often as they like. For businesses, we offer low cost advertising that works!</p>
                    <p>We pride ourselves on providing a valuable service to the community through free advertising to charities, clubs and employers that have job vacancies.</p>
                </div>
                <div class="thumb d-none d-md-block">
                    <img class="img-fluid" src="/images/history.png" alt="">
                </div>
            </div>
            <div class="col-md-8">
                <div class="our-leaders">
                    <h3 class="sec-heading mb-4">Leadership</h3>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-12 mt-0 leaders">
                            <div class="media">
                                <img class="mr-3" src="/images/niall.jpg" alt="Niall Harding">
                                <div class="media-body">
                                    <h5 class="mt-0">Niall Harding</h5>
                                    <p>Co-Founder<br>Managing Director</p>
                                    <a href="mailto:niall@thedealer.ie">niall@thedealer.ie</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 mt-0 leaders">
                            <div class="media">
                                <img class="mr-3" src="/images/eileen.jpg" alt="Eileen Laughlin">
                                <div class="media-body">
                                    <h5 class="mt-0">Eileen Laughlin</h5>
                                    <p>Co-Founder</p>
                                    <a href="mailto:info@thedealer.ie">info@thedealer.ie</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-12 leaders">
                            <div class="media">
                                <img class="mr-3" src="/images/catherine.jpg" alt="Catherine Ewing">
                                <div class="media-body">
                                    <h5 class="mt-0">Catherine Ewing</h5>
                                    <p>General Manager</p>
                                    <a href="mailto:catherine@thedealer.ie">catherine@thedealer.ie</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 leaders">
                            <div class="media">
                                <img class="mr-3" src="/images/cahir.jpg" alt="Cahir Mc Bride">
                                <div class="media-body">
                                    <h5 class="mt-0">Cahir Mc Bride</h5>
                                    <p>Production & I.T.<br> Manager</p>
                                    <a href="mailto:cahir@thedealer.ie">cahir@thedealer.ie</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-12 leaders">
                            <div class="media">
                                <img class="mr-3" src="/images/john.jpg" alt="John Jordan">
                                <div class="media-body">
                                    <h5 class="mt-0">John Jordan</h5>
                                    <p>NI Office</p>
                                    <a href="mailto:info@thedealer.ie">info@thedealer.ie</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 leaders">
                            <div class="media">
                                <img class="mr-3" src="/images/leslie.jpg" alt="Leslie Corr">
                                <div class="media-body">
                                    <h5 class="mt-0">Leslie Corr</h5>
                                    <p>NI Office</p>
                                    <a href="mailto:info@thedealer.ie">info@thedealer.ie</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-future mt-5">
                    <h3 class="sec-heading mb-4">Our Future</h3>
                    <div class="desc">
                        <p>Our website allows customers to place and browse ads completely free of charge. Business customers can join our directory for a small fee or if advertising in our paper, this service is complimentary.</p>
                        <p>In 2014, we expanded our web services and redesigned The Dealer website, making it easier than ever to place or browse online ads in Ireland and Northern Ireland.</p>
                        <p>Our digital edition of The Dealer “Free-Ads” Paper means that, like our website, you can now read your copy of The Dealer “Free-Ads” Paper on your desktop, tablet or smart phone.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="team-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="team-banner">
                    <img src="images/team.png" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-6">
                        <h4 class="list-head">FRONT DESK</h4>
                        <ul>
                            <li>Nicola Neely</li>
                            <li>Claire Gillespie</li>
                            <li>Teresa Brogan</li>
                            <li>Finola McGovern</li>
                            <li>Darren Duffy</li>
                            <li>Linda McHugh</li>
                        </ul>
                        <h4 class="list-head">PRODUCTION</h4>
                        <ul>
                            <li>Deirdre Rooney</li>
                            <li>Hollie McCosker</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 col-6">
                        <h4 class="list-head">SALES REPS</h4>
                        <ul>
                            <li>Niall McGlinchey</li>
                            <li>Michael Kelly</li>
                            <li>Michael Moran</li>
                        </ul>
                        <h4 class="list-head">MERCHANDISING</h4>
                        <ul>
                            <li>Charles Greene</li>
                        </ul>
                        <h4 class="list-head">WEB</h4>
                        <ul>
                            <li>Valerie Smyth</li>
                            <li>Kevin McFadden</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 col">
                        <h4 class="list-head">ACCOUNTS</h4>
                        <ul>
                            <li>Doreen Byrne</li>
                            <li>Caroline McLaughlin</li>
                        </ul>                                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="gallery-wrapper pt-5 pb-5">
    <div class="container">
        <h2 class="mb-5">Gallery</h2>
        <div class="gallery-grid">
            <div class="row">
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-1.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-2.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-3.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="images/gallery-4.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-5.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-6.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-7.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-8.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-9.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-10.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-11.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-2 col-sm-3 col-6">
                    <img src="/images/gallery-12.jpg" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</div>