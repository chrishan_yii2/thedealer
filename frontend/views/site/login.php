<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="form-wrapper">
        <div class="form login-form">
            <h2 class="form-head"><?= Html::encode('Sign In') ?></h2>
            <?php $form = ActiveForm::begin(['id' => 'form-signin']); ?>
            <?= $form->field($model, 'username', [
                                        'inputTemplate' => '
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fal fa-envelope fa-fw fa-2x"></i>
                                                    </span>
                                                </div>
                                                {input}
                                            </div>',
                                            'inputOptions' => [
                                                'placeholder' => 'User Name*',
                                            ],
                                        ])->label(false) ?>

            <?= $form->field($model, 'password', [
                                        'inputTemplate' => '
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="far fa-unlock-alt fa-fw fa-2x"></i>
                                                    </span>
                                                </div>
                                                {input}
                                            </div>',
                                            'inputOptions' => [
                                                'placeholder' => 'Password*',
                                            ],
                                        ])->passwordInput()->label(false) ?>
            <?= $form->field($model, 'rememberMe', [
                                        'inputTemplate' => '
                                            <i class="far fa-square fa-lg unchecked"></i>
                                            <i class="far fa-check-square fa-lg checked"></i>
                                                {input}',
                                        ])->checkbox() ?>
            <a class="forgot-pass" href="<?= Url::to('password-reset') ?>">Forgot your password?</a>

            <div class="empty-space"></div>
            <div class="empty-space"></div>
            <div class="form-group text-center">
                <?= Html::submitButton('SIGN IN', ['class' => 'btn btn-lg btn-default btn-block', 'name' => 'signin-button']) ?>
            </div>
            <div class="social-login text-center">
                <p class="mb-3"> Or <br> <strong>Log in using your social network</strong></p>
                <a class="btn btn-lg btn-fb btn-block mb-3" href="#"><i class="fab fa-facebook-f"></i> Sign In with Facebook</a>
                <a class="btn btn-lg btn-google btn-block" href="#"><i class="fab fa-google"></i> Sign In with Google</a>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>
