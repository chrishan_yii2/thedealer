<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ClassifiedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Classified Listings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-listings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Classified Listings'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
//            'id',
            'parent_id',
            'classified_cat_id',
            'sub_category',
            'user_id',
            //'image_count',
            //'ad_type',
            //'description:ntext',
            //'search_options:ntext',
            //'currency',
            //'price',
            //'search_price',
            //'price_suffix',
            //'duration',
            //'town_id',
            //'county_id',
            //'item_status',
            //'telephone',
            //'email:email',
            //'mobile',
            //'other',
            //'status',
            //'order',
            //'bump',
            //'bump_count',
            //'featured',
            //'date_sold',
            //'views',
            //'start_date',
            //'end_date',
            //'make_id',
            //'auto_make',
            //'model_id',
            //'auto_model',
            //'year',
            //'fuel',
            //'body_type',
            //'colour',
            //'engine_size',
            //'version',
            //'date_added',
            //'business_plus_url:url',
            //'edition_one',
            //'edition_two',
            //'edition_three',
            //'edition_four',
            //'edition_date',
            //'buy_now_url:url',
            //'transmission',
            //'co2_emissions',
            //'nct_date',
            //'tax_date',
            //'mileage',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
