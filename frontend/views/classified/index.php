<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ClassifiedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Classified Listings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">

    <div class="recent-ads">
        <div class="container">
            <div class="row">
                <div class="col-md-9 ads-listing">
                    <h3 class="section-heading">Recent Ads</h3>
                    <a href="<?= Url::to('/classified/search?'); ?>" class="all-ads float-right">See All</a>
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView'     => '_item',
                        'layout'       => '<div class="row no-gutters">{items}</div>',
                        'itemOptions'  => [
                                'class' => 'col-md-4 col-sm-4 col-6 ads-grid',
                        ],
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?php if(!empty($adsence)) : ?>
                        <div class="row mt-5">
                            <div class="col-12 col-sm-6 col-md-12 mb-4 text-center">
                                <?= $adsence[0]->adsense_code_1 ?>
                            </div>
                            <div class="col-12 col-sm-6 col-md-12 mb-4 text-center">
                                <?= $adsence[0]->adsense_code_2 ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    
</div>
