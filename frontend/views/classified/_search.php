<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ClassifiedSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classified-listings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parent_id') ?>

    <?= $form->field($model, 'classified_cat_id') ?>

    <?= $form->field($model, 'sub_category') ?>

    <?= $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'image_count') ?>

    <?php // echo $form->field($model, 'ad_type') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'search_options') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'search_price') ?>

    <?php // echo $form->field($model, 'price_suffix') ?>

    <?php // echo $form->field($model, 'duration') ?>

    <?php // echo $form->field($model, 'town_id') ?>

    <?php // echo $form->field($model, 'county_id') ?>

    <?php // echo $form->field($model, 'item_status') ?>

    <?php // echo $form->field($model, 'telephone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'mobile') ?>

    <?php // echo $form->field($model, 'other') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'order') ?>

    <?php // echo $form->field($model, 'bump') ?>

    <?php // echo $form->field($model, 'bump_count') ?>

    <?php // echo $form->field($model, 'featured') ?>

    <?php // echo $form->field($model, 'date_sold') ?>

    <?php // echo $form->field($model, 'views') ?>

    <?php // echo $form->field($model, 'start_date') ?>

    <?php // echo $form->field($model, 'end_date') ?>

    <?php // echo $form->field($model, 'make_id') ?>

    <?php // echo $form->field($model, 'auto_make') ?>

    <?php // echo $form->field($model, 'model_id') ?>

    <?php // echo $form->field($model, 'auto_model') ?>

    <?php // echo $form->field($model, 'year') ?>

    <?php // echo $form->field($model, 'fuel') ?>

    <?php // echo $form->field($model, 'body_type') ?>

    <?php // echo $form->field($model, 'colour') ?>

    <?php // echo $form->field($model, 'engine_size') ?>

    <?php // echo $form->field($model, 'version') ?>

    <?php // echo $form->field($model, 'date_added') ?>

    <?php // echo $form->field($model, 'business_plus_url') ?>

    <?php // echo $form->field($model, 'edition_one') ?>

    <?php // echo $form->field($model, 'edition_two') ?>

    <?php // echo $form->field($model, 'edition_three') ?>

    <?php // echo $form->field($model, 'edition_four') ?>

    <?php // echo $form->field($model, 'edition_date') ?>

    <?php // echo $form->field($model, 'buy_now_url') ?>

    <?php // echo $form->field($model, 'transmission') ?>

    <?php // echo $form->field($model, 'co2_emissions') ?>

    <?php // echo $form->field($model, 'nct_date') ?>

    <?php // echo $form->field($model, 'tax_date') ?>

    <?php // echo $form->field($model, 'mileage') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
