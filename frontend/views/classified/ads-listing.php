<?php

use yii\widgets\ListView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = "Add Listing";

?>
<?php 
$params = Yii::$app->request->queryParams;

$slug = (isset($params['ClassifiedSearch']['slug']) && !empty($params['ClassifiedSearch']['slug'])) ? $params['ClassifiedSearch']['slug'] : "";
$slug = (isset($params['ClassifiedSearch']['title']) && !empty($params['ClassifiedSearch']['title'])) ? $params['ClassifiedSearch']['title'] : $slug;
$type = (isset($params['ClassifiedSearch']['ad_type']) && !empty($params['ClassifiedSearch']['ad_type'])) ? $params['ClassifiedSearch']['ad_type'] : "";
$county = (isset($params['ClassifiedSearch']['county']) && !empty($params['ClassifiedSearch']['county'])) ? $params['ClassifiedSearch']['county'] : "";


?>
<div class="content-wrapper listing-wrapper">
    <div class="container">
        <div class="g-adverts text-center mb-5">
            <img src="images/myVehicle_banner.gif" class="img-fluid" alt="">
        </div>
        <div class="row">
            <div class="col-md-4 d-none d-md-block">
                <div class="sidebar-filter">
                    <h4 class="filter-head" data-toggle="collapse" data-target="#classified_parent_slug" aria-expanded="true" aria-controls="classified_parent_slug">
                        Category
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse show filter-items" id="classified_parent_slug">
                        <li><a href="<?= Url::to('/classified/search?ClassifiedSearch[slug]='.'&ClassifiedSearch[ad_type]='.$type.'&ClassifiedSearch[county]='.$county) ?>">All Categories</a></li>
                        <?php
                        if(!empty($categories) && count($categories) > 0):
                            foreach ($categories as $c => $category):
                            
                        ?>
                        <li><a href="<?= Url::to('/classified/search?ClassifiedSearch[slug]='.Html::encode($category->slug)).'&ClassifiedSearch[ad_type]='.$type.'&ClassifiedSearch[county]='.$county ?>"><?= $category->category ?></a></li>
                        <?php 
                            endforeach;
                        endif; 
                        ?>
                    </ul>
                    <h4 class="filter-head collapsed" data-toggle="collapse" data-target="#classified_cat_slug" aria-expanded="false" aria-controls="classified_cat_slug">
                        Sub Cetegory
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse filter-items" id="classified_cat_slug">
                        <li><a href="<?= Url::to('/classified/search?ClassifiedSearch[slug]='.'&ClassifiedSearch[ad_type]='.$type.'&ClassifiedSearch[county]='.$county) ?>">All</a></li>
                        <?php
                        if(!empty($subCategories) && count($subCategories) > 0):
                            foreach ($subCategories as $sc => $subCategory):
                            $carUrl = ($subCategory->slug == "cars") ? 'search-car?ClassifiedSearch[make]=': 'search?ClassifiedSearch[slug]='.Html::encode($subCategory->slug);
                        ?>
                            <li><a href="<?= Url::to('/classified/'.$carUrl.'&ClassifiedSearch[ad_type]='.$type.'&ClassifiedSearch[county]='.$county) ?>"><?= $subCategory->category ?></a></li>
                        <?php 
                            endforeach;
                        endif; 
                        ?>
                    </ul>
                    <h4 class="filter-head collapsed" data-toggle="collapse" data-target="#adtype" aria-expanded="false" aria-controls="adtype">
                        Account
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse filter-items" id="adtype">
                        <li><a href="<?= Url::to('/classified/search?ClassifiedSearch[ad_type]='."".'&ClassifiedSearch[slug]='.$slug.'&ClassifiedSearch[county]='.$county) ?>">Private &amp; Business</a></li>
                        <li><a href="<?= Url::to('/classified/search?ClassifiedSearch[ad_type]='.\common\models\Users::ROLE_PRIVATE.'&ClassifiedSearch[slug]='.$slug.'&ClassifiedSearch[county]='.$county) ?>">private </a></li>
                        <li><a href="<?= Url::to('/classified/search?ClassifiedSearch[ad_type]='.\common\models\Users::ROLE_BUSINESS.'&ClassifiedSearch[slug]='.$slug.'&ClassifiedSearch[county]='.$county) ?>">business </a></li>
                    </ul>
                    
                    <h4 class="filter-head collapsed" data-toggle="collapse" data-target="#county" aria-expanded="false" aria-controls="county">
                        County
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse filter-items" id="county">
                        <li><a href="<?= Url::to('/classified/search?ClassifiedSearch[county]='."".'&ClassifiedSearch[ad_type]='.$type.'&ClassifiedSearch[slug]='.$slug) ?>">All Counties</a></li>
                        <?php 
                        
                            foreach ($countries as $lc => $country):
                        ?>
                            <li><a href="<?= Url::to('/classified/search?ClassifiedSearch[county]='.Html::encode($country->slug).'&ClassifiedSearch[ad_type]='.$type.'&ClassifiedSearch[slug]='.$slug) ?>"><?= Html::encode($country->county) ?></a></li>
                        
                        <?php 
                            endforeach;
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="filtered-items border-bottom">
                    <div class="row">
                        <div class="col-md-4">
                            <h3 class="filtered-item"><?= ucfirst((!empty($slug)) ? str_replace('-', ' ', $slug) : "") ?></h3>
                        </div>
                        <div class="col-md-6">
                            <div class="g-adverts text-center mb-3">
                                <img src="images/posts-ad.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h3 class="adsCount float-right text-nowrap"><?php echo $dataProvider->getTotalCount(); ?> Ads</h3>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="sorting">
                    <div class="mobile-filter d-md-none d-block">
                        <h4 class="filtered-head">
                            Filters
                            <i class="fas fa-angle-right fa-lg float-right"></i>
                        </h4>
                    </div>
                    <select name="show" class="options" id="show">
                        <option value="15" <?= (isset($params['per-page']) && $params['per-page'] == 15) ? 'selected' :""; ?>>Show 15</option>
                        <option value="30" <?= (isset($params['per-page']) && $params['per-page'] == 30) ? 'selected' :""; ?>>Show 30</option>
                        <option value="50" <?= (isset($params['per-page']) && $params['per-page'] == 50) ? 'selected' :""; ?>>Show 50</option>
                    </select>
                    <span style="margin: 15px;"></span>
                    <select name="order" class="options" id="order">
                        <option value="desc_score">Relevance</option>
                        <option value="-id" <?= (isset($params['sort']) && $params['sort'] == '-id') ? 'selected' :""; ?>>Most Recent</option>
                        <option value="search_price" <?= (isset($params['sort']) && $params['sort'] == 'price') ? 'selected' :""; ?>>Lowest Price</option>
                        <option value="-search_price" <?= (isset($params['sort']) && $params['sort'] == '-price') ? 'selected' :""; ?>>Highest Price</option>
                    </select>
                </div>
                <div class="empty-space"></div>
                <div class="empty-space"></div>

                <div class="ads-listing">
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView'     => '_item',
                        'layout'       => '<div class="row no-gutters">{items}</div>',
                        'itemOptions'  => [
                                'class' => 'col-md-4 col-sm-4 col-6 ads-grid',
                        ],
                    ]); ?>
                </div>
                <div class="empty-space"></div>
                <div class="empty-space"></div>
                <nav aria-label="page navigation">
                <?php
                
                echo LinkPager::widget([
                    'pagination' => $dataProvider->pagination,
                    'options' => [
                        'class' => 'pagination justify-content-end'
                    ],
                    'linkContainerOptions' => [
                        'class' => 'page-item'
                    ],
                    'linkOptions' => [
                        'class' => 'page-link'
                    ]
                ]);
                
                ?>
                </nav>
                <div class="empty-space"></div>
                <div class="empty-space"></div>
                <div class="empty-space"></div>
                <div class="g-adverts text-center">
                    <img src="images/footer-advert.png" class="img-fluid" alt="">
                </div>

            </div>
        </div>
    </div>
</div>