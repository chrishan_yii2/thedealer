<?php

use yii\widgets\ListView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = "Cars - thedealer";

$params = Yii::$app->request->queryParams;

$make = (isset($params['ClassifiedSearch']['make']) && !empty($params['ClassifiedSearch']['make'])) ? $params['ClassifiedSearch']['make'] : "";
$fuel = (isset($params['ClassifiedSearch']['fuel']) && !empty($params['ClassifiedSearch']['fuel'])) ? $params['ClassifiedSearch']['fuel'] : "";
$body_type = (isset($params['ClassifiedSearch']['body_type']) && !empty($params['ClassifiedSearch']['body_type'])) ? $params['ClassifiedSearch']['body_type'] : "";
$engine_size = (isset($params['ClassifiedSearch']['engine_size']) && !empty($params['ClassifiedSearch']['engine_size'])) ? $params['ClassifiedSearch']['engine_size'] : "";
$county = (isset($params['ClassifiedSearch']['county']) && !empty($params['ClassifiedSearch']['county'])) ? $params['ClassifiedSearch']['county'] : "";
$color = (isset($params['ClassifiedSearch']['colour']) && !empty($params['ClassifiedSearch']['colour'])) ? $params['ClassifiedSearch']['colour'] : "";
$year = (isset($params['ClassifiedSearch']['year']) && !empty($params['ClassifiedSearch']['year'])) ? $params['ClassifiedSearch']['year'] : "";
?>
<div class="content-wrapper listing-wrapper cars-and-vehicles">
    <div class="container">
        <div class="g-adverts text-center mb-5">
            <img src="images/myVehicle_banner.gif" class="img-fluid" alt="">
        </div>
        <div class="row">
            <div class="col-md-4 d-none d-md-block">
                <div class="sidebar-filter">
                    <h4 class="filtered-head">
                        <span>Your Selection</span>
                        <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=&ClassifiedSearch[fuel]=&ClassifiedSearch[body_type]=&ClassifiedSearch[engine_size]=&ClassifiedSearch[county]=&ClassifiedSearch[colour]=&ClassifiedSearch[year]=') ?>" class="float-right btn btn-default btn-sm">Reset</a>
                    </h4>

                    <ul class="filtered-items">
                        <?php if(!empty($make)):?>
                            <li>
                                <strong>Makes: </strong> 
                                <?= ucfirst($make) ?> 
                                <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]='.'&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county.'&ClassifiedSearch[colour]=' . $color.'&ClassifiedSearch[year]=' . $year) ?>" class="float-right"><i class="fas fa-window-close fa-lg"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if(!empty($color)):?>
                            <li>
                                <strong>Colors: </strong> 
                                <?= ucfirst($color) ?> 
                                <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]='.$make.'&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county.'&ClassifiedSearch[colour]='.'&ClassifiedSearch[year]=' . $year) ?>" class="float-right"><i class="fas fa-window-close fa-lg"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if(!empty($fuel)):?>
                            <li>
                                <strong>Fuels: </strong> 
                                <?= ucfirst($fuel) ?> 
                                <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]='.$make.'&ClassifiedSearch[fuel]='.'&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county.'&ClassifiedSearch[colour]='.$color.'&ClassifiedSearch[year]=' . $year) ?>" class="float-right"><i class="fas fa-window-close fa-lg"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if(!empty($body_type)):?>
                            <li>
                                <strong>Body Type: </strong> 
                                <?= ucfirst($body_type) ?> 
                                <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]='.$make.'&ClassifiedSearch[fuel]='.$fuel.'&ClassifiedSearch[body_type]=' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county.'&ClassifiedSearch[colour]='.$color.'&ClassifiedSearch[year]=' . $year) ?>" class="float-right"><i class="fas fa-window-close fa-lg"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if(!empty($engine_size)):?>
                            <li>
                                <strong>Engine Size: </strong> 
                                <?= ucfirst($engine_size) ?> 
                                <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]='.$make.'&ClassifiedSearch[fuel]='.$fuel.'&ClassifiedSearch[body_type]='. $body_type . '&ClassifiedSearch[engine_size]='.'&ClassifiedSearch[county]=' . $county.'&ClassifiedSearch[colour]='.$color.'&ClassifiedSearch[year]=' . $year) ?>" class="float-right"><i class="fas fa-window-close fa-lg"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if(!empty($county)):?>
                            <li>
                                <strong>County: </strong> 
                                <?= ucfirst($county) ?> 
                                <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]='.$make.'&ClassifiedSearch[fuel]='.$fuel.'&ClassifiedSearch[body_type]='. $body_type . '&ClassifiedSearch[engine_size]='.$engine_size.'&ClassifiedSearch[county]='.'&ClassifiedSearch[colour]='.$color.'&ClassifiedSearch[year]=' . $year) ?>" class="float-right"><i class="fas fa-window-close fa-lg"></i></a>
                            </li>
                        <?php endif; ?>
                    </ul>

                    <h4 class="filter-head" data-toggle="collapse" data-target="#makes" aria-expanded="true" aria-controls="makes">
                        Makes
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse show filter-items" id="makes">
                        <li><a id="all" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county.'&ClassifiedSearch[colour]=' . $color.'&ClassifiedSearch[year]=' . $year) ?>" class="facets active">All</a></li>
                        <?php
                        foreach ($carMakes as $key => $carMake):
                            ?>
                            <li><a id="all" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . Html::encode($carMake->slug) . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>" class="facets active"><?= $carMake->make ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <h4 class="filter-head collapsed" data-toggle="collapse" data-target="#colors" aria-expanded="false" aria-controls="fuels">
                        Colors
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse filter-items" id="colors">
                        <li>
                            <a id="beige" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=beige') ?>" class="facets active">
                                <?= ucfirst('beige') ?>
                            </a>
                        </li>
                        <li>
                            <a id="black" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=diesel' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=black') ?>" class="facets ">
                                <?= ucfirst('black') ?>
                            </a>
                        </li>
                        <li>
                            <a id="blue" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=petrol' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=blue') ?>" class="facets ">
                                <?= ucfirst('blue') ?>
                            </a>
                        </li>
                        <li>
                            <a id="bronze" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=bronze') ?>" class="facets active">
                                <?= ucfirst('bronze') ?>
                            </a>
                        </li>
                        <li>
                            <a id="brown" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=diesel' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=brown') ?>" class="facets ">
                                <?= ucfirst('brown') ?>
                            </a>
                        </li>
                        <li>
                            <a id="burgundy" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=petrol' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=burgundy') ?>" class="facets ">
                                <?= ucfirst('burgundy') ?>
                            </a>
                        </li>
                        <li>
                            <a id="gold" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=gold') ?>" class="facets active">
                                <?= ucfirst('gold') ?>
                            </a>
                        </li>
                        <li>
                            <a id="green" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=diesel' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=green') ?>" class="facets ">
                                <?= ucfirst('green') ?>
                            </a>
                        </li>
                        <li>
                            <a id="grey" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=petrol' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=grey') ?>" class="facets ">
                                <?= ucfirst('grey') ?>
                            </a>
                        </li>
                        <li>
                            <a id="indigo" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=indigo') ?>" class="facets active">
                                <?= ucfirst('indigo') ?>
                            </a>
                        </li>
                        <li>
                            <a id="magenta" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=diesel' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=magenta') ?>" class="facets ">
                                <?= ucfirst('magenta') ?>
                            </a>
                        </li>
                        <li>
                            <a id="maroon" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=petrol' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=maroon') ?>" class="facets ">
                                <?= ucfirst('maroon') ?>
                            </a>
                        </li>
                        <li>
                            <a id="navy" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=navy') ?>" class="facets active">
                                <?= ucfirst('navy') ?>
                            </a>
                        </li>
                        <li>
                            <a id="orange" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=diesel' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=orange') ?>" class="facets ">
                                <?= ucfirst('orange') ?>
                            </a>
                        </li>
                        <li>
                            <a id="pink" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=petrol' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=pink') ?>" class="facets ">
                                <?= ucfirst('pink') ?>
                            </a>
                        </li>
                        <li>
                            <a id="purple" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=purple') ?>" class="facets active">
                                <?= ucfirst('purple') ?>
                            </a>
                        </li>
                        <li>
                            <a id="red" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=diesel' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=red') ?>" class="facets ">
                                <?= ucfirst('red') ?>
                            </a>
                        </li>
                        <li>
                            <a id="silver" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=petrol' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=silver') ?>" class="facets ">
                                <?= ucfirst('silver') ?>
                            </a>
                        </li>
                        <li>
                            <a id="turquoise" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=turquoise') ?>" class="facets active">
                                <?= ucfirst('turquoise') ?>
                            </a>
                        </li>
                        <li>
                            <a id="white" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=diesel' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=white') ?>" class="facets ">
                                <?= ucfirst('white') ?>
                            </a>
                        </li>
                        <li>
                            <a id="yellow" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=petrol' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county . '&ClassifiedSearch[colour]=yellow') ?>" class="facets ">
                                <?= ucfirst('yellow') ?>
                            </a>
                        </li>


                    </ul>
                    <h4 class="filter-head collapsed" data-toggle="collapse" data-target="#fuels" aria-expanded="false" aria-controls="fuels">
                        Fuels
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse filter-items" id="fuels">
                        <li><a id="all" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>" class="facets active">Fuel Type</a></li>
                        <li><a id="diesel" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=diesel' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>" class="facets ">Diesel</a></li>
                        <li><a id="petrol" href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=petrol' . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>" class="facets ">Petrol</a></li>
                    </ul>
                    <h4 class="filter-head collapsed" data-toggle="collapse" data-target="#body_type" aria-expanded="false" aria-controls="body_type">
                        Body Type
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse filter-items" id="body_type">
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">Body Type (all)</a></li>
                        <li>
                            <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=hatchback' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">hatchback</a>
                        </li>
                        <li>
                            <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=saloon' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">saloon</a>
                        </li>
                        <li>
                            <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=estate' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">estate</a>
                        </li>
                        <li>
                            <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=convertible' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">convertible</a>
                        </li>
                        <li>
                            <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=MPV' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">MPV</a>
                        </li>
                        <li>
                            <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=coupe' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">coupe</a>
                        </li>
                        <li>
                            <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=cabriolet' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">cabriolet</a>
                        </li>
                        <li>
                            <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=SUV' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">SUV</a>
                        </li>
                        <li>
                            <a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=4x4' . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $county) ?>">4x4</a>
                        </li>

                    </ul>
                    <h4 class="filter-head collapsed" data-toggle="collapse" data-target="#engine_size" aria-expanded="false" aria-controls="engine_size">
                        Engine Size
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse filter-items" id="engine_size">
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . '&ClassifiedSearch[county]=' . $county) ?>">Engine Size (All)</a></li>
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=-1L' . '&ClassifiedSearch[county]=' . $county) ?>">-1L</a></li>
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=1L' . '&ClassifiedSearch[county]=' . $county) ?>">1L</a></li>
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=1.4L' . '&ClassifiedSearch[county]=' . $county) ?>">1.4L</a></li>
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=-1.7L' . '&ClassifiedSearch[county]=' . $county) ?>">1.7L</a></li>
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=2L' . '&ClassifiedSearch[county]=' . $county) ?>">2L</a></li>
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=2.6L' . '&ClassifiedSearch[county]=' . $county) ?>">2.6L</a></li>
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=3L' . '&ClassifiedSearch[county]=' . $county) ?>">3L</a></li>
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=4L' . '&ClassifiedSearch[county]=' . $county) ?>">4L</a></li>
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=5L' . '&ClassifiedSearch[county]=' . $county) ?>">5L</a></li>
                    </ul>
                    <h4 class="filter-head collapsed" data-toggle="collapse" data-target="#county" aria-expanded="false" aria-controls="county">
                        County
                        <i class="fas fa-angle-down fa-lg float-right"></i>
                    </h4>
                    <ul class="collapse filter-items" id="county">
                        <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=') ?>">All Counties</a></li>
                        <?php
                        foreach ($countries as $lc => $country):
                            ?>
                            <li><a href="<?= Url::to('/classified/search-car?ClassifiedSearch[slug]=cars&ClassifiedSearch[make]=' . $make . '&ClassifiedSearch[fuel]=' . $fuel . '&ClassifiedSearch[body_type]=' . $body_type . '&ClassifiedSearch[engine_size]=' . $engine_size . '&ClassifiedSearch[county]=' . $country->slug) ?>"><?= Html::encode($country->county) ?></a></li>

                            <?php
                        endforeach;
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="filtered-items border-bottom">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="filtered-item">Cars</h3>
                        </div>
                        <div class="col-md-7">
                            <div class="g-adverts text-center mb-3">
                                <img src="images/posts-ad.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h3 class="adsCount float-right"><?php echo $dataProvider->getTotalCount(); ?> Ads</h3>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="sorting">
                    <div class="mobile-filter d-md-none d-block">
                        <h4 class="filtered-head">
                            Filters
                            <i class="fas fa-angle-right fa-lg float-right"></i>
                        </h4>
                    </div>
                    <select name="account_type" class="options" id="account_type">
                        <option value="">Account Type</option>
                        <option value="">Private &amp; Business</option>
                        <option value="private" <?= (isset($params['ClassifiedSearch']['ad_type']) && !empty($params['ClassifiedSearch']['ad_type']) && $params['ClassifiedSearch']['ad_type'] == 'private' ? "selected" : "") ?>>Private</option>
                        <option value="business" <?= (isset($params['ClassifiedSearch']['ad_type']) && !empty($params['ClassifiedSearch']['ad_type']) && $params['ClassifiedSearch']['ad_type'] == 'business' ? "selected" : "") ?>>Business</option>
                        
                    </select>
                </div>
                <div class="empty-space"></div>
                <div class="empty-space"></div>
                <div class="ads-listing">
                    <?=
                    ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '_item',
                        'layout' => '<div class="row no-gutters">{items}</div>',
                        'itemOptions' => [
                            'class' => 'col-md-4 col-sm-4 col-6 ads-grid',
                        ],
                    ]);
                    ?>
                </div>
                <div class="empty-space"></div>
                <div class="empty-space"></div>
                <nav aria-label="page navigation">
                    <?php
                    echo LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'options' => [
                            'class' => 'pagination justify-content-end'
                        ],
                        'linkContainerOptions' => [
                            'class' => 'page-item'
                        ],
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ]);
                    ?>
                </nav>
                <div class="empty-space"></div>
                <div class="empty-space"></div>
                <div class="empty-space"></div>
                <div class="g-adverts text-center">
                    <img src="images/footer-advert.png" class="img-fluid" alt="">
                </div>

            </div>
        </div>
    </div>
</div>