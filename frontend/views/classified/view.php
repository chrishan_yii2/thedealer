<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedListings */

$this->title = "View Ad";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classified Listings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-listings-view">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView'     => 'ad-view',
        'layout'       => '<div class="content-wrapper product-detais mt-5 mt-md-0">{items}</div>',
    ]); ?>
</div>
