<?php

/** @var Item $model */

use common\models\Item;
use frontend\components\AdViewWidget;

echo AdViewWidget::widget(['model' => $model]);