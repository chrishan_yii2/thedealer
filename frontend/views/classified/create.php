<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedListings */

$this->title = Yii::t('app', 'Submit a Free Classified Ad');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classified Listings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper">
    <div class="container">
        <div class="form ads-form">

            <h2 class="form-head mt-5"><?= Html::encode($this->title) ?></h2>

            <?= $this->render('_form', [
                'model' => $model,
                'userMoldel' => $userMoldel,
            ]); ?>

        </div>
    </div>
</div>
