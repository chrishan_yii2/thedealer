<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ClassifiedCategories;
use yii\helpers\ArrayHelper;
use common\models\LocationsCounties;
use common\models\LocationsTowns;
use backend\models\CarMakes;
use backend\models\CarModels;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedListings */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin(['id' => 'form-ads','options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="input-container">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Ad Title *'); ?>
    </div>
    <div class="input-container">
        <div class="row categories-wrapper">
            <div class="col-md-6 col-sm-6">
                <?= $form->field($model, 'parent_id',[
                    'options' => [
                        'class' => 'form-group mb-sm-0 mb-4'
                    ]])->dropDownList(ArrayHelper::map(
                    ClassifiedCategories::find()->getCategories()->all(),
                    'id', 'category'
                ),[
                    'prompt' => 'Select Main Category',
                    'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('classified/child-cats?id=').'"+$(this).val(),function(data){
                                        $("select#classifiedlistings-classified_cat_id").html(data);
                                        });'
                ])->label('Main Category *'); ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <?= $form->field($model, 'classified_cat_id')->dropDownList(ArrayHelper::map(
                    ClassifiedCategories::find()->all(),
                    'id', 'category'
                ),[
                    'prompt' => 'Select Sub Category',
                ])->label('Sub Category *'); ?>
            </div>
        </div>
    </div>
    <div id="result_cat_id">
        <div id="car_details">
            <h4>Car Details</h4>
<!--            <label>Car Registration</label>-->
<!--            <div class="input-group"><input type="text" class="form-control" placeholder="ENTER CAR REG" name="car-reg" id="carReg"><span class="input-group-btn"><button type="button" class="btn btn-danger" id="getReg">Get Car Info</button></span></div>-->
<!--                <p class="text-center">or</p>-->
            <button type="button" id="editCarDetails" class="btn btn-danger mb-3" data-toggle="collapse" data-target="#car_toggle" aria-expanded="false" aria-controls="car_toggle">Add Car Info Manually</button>
        </div>
        <div id="car_toggle" class="collapse in <?= ($model->isNewRecord) ? '' : 'show'; ?>">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?php
                        $makes = CarMakes::find()->all();
                        $makesList = array(0 => 'Any') + ArrayHelper::map($makes, 'id','make');
                    ?>
                    <?= $form->field($model, 'make_id',[
                        'options' => [
                            'class' => 'form-group mb-sm-0 mb-4'
                        ]])->dropDownList($makesList, [
                                'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('classified/model-list?id=').'"+$(this).val(),function(data){
                                        $("select#classifiedlistings-model_id").html(data);
                                        });'
                    ])->label('Car Make'); ?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?php
                    $models = CarModels::find()->all();
                    $modelsList = array(0 => 'Any') + ArrayHelper::map($models, 'id','model');
                        ?>
                    <?= $form->field($model, 'model_id')->dropDownList($modelsList)->label('Car Model'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?php
                    $fuels = array('petrol' => 'Petrol', 'diesel' => 'Diesel');
                    ?>
                    <?= $form->field($model, 'fuel',[
                        'options' => [
                            'class' => 'form-group mb-sm-0 mb-4'
                        ]])->dropDownList($fuels,[
                                'prompt' => 'Select Fuel'
                    ])->label('Fuel'); ?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?php
                    $transmission = ['M' => 'Manual', 'A' => 'Automatic', 'C' => 'CVT (Continuously Variable Transmission)', 'S' => 'Semi - Automatic', 'T' => 'Tronic', 'other' => 'Other' ];
                    ?>
                    <?= $form->field($model, 'transmission')->dropDownList($transmission, [
                            'prompt' => 'Select Transmission'
                    ])->label('Transmission'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'co2_emissions')->textInput(['maxlength' => true])->label('CO2 emissions'); ?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'year')->textInput(['maxlength' => true])->label('Year Of Vehicle'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <?php
                    $types = ['hatchback' => 'Hatchback', 'saloon' => 'Saloon', 'estate' => 'Estate', 'convertible' => 'Convertible', 'MPV' => 'MPV', 'coupe' => 'Coupe', 'cabriolet' => 'Cabriolet', 'SUV' => 'SUV', '4X4' => '4X4'];
                    ?>
                    <?= $form->field($model, 'body_type',[
                        'options' => [
                            'class' => 'form-group mb-sm-0 mb-4'
                        ]])->dropDownList($types,[
                        'prompt' => 'Select Body Type'
                    ])->label('Body Type'); ?>
                </div>
                <div class="col-md-4 col-sm-4">
                    <?php
                    $colour = ['beige' => 'beige','black' => 'black','blue' => 'blue','bronze' => 'bronze','brown' => 'brown','burgundy' => 'burgundy','gold' => 'gold','green' => 'green','grey' => 'grey','indigo' => 'indigo','magenta' => 'magenta','maroon' => 'maroon','navy' => 'navy','orange' => 'orange','pink' => 'pink','purple' => 'purple','red' => 'red','silver' => 'silver','turquoise' => 'turquoise','white' => 'white','yellow' => 'yellow'];
                    ?>
                    <?= $form->field($model, 'colour')->dropDownList($colour, [
                        'prompt' => 'Select Colour'
                    ])->label('Colour'); ?>
                </div>
                <div class="col-md-4 col-sm-4">
                    <?php
                    $size = ['-1L' => 'Less than 1.0L','1L' => '1.0L - 1.3L','1.4L' => '1.4L - 1.6L','1.7L' => '1.7L - 1.9L','2L' => '2.0L - 2.5L','2.6L' => '2.6L - 2.9L','3L' => '3.0L - 3.9L','4L' => '4.0L - 4.9L','5L' => '5.0L +'];
                    ?>
                    <?= $form->field($model, 'engine_size')->dropDownList($size, [
                        'prompt' => 'Select Engine Size'
                    ])->label('Engine Size'); ?>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <?= $form->field($model, 'nct_date')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'options' => ['placeholder' => 'DD/MM/YYYY'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy'
                    ]
                ])->label('NCT/MOT date'); ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <?= $form->field($model, 'tax_date')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'options' => ['placeholder' => 'DD/MM/YYYY'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy'
                    ]
                ])->label('Taxed until date'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6">
                <?= $form->field($model, 'mileage')->textInput(['maxlength' => true])->label('Mileage'); ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <?= $form->field($model, 'version')->textInput(['maxlength' => true])->label('Vehicle Version'); ?>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-4 col-sm-4">
                <?php
                    $comfort = ['Air Conditioning' => 'Air Conditioning','Phone Kit' => 'Phone Kit','Front Electric Windows' => 'Front Electric Windows','Rear Electric Windows' => 'Rear Electric Windows','Heated Seats' => 'Heated Seats','Leather Seats' => 'Leather Seats','Cloth interior' => 'Cloth interior','Split Rear Seats' => 'Split Rear Seats','Bluetooth' => 'Bluetooth','DVD System' => 'DVD System','Metallic Paint' => 'Metallic Paint','Colour Coded' => 'Colour Coded','Lumbar Support' => 'Lumbar Support','Roof Rail' => 'Roof Rail','Electric Front Seats' => 'Electric Front Seats','Trip Computer' => 'Trip Computer','6 Disc CD' => '6 Disc CD'];
                ?>
                <?= $form->field($model, 'search_options[]')->checkboxList($comfort,[
                    'item' => function($index, $label, $name, $checked, $value) {
                        return "<div class='custom-control custom-checkbox'>
                                    <input type='checkbox' {$checked}  class='custom-control-input' id='{$value}' name='{$name}' value='{$value}'>
                                    <label class='custom-control-label' for='{$value}'>{$label}</label>
                                </div>";
                    }
                ] )->label('Comfort/Entertainment') ?>
            </div>
            <div class="col-md-4 col-sm-4">
                <?php
                $visibility = ['Heated Mirrors' => 'Heated Mirrors','Xenon Headlights' => 'Xenon Headlights','Headlamp Wash' => 'Headlamp Wash','Manual Rain Sensors' => 'Manual Rain Sensors','Parking Sensors' => 'Parking Sensors','Reversing Camera' => 'Reversing Camera','Front Fogs' => 'Front Fogs']
                ?>
                <?= $form->field($model, 'search_options[]')->checkboxList($visibility,[
                    'item' => function($index, $label, $name, $checked, $value) {
                        return "<div class='custom-control custom-checkbox'>
                                    <input type='checkbox' {$checked}  class='custom-control-input' id='{$value}' name='{$name}' value='{$value}'>
                                    <label class='custom-control-label' for='{$value}'>{$label}</label>
                                </div>";
                    }
                ] )->label('Visibility Aids') ?>
            </div>
            <div class="col-md-4 col-sm-4">
                <?php
                $safety = ['Anti Lock Brakes' => 'Anti Lock Brakes','ESP' => 'ESP','Power Steering' => 'Power Steering','Rake/Reach Steering' => 'Rake/Reach Steering','Remote Locking' => 'Remote Locking','Immobiliser' => 'Immobiliser','Alarm' => 'Alarm','Side Airbags' => 'Side Airbags','Driver Airbag' => 'Driver Airbag','Passenger Airbag' => 'Passenger Airbag','3x3 Rear Seat Belts' => '3x3 Rear Seat Belts']
                ?>
                <?= $form->field($model, 'search_options[]')->checkboxList($safety,[
                    'item' => function($index, $label, $name, $checked, $value) {
                        return "<div class='custom-control custom-checkbox'>
                                    <input type='checkbox' {$checked}  class='custom-control-input' id='{$value}' name='{$name}' value='{$value}'>
                                    <label class='custom-control-label' for='{$value}'>{$label}</label>
                                </div>";
                    }
                ] )->label('Safety/Security') ?>
            </div>
        </div>
    </div>
    <div class="input-container">
        <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('Detailed Description *') ?>
    </div>
    <div class="input-container">
        <div class="row">
            <div class="col-md-3 col-4">
                <?= $form->field($model, 'currency')
                                    ->dropDownList([
                                            'EUR' => "€",
                                            'GBP' => "£",
                                            'FREE' => 'FREE',
                                    ])->label('Select currency *')?>
            </div>
            <div class="col-md-9 col-8">
                <?= $form->field($model, 'price')->textInput(['maxlength' => true])->label('Price *') ?>
            </div>
        </div>
    </div>
    <div class="input-container">
        <div class="row align-items-center upload-imgs">
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label mb-0" for="">Upload Photos</label>
                </div>
            </div>
            <div class="col-md-10">
                <div class="form-group required upload-img">
<!--                    <label for="adsimages" class="control-label adsimages">
                        <i class="fas fa-plus-circle fa-2x"></i>
                    </label>-->
                    <input type="file" id="adsimages" name="ClassifiedListings[images][]" multiple accept="image/*">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <label for="" class="sec-head mb-4">Place in Printed Edition</label>
    </div>
    <div class="input-container border-0">
        <div class="row print-advert">
            <div class="col-md-4">
                <?= $form->field($model, 'edition_one',[
                                        'options' => [
                                            'class' => 'form-group mb-sm-0 mb-4'
                                        ]])->dropDownList([
                                                    'no' => 'No, Do Not Print My Advert',
                                                    'yes' => 'Yes, Print My Advert',
                                            ])->label('Donegal, Northwest & West') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'edition_two',[
                                        'options' => [
                                            'class' => 'form-group mb-sm-0 mb-4'
                                        ]])->dropDownList([
                                            'no' => 'No, Do Not Print My Advert',
                                            'yes' => 'Yes, Print My Advert',
                                        ])->label('Northern Ireland') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'edition_three',[
                                        'options' => [
                                            'class' => 'form-group mb-sm-0 mb-4'
                                        ]])->dropDownList([
                                            'no' => 'No, Do Not Print My Advert',
                                            'yes' => 'Yes, Print My Advert',
                                        ])->label('Ireland Midlands') ?>
            </div>
        </div>
    </div>
    <div class="input-container border-0">
        <div class="row contact-wrapper">
            <div class="col-md-6 mb-sm-0 mb-3">
                <div class="form-group contact-details">
                    <?php
                    if(isset($model->town_id)){
                        $town = $model->town->town;
                        $town_id = $model->town_id;
                    } else {
                        $town = isset($userMoldel->locationsTowns->town) ? $userMoldel->locationsTowns->town : "";
                        $town_id = $userMoldel->town_id;
                    }

                    if(isset($model->county_id)){
                        $county = $model->county->county;
                        $county_id = $model->county_id;
                    } else {
                        $county = isset($userMoldel->locationsCounties->county) ? $userMoldel->locationsCounties->county : "";
                        $county_id = $userMoldel->county_id;
                    }

                    ?>
                    <label class="control-label">Location</label>
                    <input type="text" readonly class="form-control-plaintext" value="Town: <?= $town ?>">
                    <input type="text" readonly class="form-control-plaintext" value="County: <?= $county ?>">
                    <a class="btn btn-primary btn-small" data-toggle="collapse" href="#edit_location" role="button">Edit</a>
                </div>
                <?php $collapse1 = (empty($town) || empty($county)) ? "collapse show" :  "collapse"; ?>
                <div id="edit_location" class="edit-contact-details <?= $collapse1 ?>">
                    <p class="edit-contact-desc">Please provide a location where this item can be taken to, or collected from.</p>
                    <?= $form->field($model, 'county_id')
                        ->dropDownList(
                            ArrayHelper::map(
                                LocationsCounties::find()->all(),
                                'id', 'county'),
                            [
                                'options' => [ $county_id => ['Selected'=>'selected']]
                            ],
                            [
                                'prompt'=>'Select Country',
                                'onchange'=> ''
                            ])->label('County*') ?>

                    <?= $form->field($model, 'town_id')
                        ->dropDownList(
                            ArrayHelper::map(
                                LocationsTowns::find()->all(),
                                'id', 'town'),
                            [
                                'options' => [ $town_id => ['Selected'=>'selected']]
                            ],
                            [
                                'prompt'=>'Select Town',
                                'onchange'=> ''
                            ])->label('Town*') ?>
                </div>
            </div>
            <div class="col-md-6">
                <?php
                if(isset($model->email)){
                    $email = $model->email;
                } else {
                    $email = $userMoldel->email;
                }

                if(isset($model->telephone)){
                    $telephone = $model->telephone;
                } else {
                    $telephone = $userMoldel->telephone;
                }
                if(isset($model->mobile)){
                    $mobile = $model->mobile;
                } else {
                    $mobile = $userMoldel->mobile;
                }

                ?>
                <div class="form-group contact-details">
                    <label class="control-label">Contact Deatils</label>
                    <input type="text" readonly class="form-control-plaintext" value="Email: <?= $email ?>">
                    <input type="text" readonly class="form-control-plaintext" value="Tel: <?= $telephone ?>">
                    <input type="text" readonly class="form-control-plaintext" value="Mob: <?= $mobile ?>">
                    <a class="btn btn-primary btn-small" data-toggle="collapse" href="#edit_contact" role="button">Edit</a>
                </div>
                <?php $collapse = (empty($email) || empty($telephone) || empty($mobile)) ? "collapse show" :  "collapse"; ?>
                <div id="edit_contact" class="edit-contact-details <?= $collapse ?>">
                    <p class="edit-contact-desc">Please enter your contact phone numbers and/or email address so customers can contact you.</p>
                    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true, 'value' => $telephone]) ?>

                    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true, 'value' => $mobile]) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'value' => $email]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="empty-space"></div>
    <div class="empty-space"></div>
    <div class="form-group text-center mb-5">
        <button type="submit" class="btn btn-lg btn-default" name="signup-button">Finish</button>
    </div>
<?php ActiveForm::end(); ?>