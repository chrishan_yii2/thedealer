<?php

namespace frontend\controllers;

use common\models\Users;
use Yii;
use common\models\ClassifiedListings;
use common\models\ClassifiedCategories;
use frontend\models\ClassifiedSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\AdsenseCategory;
use backend\models\CarModels;

/**
 * ClassifiedController implements the CRUD actions for ClassifiedListings model.
 */
class ClassifiedController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                 'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('error', 'Please login to continue');
                    return $this->redirect('/login');
                },
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'search', 'view','search-car'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'search', 'view', 'create', 'update','delete','child-cats','search-car', 'model-list'],
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all ClassifiedListings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClassifiedSearch();
        $dataProvider = $searchModel->recentAds(Yii::$app->request->queryParams);
        $adsence = AdsenseCategory::find()->where(['ad_location' => 'Home Page'])->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'adsence'=> $adsence,
        ]);
    }
    
    /**
     * Lists all ClassifiedListings models.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = new ClassifiedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $params = Yii::$app->request->queryParams;
        if(isset($params['per-page'])){
            $dataProvider->pagination = ['pageSize' => $params['per-page'],];
        } else {
            $dataProvider->pagination = ['pageSize' => 15,];
        }
        
        $categories = ClassifiedCategories::find()->where(['parent_id' => 0])->andWhere(['status' => 'active'])->all();
        $ifExist = [];
        
        if(isset($params['ClassifiedSearch']['slug'])){
            $ifExist = \common\models\ClassifiedCategories::find()->where(['slug' => $params['ClassifiedSearch']['slug']])->andWhere(['status' => 'active'])->one();
        }
        if(isset($ifExist->parent_id) && $ifExist->parent_id == 0){
            $subCategories = ClassifiedCategories::find()->where(['parent_id' => $ifExist->id])->andWhere(['status' => 'active'])->all();
        }else{
            $subCategories = ClassifiedCategories::find()->where(['!=', 'parent_id', 0])->andWhere(['status' => 'active'])->all();
        }
        
        $countries = \common\models\LocationsCounties::find()->where(['status' => \common\models\Users::STATUS_ACTIVE])->all();
//                $data = $dataProvider->query->all();
//        foreach ($data as $key => $value) {
//            echo "<pre>";
//            print_r($value);exit;
//        }
        
        return $this->render('ads-listing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => $categories,
            'subCategories' => $subCategories,
            'countries' => $countries
        ]);
    }
    
    
    /**
     * Lists all ClassifiedListings models.
     * @return mixed
     */
    public function actionSearchCar()
    {
        $searchModel = new ClassifiedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
//        $max = $dataProvider->query->find()->max('price');
        
        $params = Yii::$app->request->queryParams;
        if(isset($params['per-page'])){
            $dataProvider->pagination = ['pageSize' => $params['per-page'],];
        } else {
            $dataProvider->pagination = ['pageSize' => 15,];
        }
        
        $categories = ClassifiedCategories::find()->where(['parent_id' => 0])->andWhere(['status' => 'active'])->all();
        $ifExist = [];
        
        if(isset($params['ClassifiedSearch']['slug'])){
            $ifExist = \common\models\ClassifiedCategories::find()->where(['slug' => $params['ClassifiedSearch']['slug']])->andWhere(['status' => 'active'])->one();
        }
        if(isset($ifExist->parent_id) && $ifExist->parent_id == 0){
            $subCategories = ClassifiedCategories::find()->where(['parent_id' => $ifExist->id])->andWhere(['status' => 'active'])->all();
        }else{
            $subCategories = ClassifiedCategories::find()->where(['!=', 'parent_id', 0])->andWhere(['status' => 'active'])->all();
        }
        
        $countries = \common\models\LocationsCounties::find()->where(['status' => \common\models\Users::STATUS_ACTIVE])->all();
        $carMakes = \backend\models\CarMakes::find()->where(['status' => \common\models\Users::STATUS_ACTIVE])->all();
        
        return $this->render('car-listing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => $categories,
            'subCategories' => $subCategories,
            'countries' => $countries,
            'carMakes' => $carMakes
        ]);
    }

    /**
     * Displays a single ClassifiedListings model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        $searchModel = new ClassifiedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $params = Yii::$app->request->queryParams;
        if(isset($params['ClassifiedSearch']['id']) && !empty($params['ClassifiedSearch']['id'])){
            $model = ClassifiedListings::findOne(['id' => $params['ClassifiedSearch']['id']]);
            $model->views = $model->views + 1;
            $model->save(false);
        }
        
        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new ClassifiedListings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        if(Yii::$app->user->identity->user_type == Users::ADMIN_USER){
            $this->redirect('/admin/classified/create');
        }
        
        $largePath = './uploads/large';
        $smallPath = './uploads/small';
        $fileName = 'ClassifiedListings';

        if (isset($_FILES[$fileName]) && !empty($_FILES[$fileName])) {

            $imgCheck = \yii\web\UploadedFile::getInstancesByName($fileName);

            foreach ($imgCheck as $f => $file) {
                $size = getimagesize($file->tempName);
                if($size[0] < 768){
                    Yii::$app->session->setFlash('error', "Image size is less then 768px");
                    return $this->redirect(Yii::$app->request->referrer);
                }
            
            }

        }
        $price_type = ClassifiedListings::CLASSIFIED_ADVERT;
        $ads_price = \backend\models\base\Prices::find()->where(['price_type' => $price_type])->one();
        
       if(Yii::$app->user->identity->account_balance < $ads_price->price){
           Yii::$app->session->setFlash('error', "You don't have enough Balance to create add");
       }
        $model = new ClassifiedListings();
        
        $userMoldel = Users::findOne($userId = Yii::$app->user->identity->id);
        
        if(isset(Yii::$app->request->isPost) && $model->load(Yii::$app->request->post())){
           if(Yii::$app->user->identity->account_balance < $ads_price->price){
               Yii::$app->session->setFlash('error', "You don't have enough Balance to create add");
               return $this->redirect(Yii::$app->request->referrer);
           }else{
                $post = Yii::$app->request->post();
                $model->load(Yii::$app->request->post());
                $model->user_id = Yii::$app->user->identity->id;
                $model->start_date = time();
                $model->date_added = time();
                $model->end_date = time() + (120*24*60*60);
                $model->search_price = preg_replace("/[^0-9+.]/","",$post['ClassifiedListings']['price']);
                $model->search_options = empty($post['ClassifiedListings']['search_options']) ? "" : implode(',', $post['ClassifiedListings']['search_options']);

                if ($model->save()) {
                    
                    
                    
                    if (isset($_FILES[$fileName]) && !empty($_FILES[$fileName])) {
                        $files = \yii\web\UploadedFile::getInstancesByName($fileName);
                        
                        foreach ($files as $f => $file) {
                            $newFileName = \Yii::$app->security
                            ->generateRandomString().'.'.$file->extension;
                            if ($file->saveAs($smallPath . '/' . $newFileName, false) && $file->saveAs($largePath . '/' . $newFileName)) {
                                $imgModel = new \common\models\Images;
                                $imgModel->table_name = $model->tableName();
                                $imgModel->parent_id = $model->id;
                                $imgModel->image_name = $newFileName;
                                $imgModel->order = 1;
                                $imgModel->rotate = 0;
                                $imgModel->date_added = time();
                                $imgModel->save(false);
                            }
                        }
                    }
                    return $this->redirect(['users/my-ads']);
                }
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'userMoldel' => $userMoldel,
        ]);
    }
    

    /**
     * Updates an existing ClassifiedListings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->search_options != ""){
            $model->search_options = explode(',', $model->search_options);
        }

        if(isset(Yii::$app->request->isPost) && $model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $model->load(Yii::$app->request->post());
            $model->search_options = empty($post['ClassifiedListings']['search_options']) ? "" : implode(',', $post['ClassifiedListings']['search_options']);

            if ($model->save()) {
                return $this->redirect(['users/my-ads']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ClassifiedListings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->status = \common\models\Users::STATUS_INACTIVE;
        $model->save(FALSE);
        return $this->redirect(['index']);
    }

    /**
     * Finds the ClassifiedListings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClassifiedListings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClassifiedListings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionChildCats($id) {
        
        $countChildCats = ClassifiedCategories::find()->childCategories($id)->count();
        $childCats = ClassifiedCategories::find()->childCategories($id)->all();
        if ($countChildCats > 0) {
            echo '<option> Select Sub Category </option>';
            foreach ($childCats as $childCat) {
                echo '<option value="'.$childCat->id.'">'.$childCat->category.'</option>';
            }
        }
        else{
            echo '<option> - </option>';
        }
    }

    public function actionModelList($id) {

        $countModelsList = CarModels::find()->where([ 'make_id' => $id ])->count();
        $modelsLists = CarModels::find()->where([ 'make_id' => $id ])->all();
        if ($countModelsList > 0) {
            echo '<option value="0"> Any </option>';
            foreach ($modelsLists as $modelsList) {
                echo '<option value="'.$modelsList->id.'">'.$modelsList->model.'</option>';
            }
        }
        else{
            echo '<option value="0"> Any </option>';
        }
    }

}
