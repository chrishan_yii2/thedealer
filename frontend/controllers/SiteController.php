<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\LocationsTowns;
use frontend\models\VerifyAccount;
use conquer\select2\Select2Action;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'ajax' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataCallback'],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        
        $post = Yii::$app->request->post();
        if(isset($post) && !empty($post)){
            $post['LoginForm']['username'] = (isset($post['LoginForm']['username']) && !empty($post['LoginForm']['username'])) ? trim($post['LoginForm']['username']) : "";
        }
        if ($model->load($post) && $model->login()) {
            if(Yii::$app->user->identity->user_type == \common\models\Users::ADMIN_USER){
                return $this->redirect('/admin/classified/index?ClassifiedSearch[ad_type]=private');
            }
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    /**
     * Displays faq page.
     *
     * @return mixed
     */
    public function actionFaq()
    {
        return $this->render('faq');
    }
    
    /**
     * Displays Terms & condition page.
     *
     * @return mixed
     */
    public function actionTermConditions()
    {
        return $this->render('terms');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ( isset(Yii::$app->request->isPost) && $model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $model->user_type = $post['SignupForm']['user_type'];
            if ($user = $model->signup() && $model->sendAccountActivationEmail()) {
                Yii::$app->session->setFlash('success', 'Your account has been created successfully. Please verify you email.');
                return $this->goHome();
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    /**
     * Verify Account
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionVerify($token)
    {
        try {
            $model = new VerifyAccount($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->verifyAccount()) {
            Yii::$app->session->setFlash('success', 'Your new account has been activated.');
            return $this->goHome();
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionPasswordReset()
    {
        $model = new PasswordResetForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('passwordReset', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionLists($id) {
        $this->enableCsrfValidation = false;
        $countPosts = LocationsTowns::find()
               ->where(['county_id' => $id])
                ->count();
        $posts = LocationsTowns::find()
               ->where(['county_id' => $id])
                ->all();
        if ($countPosts > 0) {
                echo '<option> Select Town </option>';
            foreach ($posts as $post) {
                echo '<option value="'.$post->id.'">'.$post->town.'</option>';
            }
        }
        else{
            echo '<option> - </option>';
        }
    }
    
    public function actionWatchList($id){
        echo "hello";exit;
        if(yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->request->referrer);
        }
        $userId = Yii::$app->user->identity->id;
        $ifExist = \common\models\ClassifiedListings::find()
                                                    ->where(['user_id' => $userId,'listing_id' => $id])
                                                    ->one();
        
        echo "<pre>";
        print_r($ifExist);exit;
    }
}
