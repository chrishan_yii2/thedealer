<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use \common\models\WatchlistLookup;
use frontend\models\ClassifiedSearch;
use common\models\Users;

/**
 * Site controller
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('error', 'Please login to continue');
                    return $this->redirect('/login');
                },
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['signup', 'business-details', 'business-json','business-directory','request-password-reset'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['dashboard', 'my-ads', 'buy-credit', 'business-details', 'business-json','business-directory','edit-profile', 'remove-watchlist','my-watchlist','watch-list'],
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    
    public function actionDashboard()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        return $this->render('dashboard');
    }

    
    public function actionMyAds()
    {
        $id = Yii::$app->user->identity->id;
        $searchModel = new ClassifiedSearch();
        $dataProvider = $searchModel->searchByUser($id);
        
        return $this->render('my-ads',[
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays Buy Credit page.
     *
     * @return mixed
     */
    public function actionBuyCredit()
    {
        return $this->render('buy-credit');
    }

    /**
     * Signs users up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($users = $model->signup()) {
                if (Yii::$app->getUsers()->login($users)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    /**
     * Edit Profile
     *
     * @return mixed
     */
    public function actionEditProfile()
    {
        
        $model = \common\models\Users::findOne($userId = Yii::$app->user->identity->id);
        $model->setScenario($model->user_type);
        if ($model->load(Yii::$app->request->post())) {
            if ($users = $model->save(false)) {
                Yii::$app->session->setFlash('success', 'Profile Updated Successfully');
            }
        }

        return $this->render("edit-profile", [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionWatchList($id){
        
        if(yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->request->referrer);
        }
        $userId = Yii::$app->user->identity->id;
        $ifExist = WatchlistLookup::find()
                                    ->where(['user_id' => $userId,'listing_id' => $id])
                                    ->one();
        
        
        if(!empty($ifExist) && count($ifExist) > 0){
            Yii::$app->session->setFlash('error', 'Alreay Added in Watchlist');
            if(Yii::$app->request->isAjax){
                $response = array('status' => 'error','msg' => 'Alreay Added in Watchlist');
                return json_encode($response);exit;
            }
            return $this->redirect(Yii::$app->request->referrer);
        }else{
            $model = new WatchlistLookup();
            
            $model->user_id = $userId;
            $model->listing_id = $id;
            if($model->save(false)){
                Yii::$app->session->setFlash('success', 'Added to watchlist');
                if(Yii::$app->request->isAjax){
                    $response = array('status' => 'success');
                    return json_encode($response);exit;
                }
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
    }
    
    public function actionMyWatchlist(){
        
        $userId = Yii::$app->user->identity->id;
        $watchlist = WatchlistLookup::find()
                                    ->where(['user_id' => $userId])
                                    ->all();
        
        return $this->render('watch-list', [
            'model' => $watchlist,
        ]);
        
    }
    
    public function actionRemoveWatchlist($id){
        
        if(yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->request->referrer);
        }
        $userId = Yii::$app->user->identity->id;
        $ifExist = WatchlistLookup::find()
                                    ->where(['user_id' => $userId,'listing_id' => $id])
                                    ->one();
                                    
        if($ifExist->delete()){
            Yii::$app->session->setFlash('error', 'Removed from Watchlist');
            return $this->redirect(Yii::$app->request->referrer);
        }   
    }
    
    public function actionBusinessDirectory(){
        
//        $searchModel = new \frontend\models\UsersSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $countries = \common\models\LocationsCounties::find()->all();
        
        $countyTowns = [];
        foreach ($countries as $c => $country){
            $countyTowns[$country->slug."~".$country->id] = $country->county;
            $towns = \common\models\LocationsTowns::find()->where(['county_id' => $country->id])->all();
            if(!empty($towns) && count($towns) > 0){
                foreach ($towns as $t => $town){
                    $countyTowns[$town->slug."~".$town->id] = $country->county." - ".$town->town;
                }
            }
        }
//        echo "<pre>";
//        print_r($countyTowns);exit;
        return $this->render('business-list', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
            'countyTowns' => $countyTowns
        ]);
        
    }
    
    public function actionCheckout(){
        // Setup order information array with all items
        $params = [
            'method'=>'paypal',
            'intent'=>'sale',
            'order'=>[
                'description'=>'Payment description',
                'subtotal'=>44,
                'shippingCost'=>0,
                'total'=>44,
                'currency'=>'USD',
                'items'=>[
                    [
                        'name'=>'Item one',
                        'price'=>10,
                        'quantity'=>1,
                        'currency'=>'USD'
                    ],
                    [
                        'name'=>'Item two',
                        'price'=>12,
                        'quantity'=>2,
                        'currency'=>'USD'
                    ],
                    [
                        'name'=>'Item three',
                        'price'=>1,
                        'quantity'=>10,
                        'currency'=>'USD'
                    ],

                ]

            ]
        ];
        
        // In this action you will redirect to the PayPpal website to login with you buyer account and complete the payment
        Yii::$app->PayPalRestApi->checkOut($params);
    }
    
    public function actionMakePayment(){
         // Setup order information array 
        $params = [
            'order'=>[
                'description'=>'Payment description',
                'subtotal'=>44,
                'shippingCost'=>0,
                'total'=>44,
                'currency'=>'USD',
            ]
        ];
      // In case of payment success this will return the payment object that contains all information about the order
      // In case of failure it will return Null
      print_r(Yii::$app->PayPalRestApi->processPayment($params));

    }
    
    public function actionBusinessJson(){
        
        $query = Users::find()
                            ->select(['id','email','company_name', 'user_lat','user_long','address_one','email','telephone','website'])
                            ->where(['!=', 'company_name', ""])
                            ->andWhere(['user_type' => Users::BUSINESS_USER]);
                            
        if(isset(Yii::$app->request->isAjax) && !empty(Yii::$app->request->post())){
            $post = Yii::$app->request->post();
            
            if(isset($post['directory_category']) && !empty($post['directory_category'])){
                $query->andFilterWhere(['like', 'company_name', $post['directory_category']]);
            }
            if(isset($post['directory_location']) && !empty($post['directory_location'])){
                $town = explode('~', $post['directory_location']);
                $query->orWhere(['county_id' => $town[1]]);
                $query->orWhere(['town_id' => $town[1]]);
            }
            
        }
        
        $userLists = $query->orderBy(['date_added' => SORT_DESC])
                            ->limit(200)
                            ->all();
        
        
        
        $json = [];
        
        foreach ($userLists as $key => $userList) {
            $image = \common\models\Images::find()
                                            ->where(['table_name' => 'users'])
                                            ->andWhere(['parent_id' => $userList->id])
                                            ->one();
            $json['places'][$key]['id'] = $userList->id;
            $json['places'][$key]['name'] = $userList->company_name;
            $json['places'][$key]['lat'] =$userList->user_lat;
            $json['places'][$key]['lng'] = $userList->user_long;
            $json['places'][$key]['email'] = $userList->email;
            $json['places'][$key]['address'] = $userList->address_one;
            $json['places'][$key]['telephone'] = $userList->telephone;
            $json['places'][$key]['website'] = $userList->website;
            
            if(isset($image->image_name) && !empty($image->image_name))
            $json['places'][$key]['image'] = "/uploads/small/".$image->image_name;
            
            
        }
        
        echo \yii\helpers\Json::encode($json); exit;
    }
    
    public function actionBusinessDetails(){
        $searchModel = new \frontend\models\UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $ads = \common\models\ClassifiedListings::find()
                                                ->where(['user_id' => $dataProvider->models[0]->id])
                                                ->andWhere(['status' => 'active'])
                                                ->orderBy(['date_added' => SORT_DESC])
                                                ->limit(8)
                                                ->all();
        
        return $this->render('business-details', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ads' => $ads
        ]);
    }
}
