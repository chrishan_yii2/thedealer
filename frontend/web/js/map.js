var map;
var boundbox = null;
var icon1 = "http://www.thedealer.ie/application/views/templates/default/images/dealer-marker.png";
var icon2 = "http://www.thedealer.ie/application/views/templates/default/images/dealer-marker-black.png";
var blank = "http://www.thedealer.ie/application/views/templates/default/images/blank.png";
var arrMarkers = [];
var arrInfoWindows = [];
var markerCluster = "";
var gmarkers = [];
var htmls = [];

var styles = [[{
            url: 'http://www.thedealer.ie/application/views/templates/default/images/marker-small.png',
            height: 35,
            width: 35,
            opt_anchor: [16, 0],
            opt_textColor: '#000000'
        },
        {
            url: 'http://www.thedealer.ie/application/views/templates/default/images/marker-medium.png',
            height: 45,
            width: 45,
            opt_anchor: [24, 0],
            opt_textColor: '#FFF',
            textColor: '#000'},
        {
            url: 'http://www.thedealer.ie/application/views/templates/default/images/marker-large.png',
            height: 55,
            width: 55,
            opt_anchor: [32, 0]
        }]];


function mapInit(json_url) {
    var centerCoord = new google.maps.LatLng(52.996524259832526, -5.327880859375);
    boundbox = new google.maps.LatLngBounds();
    var mapOptions = {
        zoom: 6,
        center: centerCoord,

        mapTypeControl: false,
        panControl: false,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        treetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },

        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

}



function getCenterOnDrag() {
    google.maps.event.addListener(map, 'dragend', function () {
        map.getCenter(); 

    });

}
function getCenterOnZoom() {
    google.maps.event.addListener(map, 'zoom_changed', function () {
        console.log();
        map.getCenter(); 
    });

}

function getLocationsLatLng(location) {

    geocoder = new google.maps.Geocoder();
    var address = location;
    geocoder.geocode({'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            alert(results[0].geometry.location);
            return results[0].geometry.location;
        } else {
            return null;
        }



    });
}



function getLocationBeforeSubmit() {

    $("#submit").click(function (event) {
        var location = $(".directory_location").val();
        event.preventDefault();

    });

}


function closeAllInfoWindows() {
    for (var i = 0; i < arrInfoWindows.length; i++) {
        arrInfoWindows[i].close(map, arrMarkers[i]);
    }
}
function resetMarkers() {
    closeAllInfoWindows();
    for (var i = 0; i < arrMarkers.length; i++) {
        arrMarkers[i].setIcon(icon1);


        $("#" + i).css("background-color", "#fff");
        $("#" + i).css("background-image", "none");

    }
}

function getUrlVars() {

    var vars = location.pathname.split('/');
    return vars[0];
}


function checkHistory() {
    loadHistoryContent('/users/business-json');
}

$(function () {
    // initialize map (create markers, infowindows and list)
    checkHistory();

    mapInit();

    $(window).bind("resize", function () {

    });
    getCenterOnDrag();
    getCenterOnZoom();
    onChangeInput();
    //getLocationBeforeSubmit();
    // "live" bind click event
    $("#markers a").on("click", function () {
        var i = $(this).attr("rel");
        // this next line closes all open infowindows before opening the selected one
        //for(x=0; x < arrInfoWindows.length; x++){ arrInfoWindows[x].close(); }
        arrInfoWindows[i].open(map, arrMarkers[i]);
    });

    addHoverToMap();



});

function onChangeInput() {


    $("#directory_find").submit(function (event) {
        loadSearch();
        event.preventDefault();
    });

}



function clearOverlays() {
    for (var i = 0; i < arrMarkers.length; i++) {
        arrMarkers[i].setMap(null);
    }
    arrMarkers.length = 0;

    for (var i = 0; i < arrInfoWindows.length; i++) {
        arrInfoWindows[i].setMap(null);
    }
    arrInfoWindows.length = 0;


}

function loadLinks(data) {
    $("#bus_listings div").remove();
    var items = [];
    var index = 0;
    $.each(data.places, function (i, item) {
        items.push('<div class="result" data-rowid="' + index + '" id="' + index + '"><i class="flaticon-pin"></i><a href="/business_directory/details/' + item.id + '/' + item.slug + '">' + item.name + '</a></div>');
        index++;
    });  // close each()

    $('#bus_listings').append(items.join(''));
    addHoverToMap();
//                $('#status .current-status').remove();
//                $('#status .loading').remove();
//                $('#status').append('<div class="current-status">' + data.status + '</div>');
    $('#pagenav .page_numbers').remove();
    $('#pagenav').append(data.pages);
}

function addHoverToMap()
{
    $(".result").hover(
            function () {
                //closeAllInfoWindows();
                resetMarkers();
                map.panTo(arrMarkers[this.id].getPosition());

                arrInfoWindows[this.id].open(map, arrMarkers[this.id]);
                arrMarkers[this.id].setIcon(icon2);
            });

}
var panel = $('#bus_listings');
function buildMap(data)
{

    panel.innerHTML = '';
    $("#bus_listings div").remove();

    console.log(data);
//                console.log(json);
    $.each(data.places, function (i, item) {

        var markerPosition = new google.maps.LatLng(item.lat, item.lng);
        $("#markers").append('<li><a href="#" rel="' + i + '">' + item.title + '</a></li>');
        var marker = new google.maps.Marker({
            position: markerPosition,
            map: map,
            title: item.title,
            size: new google.maps.Size(32, 40),
            icon: icon1
        });

        boundbox.extend(markerPosition);
        arrMarkers[i] = marker;
        if (item.name) {
            var businessDetails = "<div class='marker-info-win'>";
            if (item.name) {
                businessDetails = businessDetails + "<h1 class='marker-heading'><a href='/users/business-details?UsersSearch[id]=" + item.id + "'>" + item.name + "</a></h1>"
            }
            if (item.image) {
                businessDetails = businessDetails + "<img class='map-marker-image' src='" + item.image + "' />"
            }
            if (item.address) {
                businessDetails = businessDetails + "<p class='map-marker-address'>A: " + item.address + "</p>"
            }
            if (item.telephone) {
                businessDetails = businessDetails + "<p class='map-marker-telephone'>T: " + item.telephone + "</p>"
            }
            if (item.email) {
                businessDetails = businessDetails + "<p class='map-marker-email'>E: <a href='mailto:" + item.email + "'>" + item.email + "</a></p>"
            }
            if (item.website) {
                businessDetails = businessDetails + "<p class='map-marker-website'>W: " + item.website + "</p>"
            }
            businessDetails = businessDetails + "</div>";
        }
        var infowindow = new google.maps.InfoWindow({

            content: businessDetails
        });

        arrInfoWindows[i] = infowindow;
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setOptions({pixelOffset: new google.maps.Size(0, 0)});
            infowindow.open(map, marker);
        });

        google.maps.event.addListener(marker, 'mouseover', function () {

            resetMarkers();
            infowindow.setOptions({pixelOffset: new google.maps.Size(0, 0)});
            infowindow.open(map, marker);
            marker.setIcon(icon2);
            scrollToElement(i);

        });


        //////
        var titleText = item.name;
        if (titleText === '') {
            titleText = 'No title';
        }
        var glyphIcon = document.createElement('i');

        var el = document.createElement('div');
        var title = document.createElement('A');
        el.className = 'result list-group-item';
        el.id = i;
        title.href = '/users/business-details?UsersSearch[id]=' + item.id;
        title.className = 'title';
        title.innerHTML = titleText;
        glyphIcon.className = 'flaticon-pin';
        if (item.name) {
            el.appendChild(glyphIcon);
            el.appendChild(title);
            $('#bus_listings').append(el);
        }



        var mymarker = null;
        google.maps.event.addDomListener(title, 'mouseover', function () {

            resetMarkers();

            mymarker = new google.maps.Marker({
                position: markerPosition,
                map: map,
                title: item.title,
                size: new google.maps.Size(32, 40),
                icon: icon2

            });

            map.panTo(arrMarkers[i].getPosition());

            infowindow.setOptions({pixelOffset: new google.maps.Size(0, 0)});
            infowindow.open(map, mymarker);
        });
        google.maps.event.addDomListener(title, 'mouseout', function () {


            mymarker.setMap(null);

            resetMarkers();

        });
    });
    map.setCenter(boundbox.getCenter());
    map.setZoom(7);
    map.fitBounds(boundbox);  // keep map.fitBounds(boundbox) inside getJSON function as can be called before url finished loading  

    google.maps.event.addListenerOnce(map, 'bounds_changed', function (event) {
        if (this.getZoom()) {
            //if zoomed in all the way set to 15 to give margin
            if (this.getZoom() > 15) {
                this.setZoom(15);
            }
        }
    });

//                $('#status .current-status').remove();
//                $('#status .loading').remove();
//                $('#status').append('<div class="current-status">' + data.status + '</div>');
    $('#pagenav .page_numbers').remove();
    $('#pagenav').append(data.pages);
}

function scrollToElement(i) {



    $(".result").css("background-color", "white");
    var newpos = $('#' + i).offset().top - $('#0').offset().top;
    $('#scroll-div').animate({scrollTop: newpos}, 200);
    $("#" + i).css("background-color", "#eee");
    $("#" + i).css("background-image", "url('')");
    $("#" + i).css("background-repeat", "no-repeat");
    $("#" + i).css("background-position", "right center");


}

function loadHistoryContent(link, state)
{
    boundbox = new google.maps.LatLngBounds();
    clearOverlays();
    $.post(link,
            {

            },
            function (res, status) {
                data = JSON.parse(res);
                buildMap(data);
                urlPath = data.url;
//                document.title = data.title;
                if (state) {
                    markerCluster.clearMarkers();
                    markerCluster = new MarkerClusterer(map, arrMarkers, {
                        maxZoom: 20,
                        gridSize: 70,
                        styles: styles[0]
                    });
                    $('.directory_category').val(state.category);
                    $('.directory_location').val(state.location);

                } else {
                    markerCluster = new MarkerClusterer(map, arrMarkers, {
                        maxZoom: 20,
                        gridSize: 70,
                        styles: styles[0]
                    });//cluster
                    $('.directory_category').val(data.category);
                    $('.directory_location').val(data.location);
                }

            });

}


function loadSearch()
{

    var directory_categoryValue = $('.directory_category').val();
    var directory_locationValue = $('.directory_location').val();
//                $('#status .current-status').remove();
//                $('#status').append("<div class =\"loading\">loading...</div>");
    boundbox = new google.maps.LatLngBounds();

    $.post("/users/business-json",
            {
                directory_category: directory_categoryValue,
                directory_location: directory_locationValue
            },
            function (res, status) {
                clearOverlays();
                data = JSON.parse(res);
                if (data.redirect) {
                    window.location.href = data.redirect;
                } else if (!data.places) {

//                                $('#status .current-status').remove();
//                                $('#status .loading').remove();
//                                $('#status').append('<div class="current-status">' + data.status + '</div>');
                    $('#pagenav .page_numbers').remove();
                    $("#bus_listings div").remove();


                } else {



                    buildMap(data);
                    markerCluster.clearMarkers();
                    markerCluster = new MarkerClusterer(map, arrMarkers, {
                        maxZoom: 20,
                        gridSize: 70,
                        styles: styles[0]
                    });//cluster 
                    urlPath = data.url;
                    document.title = data.title;
                    window.history.pushState({"category": directory_categoryValue, "location": directory_locationValue, "pageTitle": document.title}, "", urlPath);


                }

            });

}