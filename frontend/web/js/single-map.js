var map;
var gdir;
var geocoder = null;
var addressMarker;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var centerCord = null;

var icon1 = "http://www.thedealer.ie/application/views/templates/default/images/icon-business-hover.png";

var arrMarkers = [];

function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    var eleLat = document.getElementById("user-lat");
    var eleLong = document.getElementById("user-long");
    var lat = eleLat.innerHTML;
    var long = eleLong.innerHTML;
    centerCord = new google.maps.LatLng(lat, long);
    var mapOptions = {
        zoom: 9,
        center: centerCord,

        mapTypeControl: false,
        panControl: false,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        },

        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas_small"), mapOptions);

    var marker = new google.maps.Marker({
        position: centerCord,
        map: map,
        size: new google.maps.Size(32, 40),
        icon: icon1
    });

    directionsDisplay.setMap(map);

    directionsDisplay.setOptions({suppressMarkers: true});

    directionsDisplay.setPanel(document.getElementById('directions-panel'));


}

function setDirections(start) {

    var end = centerCord;
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    };

    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });

}

function scrollToAds() {
    $("#click").click(function () {

        $('html, body').animate({
            scrollTop: $("#directory-spacer").offset().top
        }, 2000);

    });
}


$(function () {

    initialize();
    scrollToAds();
});