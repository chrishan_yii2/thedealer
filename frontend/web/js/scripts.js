$(document).ready(function(){
    $("#subscribe-div").find('form').attr('action', '');
    $('#signupform-company_name').hide();
    
    $('input[type="radio"]:checked').parent().addClass('check');


    $('input[type="radio"]').on('click', function(){
        $('input[type="radio"]').parent().removeClass("check");
        $(this).parent().addClass("check");
    });


    $('input[name="SignupForm[user_type]"]').on('click', function(){
        $('.radio-inline').removeClass("check");
        var companyName = $('#signupform-company_name');
        var val = $('input[name="SignupForm[user_type]"]:checked').val();
        if (val === "business"){
            $(companyName).show();
            $(companyName).addClass('required');
            $(this).parent('.radio-inline').addClass("check");
        } else if (val === "private"){
            $(companyName).hide();
            $(this).parent('.radio-inline').removeClass("check");
            $(this).parent('.radio-inline').addClass("check");
        }
    });
    $('label input[type="checkbox"]').change( function(){
        if($(this).is(":checked")){
            $(this).parent('label').addClass('checked');
        }
        else if($(this).is(":not(:checked)")){
            $(this).parent('label').removeClass('checked');
        }
    });
    $('#signupform-show_pass').change( function(){
        if($(this).is(":checked")){
            $('#signupform-password').prop('type', 'text');
        }
        else if($(this).is(":not(:checked)")){
            $('#signupform-password').prop('type', 'password');
        }
    });
     $('.options').select2({
         minimumResultsForSearch: -1
     });
     $('form select').select2();
});


$(function () {
    var maxHeight = 0;

    $(".contact-wrapper .contact-details").each(function(){
        if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });

    $(".contact-wrapper .contact-details").height(maxHeight);

    var height = $('#directory-page header').height() + $('#directory-page .search-form').height() + 32;
    $('.directory-wrapper .directories-location').css('height','calc(100vh - '+height+'px)');
});


$(document).ready(function(){
    
    $('#subscribe-div form').attr('action',' ');
    
//    $('#show').on('change',function(){
//        alert($(location).attr('href'));
//        
//    });
    
    $('.ajaxSave').on('click',function(){
        var id = $(this).data('id'),
            url = $(this).data('url'),
            cur = $(this);
            
            
        if(url != ""){
            $.ajax({
                type: "get",
                url: url,
                dataType: "json",
                data:{id: id},
                success: function(result){
                    if(result.status == "success"){
                        cur.html('<i class="fas fa-heart fa-fw fa-lg"></i>');
                    }else{
                        alert(result.msg);
                    }
                }
            });
        }
        
    });
    
    $('.categories-grid > a').click(function(){
        $('.categories-grid').css('margin-bottom', '20px');
        var that = $('.categories-grid > a.active');
        $('.categories-grid > a').removeClass('active');
        var margin = $($(this).attr('href')).outerHeight(true) + 25;
        var top = $(this).outerHeight(true);
        var count = $(this).parent('.categories-grid').data('count');
        if($(this).hasClass('collapsed')){
            $(this).addClass('active');
            $(this).parent('.categories-grid').css('margin-bottom', margin+'px');
            if (count > 9) {
                if (that.parent().data('count') < 9){
                    console.log('true');
                    top = top + that.outerHeight(true) + 36;
                    console.log(top);
                } else {
                    console.log('false');
                    top = top + $('.categories-grid[data-count="9"]').outerHeight(true);
                    console.log(top);
                }
            }
            $($(this).attr('href')).css('top',top+'px');
        } else {
            $(this).parent('.categories-grid').css('margin-bottom', '20px');
        }
    });

    $('.mobile-filter .filtered-head').click(function(){
        // var sidebar = $('.sidebar-filter').parent().html();
        // $('.mobile-sidebar-filter').append(sidebar);
    });
    
    function getQueryVariable(variable)
    {
           var query = window.location.search.substring(1);
           var vars = query.split("&");
           for (var i=0;i<vars.length;i++) {
                   var pair = vars[i].split("=");
                   if(pair[0] == variable){return pair[1];}
           }
           return(false);
    }

    $('#show').on('change', function(){
        $('body').append('<div class="loader"><i class="fal fa-spinner-third fa-spin fa-5x"></i></div>');
        var limit = $(this).val();
        var uri = window.location.href;
        var url = uri.toString();
        var old = getQueryVariable("per-page");
        var newOld = "&per-page="+old;
        var use = newOld.toString();
        var newUrl =  url.replace(use,'');
        
        document.location = newUrl+'&per-page='+limit;
    });
    
    $('#order').on('change', function () {
        $('body').append('<div class="loader"><i class="fal fa-spinner-third fa-spin fa-5x"></i></div>');
        var limit = $(this).val();
        var uri = window.location.href;
        var url = uri.toString();
        var old = getQueryVariable("sort");
        var newOld = "&sort="+old;
        var use = newOld.toString();
        var newUrl =  url.replace(use,'');
        
        document.location = newUrl+'&sort='+limit;
    });
    
    $('#account_type').on('change', function () {
        $('body').append('<div class="loader"><i class="fal fa-spinner-third fa-spin fa-5x"></i></div>');
        var limit = $(this).val();
        var uri = window.location.href;
        var url = uri.toString();
        var old = getQueryVariable("&ClassifiedSearch[ad_type]=");
        var newOld = "&ClassifiedSearch[ad_type]="+old;
        var use = newOld.toString();
        var newUrl =  url.replace(use,'');
        
        document.location = newUrl+'&ClassifiedSearch[ad_type]='+limit;
    });
    
    $('.ad-img').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.item-img-slider',
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-left fa-lg"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-right fa-lg"></i></button>',
    });
    $('.item-img-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.ad-img',
        focusOnSelect: true,
        vertical: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-up fa-lg"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-down fa-lg"></i></button>',
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    vertical: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    if($('#classifiedlistings-classified_cat_id').val() == '47'){
        $('#result_cat_id').css('display','block');
    } else {
        $('#result_cat_id').css('display','none');
    }

    $('#classifiedlistings-classified_cat_id').on('change', function () {
        if($(this).val() == '47') {
            $('#result_cat_id').css('display','block');
        } else {
            $('#result_cat_id').css('display','none');
        }
    });

});
