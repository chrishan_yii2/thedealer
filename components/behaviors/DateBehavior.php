<?php
namespace components\behaviors;

use yii\db\ActiveRecord;

/**
 * Description of GuidBehavior
 *
 * @author Sunil Kumar
 */
class DateBehavior extends \yii\base\Behavior
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
    
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
        ];
    }
    
    //Save data in DATEADDED field
    public static function beforeSave($event)
    {
        $this->owner->date_added = time();
        return true;
    }
}
